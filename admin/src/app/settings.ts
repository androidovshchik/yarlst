export enum Settings {
    MIN_WORKS_DATE = 'minWorksDate',
    MAX_WORKS_DATE = 'maxWorksDate',
    MIN_FINANCE_DATE = 'minFinanceDate',
    MAX_FINANCE_DATE = 'maxFinanceDate',
    MODE_LOAD_DEMO_WORKS = 'modeLoadDemoWorks'
}

export const admins: string[] = [
    'VqtkzG1oS5YoUVrc9Ty2y4vmLIo1',
    '0frlsMwmqJfOzmygjRAR3gnysuA3',
    'ASdHBqPYNTUr4AjNCZORmdDRCvr1',
    'q1gfuPXUH2MDgZ4eyo0M6bgl59q1',
    'e3hu48BVl7V5O1RtMjmYtGQzIGo2'
];