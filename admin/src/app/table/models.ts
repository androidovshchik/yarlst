export enum InputType {
    TEXT = 1,
    MILTILINE = 2,
    FLOAT = 3,
    NUMBER = 4,
    DROPDOWN = 5,
    DATE = 6,
    FUNCTION = 7
}

export interface Column {
    name: string,
    title: string,
    changeable?: boolean,
    input?: InputType,
    dimension?: string,
    calculate?: (...args: any[]) => string
}

export interface WorkerItem {
    _uid: string,
    name: string
}