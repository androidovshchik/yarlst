import {AfterViewInit, Component, EventEmitter, Input, NgModule, OnInit, Output, ViewChild} from '@angular/core';
import {
    DateAdapter,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
    MatButtonModule,
    MatDatepickerModule,
    MatDialog,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginator,
    MatPaginatorIntl,
    MatPaginatorModule,
    MatSelectModule,
    MatSnackBar,
    MatSnackBarModule,
    MatSort,
    MatSortModule,
    MatTableDataSource,
    MatTableModule,
    MatTooltipModule
} from '@angular/material';
import {CommonModule} from '@angular/common';
import {AngularFirestore} from 'angularfire2/firestore';
import {WorkerModel} from '../services/workers';
import {MatPaginatorRu} from './paginator';
import {FormsModule} from '@angular/forms';
import {APP_DATE_FORMATS, AppDateAdapter} from './picker';
import {RowEvent, TableService} from '../services/local/table';
import {DialogPrompt} from "../dialogs/prompt/scripts";
import {DialogGallery} from "../dialogs/gallery/scripts";
import {NgxGalleryModule} from "ngx-gallery";
import {Column, InputType, WorkerItem} from "./models";

@Component({
    selector: 'master-table',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss'],
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'ru-RU'},
        {provide: DateAdapter, useClass: AppDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS},
        {provide: MatPaginatorIntl, useClass: MatPaginatorRu}
    ]
})
export class MasterTableComponent implements OnInit, AfterViewInit {

    @ViewChild(MatSort)
    sort: MatSort;
    @ViewChild(MatPaginator)
    paginator: MatPaginator;

    InputType: typeof InputType = InputType;

    @Input()
    items: MatTableDataSource<any>;

    @Input()
    emitTableEvents: boolean = false;
    @Output()
    rowEmitter: EventEmitter<RowEvent> = new EventEmitter<RowEvent>();
    @Input()
    emitRowEvents: boolean = false;

    @Input()
    collection?: string = null;
    @Input()
    document?: string = null;
    @Input()
    array?: string = null;
    isRowDocument: boolean;

    @Input()
    enableRowAdd: boolean = false;
    @Input()
    enableRowAccept: boolean = false;
    @Input()
    enableRowEdit: boolean = false;
    @Input()
    enableRowDelete: boolean = false;
    waitingAccept: boolean = false;

    @Input()
    showWorkers: boolean = false;
    @Input()
    workers: WorkerItem[] = [];
    @Input()
    workerColumnsCount: number = 0;

    @Input()
    textColumns: Column[] = [];
    @Input()
    iconColumns: Column[] = [];
    @Input()
    displayedColumns: string[] = [];
    @Input()
    unsavedColumns: string[] = [];
    @Input()
    hiddenColumns: string[] = [];
    @Input()
    emittedColumns: string[] = [];
    menuColumn: Column = {
        name: '__menu',
        title: ' '
    };

    @Input()
    stickyCount: number = 0;

    @Input()
    nonNullValues: boolean = false;

    // for INPUT_FUNCTION
    @Input()
    multiplyFactor: number = 1;

    constructor(public store: AngularFirestore, public snackbar: MatSnackBar, public tableService: TableService,
                public dialog: MatDialog) {}

    ngOnInit() {
        this.items.filterPredicate = (data, filter) => {
            let matcher = new RegExp(filter, "i");
            for (let i = 0; i < this.displayedColumns.length; i++) {
                if (matcher.test(data[this.displayedColumns[i]])) {
                    return true;
                }
            }
            return false;
        };
        this.isRowDocument = this.document === null || this.array === null;
    }

    ngAfterViewInit() {
        this.items.sort = this.sort;
        this.items.paginator = this.paginator;
    }

    getClasses(index: number): object {
        return {
            [index < this.stickyCount ? 'cell-sticky-' + (index + 1) : 'cell']: true,
            'cell-border-right': this.stickyCount > 0 && index < this.displayedColumns.length - 1 && (index == this.stickyCount - 1 ||
                index > this.stickyCount && (index - this.stickyCount + 1) % this.workerColumnsCount == 0),
            'cell-max-width': this.stickyCount > 0,
            'cell-padding-left': index == 0
        };
    }

    isDisabled(column: Column, cell): boolean {
        return column.changeable === false && (!this.waitingAccept || this.items.data.indexOf(cell) !== 0);
    }

    onIcon(index: number, cell) {
        this.dialog.open(DialogGallery, {
            width: '600px',
            data: {
                cell: cell
            }
        }).afterClosed().subscribe(result => {
            if (result == null) {
                return;
            }
        });
    }

    onAddRow() {
        if (this.collection === null) {return;}
        this.waitingAccept = this.enableRowAccept;
        let row: object = {};
        if (this.isRowDocument) {
            row['_uid'] = MasterTableComponent.generateUid();
        }
        for (let d = 0, u; d < this.displayedColumns.length; d++) {
            if (this.displayedColumns[d].indexOf('__') !== -1) {
                continue;
            }
            for (u = 0; u < this.unsavedColumns.length; u++) {
                if (this.displayedColumns[d] === this.unsavedColumns[u]) {
                    break;
                }
            }
            if (u === this.unsavedColumns.length) {
                row[this.displayedColumns[d]] = this.getDefaultValue();
            }
        }
        this.items.data.unshift(row);
        this.items._updateChangeSubscription();
    }

    onAcceptRow() {
        this.waitingAccept = false;
        this.onRowChanged(this.items.data[0]);
    }

    onRowChanged(cell) {
        if (this.collection === null) {return;}
        if (this.waitingAccept) {return;}
        if (this.isRowDocument) {
            let object = this.processSavedColumns(cell);
            if (!this.isEmptyObject(object)) {
                this.updateDocument(this.collection, cell._uid, object, false)
                    .then(() => {
                        this.onEmitDataEvents();
                        this.showSnackbar("Изменения сохранены");
                    })
                    .catch(() => this.showSnackbar("Не удалось сохранить изменения"));
            } else {
                this.deleteDocument(cell._uid)
                    .then(() => {
                        this.onEmitDataEvents();
                        this.showSnackbar("Изменения сохранены")
                    })
                    .catch(() => this.showSnackbar("Не удалось сохранить изменения"));
            }
        } else {
            this.updateDocument(this.collection, this.document,
                {[this.array]: this.processRows(null)}, true)
                .then(() => {
                    this.onEmitDataEvents();
                    this.showSnackbar("Изменения сохранены");
                })
                .catch(() => this.showSnackbar("Не удалось сохранить изменения"));
        }
    }

    onEditRow(cell) {
        if (this.emitRowEvents) {
            setTimeout(() => {
                this.rowEmitter.emit({
                    position: this.items.data.indexOf(cell),
                    edit: cell
                });
            }, 0);
        }
    }

    onDeleteRow(cell) {
        this.dialog.open(DialogPrompt, {
            width: '280px',
            data: {}
        }).afterClosed().subscribe(result => {
            if (result == null) {
                return;
            }
            if (this.emitRowEvents) {
                setTimeout(() => {
                    this.rowEmitter.emit({
                        position: this.items.data.indexOf(cell),
                        del: cell
                    });
                }, 0);
                return;
            }
            if (this.collection === null) {return;}
            if (this.isRowDocument) {
                this.deleteDocument(cell._uid)
                    .then(() => this.removeRow(cell))
                    .catch(() => this.showSnackbar("Не удалось сохранить изменения"));
            } else {
                this.updateDocument(this.collection, this.document,
                    {[this.array]: this.processRows(this.items.data.indexOf(cell))}, true)
                    .then(() => this.removeRow(cell))
                    .catch(() => this.showSnackbar("Не удалось сохранить изменения"));
            }
        });
    }

    removeRow = (cell) => {
        this.items.data.splice(this.items.data.indexOf(cell), 1);
        this.items._updateChangeSubscription();
        this.onEmitDataEvents();
        this.showSnackbar("Изменения сохранены")
    };

    processRows(missingIndex?: number): object[] {
        let array: object[] = [];
        for (let i = 0; i < this.items.data.length; i++) {
            if (missingIndex !== null && i === missingIndex) {
                continue;
            }
            let object = this.processSavedColumns(this.items.data[i]);
            if (!this.isEmptyObject(object)) {
                array.push(object);
            }
        }
        return array;
    }

    processSavedColumns(cell): object {
        let object = {};
        for (let d = 0, u; d < this.displayedColumns.length; d++) {
            if (this.displayedColumns[d].indexOf('__') !== -1) {
                continue;
            }
            for (u = 0; u < this.unsavedColumns.length; u++) {
                if (this.displayedColumns[d] === this.unsavedColumns[u]) {
                    break;
                }
            }
            if (u === this.unsavedColumns.length) {
                let key = this.displayedColumns[d];
                object[key] = this.processColumn(cell[key]);
            }
        }
        for (let h = 0; h < this.hiddenColumns.length; h++) {
            let key = this.hiddenColumns[h];
            object[key] = this.processColumn(cell[key]);
        }
        return object;
    }

    processColumn(value) {
        if (/^ *$/.test(value)) {
            return this.getDefaultValue();
        } else if (Object.prototype.toString.call(value) === "[object Date]") {
            return isNaN(value.getTime()) ? this.getDefaultValue() : +value.getTime();
        } else {
            return value;
        }
    }

    isEmptyObject(object) {
        return !this.nonNullValues && Object.keys(object)
            .every((key) => key.indexOf('_') !== -1 || object[key] === null);
    }

    getDefaultValue(): string {
        return this.nonNullValues ? '' : null;
    }

    onWorkerPrint(worker: WorkerModel) {
        if (!worker.name) {
            this.showSnackbar("Имя работника не может быть пустым");
            return;
        }
        this.updateDocument('workers', worker._uid, {name: worker.name}, true)
            .then(() => this.showSnackbar("Изменения сохранены"))
            .catch(() => this.showSnackbar("Не удалось сохранить изменения"));
    }

    updateDocument(collection: string, document: string, object: object, merge: boolean): Promise<void> {
        console.log("Updating document " + document);
        console.log(object);
        return this.store.collection(collection)
            .doc(document).ref
            .set(object, {merge: merge});
    }

    deleteDocument(document: string): Promise<void> {
        console.log("Deleting document " + document);
        return this.store.collection(this.collection)
            .doc(document)
            .delete();
    }

    onEmitDataEvents = () => {
        if (this.emitTableEvents) {
            let array: object[] = [];
            for (let i = 0; i < this.items.data.length; i++) {
                let object = {};
                for (let e = 0; e < this.emittedColumns.length; e++) {
                    let key = this.emittedColumns[e];
                    object[key] = this.processColumn(this.items.data[i][key]);
                }
                if (!this.isEmptyObject(object)) {
                    array.push(object);
                }
            }
            this.tableService._event.next({
                path: this.isRowDocument ? `${this.collection}` : `${this.collection}/${this.document}/${this.array}`,
                array: array
            });
        }
    };

    showSnackbar = (message) => {
        this.snackbar.open(message, null,{
            duration: 1500
        });
    };

    static generateUid(): string {
        const chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        let uid = "";
        for (let i = 0; i < 24; i++) {
            uid += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return uid;
    }
}

@NgModule({
    imports: [
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatInputModule,
        MatMenuModule,
        MatSelectModule,
        MatButtonModule,
        MatIconModule,
        MatSnackBarModule,
        MatTooltipModule,
        MatDialogModule,
        NgxGalleryModule,
        CommonModule,
        FormsModule
    ],
    exports: [MasterTableComponent],
    declarations: [
        MasterTableComponent,
        DialogPrompt,
        DialogGallery
    ],
    entryComponents: [
        DialogPrompt,
        DialogGallery
    ]
})
export class MasterTableModule {}
