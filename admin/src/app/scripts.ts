import {Component, ElementRef, OnDestroy, ViewChild} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {MatSidenav, MatSnackBar} from '@angular/material';
import {ISubscription, Subscription} from 'rxjs/Subscription';
import {WorkerWorksService} from './services/workers/works';
import {WorkersService} from './services/workers';
import {Location} from '@angular/common';
import {MaterialsService} from './services/materials';
import {OperationsService} from './services/operations';
import {ProjectModel, ProjectsService} from './services/projects';
import {ProjectMaterialsService} from './services/projects/materials';
import {ProjectWorksService} from './services/projects/works';
import {AngularFireAuth} from "angularfire2/auth";
import {Settings} from "./settings";
import {LocalStorageService} from "angular-2-local-storage";
import * as firebase from "firebase";
import {User} from "firebase";

@Component({
    selector: 'root',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class RootComponent implements OnDestroy {

    @ViewChild(MatSidenav)
    sideNav: MatSidenav;
    @ViewChild('hamburger', {read: ElementRef})
    hamburger: ElementRef;

    _auth: Subscription;
    title: string = "";
    url: string = "";
    phone: string = "";

    _router: Subscription;
    _location: ISubscription;
    forceReload: boolean = false;

    _projects: Subscription;
    projects: ProjectMenuItem[] = [];
    filteredProjects: ProjectMenuItem[] = [];
    oldFilter: string = "";
    newFilter: string = "";

    _spinnerWorkers: Subscription;
    _spinnerWorkerWorks: Subscription;
    _spinnerMaterials: Subscription;
    _spinnerOperations: Subscription;
    _spinnerProjects: Subscription;
    _spinnerProjectWorks: Subscription;
    _spinnerProjectMaterials: Subscription;
    _refreshWorkerWorks: Subscription;
    showSpinner: boolean = false;
    showRefresh: boolean = false;

    _errorsWorkers: Subscription;
    _errorsWorkerWorks: Subscription;
    _errorsMaterials: Subscription;
    _errorsOperations: Subscription;
    _errorsProjects: Subscription;
    _errorsProjectWorks: Subscription;
    _errorsProjectMaterials: Subscription;

    constructor(private router: Router, private snackbar: MatSnackBar, private workerWorksService: WorkerWorksService,
                private workersService: WorkersService, private materialsService: MaterialsService,
                private operationsService: OperationsService, private projectsService: ProjectsService,
                private projectWorksService: ProjectWorksService, private location: Location,
                private projectMaterialsService: ProjectMaterialsService, private auth: AngularFireAuth,
                private localStorageService: LocalStorageService) {
        const firestore = firebase.firestore();
        const settings = {
            timestampsInSnapshots: true
        };
        firestore.settings(settings);
        this._router = router.events
            .filter((event: any) => event instanceof NavigationEnd)
            .subscribe(event => {
                if (this.forceReload) {
                    this.forceReload = false;
                    this.router.routeReuseStrategy.shouldReuseRoute = function() {
                        return true;
                    };
                }
                this.url = event['urlAfterRedirects'];
                this.setTitle(this.url);
            });
        this._location = location.subscribe(() => {
            this.forceReload = true;
            this.router.routeReuseStrategy.shouldReuseRoute = function() {
                return false;
            };
        });
        this._refreshWorkerWorks = workerWorksService._suggestion.asObservable()
            .subscribe(show => this.showRefresh = show);
        this._spinnerProjectWorks = projectWorksService._spinner.asObservable()
            .subscribe(show => this.showSpinner = show);
        this._spinnerProjectMaterials = projectMaterialsService._spinner.asObservable()
            .subscribe(show => this.showSpinner = show);
        this._spinnerProjects = projectsService._spinner.asObservable()
            .subscribe(show => this.showSpinner = show);
        this._spinnerWorkers = workersService._spinner.asObservable()
            .subscribe(show => this.showSpinner = show);
        this._spinnerWorkerWorks = workerWorksService._spinner.asObservable()
            .subscribe(show => this.showSpinner = show);
        this._spinnerMaterials = materialsService._spinner.asObservable()
            .subscribe(show => this.showSpinner = show);
        this._spinnerOperations = operationsService._spinner.asObservable()
            .subscribe(show => this.showSpinner = show);
        this._errorsWorkers = workersService._errors.asObservable()
            .subscribe(error => this.showSnackbar(error));
        this._errorsWorkerWorks = workerWorksService._errors.asObservable()
            .subscribe(error => this.showSnackbar(error));
        this._errorsMaterials = materialsService._errors.asObservable()
            .subscribe(error => this.showSnackbar(error));
        this._errorsOperations = operationsService._errors.asObservable()
            .subscribe(error => this.showSnackbar(error));
        this._errorsProjects = projectsService._errors.asObservable()
            .subscribe(error => this.showSnackbar(error));
        this._errorsProjectWorks = projectWorksService._errors.asObservable()
            .subscribe(error => this.showSnackbar(error));
        this._errorsProjectMaterials = projectMaterialsService._errors.asObservable()
            .subscribe(error => this.showSnackbar(error));
        this._projects = projectsService._documents.asObservable()
            .subscribe((model: ProjectModel) => {
                if (model != null) {
                    this.projects.push({
                        name: model.name,
                        link: '/projects/' + model._uid +'/' + model.name.replace(/\//g, "") + '/0'
                    });
                } else {
                    this.oldFilter = null;
                }
            });
        projectsService.loadAll();
        const periodEnd = new Date(Date.now() + 24 * 60 * 60 * 1000);
        periodEnd.setHours(0);
        periodEnd.setMinutes(0);
        periodEnd.setSeconds(0);
        periodEnd.setMilliseconds(0);
        const periodStart = new Date(periodEnd.getTime());
        periodStart.setMonth(periodStart.getMonth() - 1);
        this.localStorageService.set(Settings.MIN_WORKS_DATE, periodStart.getTime());
        this.localStorageService.set(Settings.MAX_WORKS_DATE, periodEnd.getTime());
        this.onRefreshWorkerWorks(false);
        workersService.loadAll();
        materialsService.loadAll();
        operationsService.loadAll();
        this._auth = this.auth.authState.subscribe((user: User) => {
            if (user != null) {
                this.phone = user.phoneNumber;
            }
        });
    }

    showSnackbar(error) {
        this.snackbar.open(error, null,{
            duration: 1500
        });
    }

    goToPage(page: string) {
        this.sideNav.close()
            .then(() => {
                this.hamburger.nativeElement.blur();
                let routeParams = decodeURIComponent(this.router.url).split('/'), pageParams = page.split('/');
                if (pageParams.length === routeParams.length) {
                    if (pageParams.length >= 4) {
                        if (routeParams[2] === pageParams[2]) {
                            return null;
                        }
                    } else if (pageParams.length >= 2) {
                        if (routeParams[1] === pageParams[1]) {
                            return null;
                        }
                    }
                }
                this.forceReload = true;
                this.router.routeReuseStrategy.shouldReuseRoute = function() {
                    return false;
                };
                setTimeout(() => {
                    this.router.navigate([page]);
                }, 0);
            });
    }

    setTitle(url: string) {
        if (url.startsWith('/information')) {
            this.title = "Сведения";
        } else if (url.startsWith('/data')) {
            this.title = "Данные";
        } else if (url.startsWith('/projects')) {
            this.title = decodeURIComponent(url.split('/')[3]);
        } else if (url.startsWith('/store')) {
            this.title = "Склад";
        } else if (url.startsWith('/finance')) {
            this.title = "Финансы";
        } else if (url.startsWith('/reports')) {
            this.title = "Отчёты";
        } else if (url.startsWith('/settings')) {
            this.title = "Настройки";
        } else if (url.startsWith('/login')) {
            this.title = "Авторизация";
        }
    }

    getFilteredProjects(): ProjectMenuItem[] {
        if (this.oldFilter === this.newFilter) {
            return this.filteredProjects;
        }
        this.oldFilter = this.newFilter;
        this.filteredProjects = [];
        let matcher = new RegExp(this.newFilter, "i");
        for (let i = 0, j; i < this.projects.length; i++) {
            if (this.filteredProjects.length >= 100) {
                break;
            }
            if (!matcher.test(this.projects[i].name)) {
                continue;
            }
            if (this.filteredProjects.length == 0) {
                this.filteredProjects.push(this.projects[i]);
            }
            for (j = 0; j < this.filteredProjects.length; j++) {
                if (this.filteredProjects[j].name === this.projects[i].name) {
                    break;
                }
            }
            if (j == this.filteredProjects.length) {
                this.filteredProjects.push(this.projects[i]);
            }
        }
        return this.filteredProjects;
    }

    onRefreshWorkerWorks(clearBuffer: boolean) {
        this.showRefresh = false;
        if (clearBuffer) {
            this.workerWorksService.clearDocuments();
        }
        const periodStart = this.localStorageService.get(Settings.MIN_WORKS_DATE);
        const periodEnd = this.localStorageService.get(Settings.MAX_WORKS_DATE);
        const subscription = this.workersService._documents.asObservable()
            .subscribe(worker => {
                if (worker != null) {
                    if (worker.enabled) {
                        this.workerWorksService.loadPart(worker, periodStart, periodEnd);
                    }
                } else {
                    if (subscription != null) {
                        subscription.unsubscribe();
                    }
                }
            });
    }

    onLogout() {
        this.auth.auth.signOut()
            .then(() => {
                this.goToPage('/login');
            })
            .catch(() => {
                this.showSnackbar('Не удалось выйти из аккаунта');
            });
    }

    ngOnDestroy() {
        if (this._auth !== null) {this._auth.unsubscribe();}
        if (this._router !== null) {this._router.unsubscribe();}
        if (this._location !== null) {this._location.unsubscribe();}
        if (this._spinnerWorkers !== null) {this._spinnerWorkers.unsubscribe();}
        if (this._spinnerWorkerWorks !== null) {this._spinnerWorkerWorks.unsubscribe();}
        if (this._spinnerMaterials !== null) {this._spinnerMaterials.unsubscribe();}
        if (this._spinnerOperations !== null) {this._spinnerOperations.unsubscribe();}
        if (this._spinnerProjects !== null) {this._spinnerProjects.unsubscribe();}
        if (this._refreshWorkerWorks !== null) {this._refreshWorkerWorks.unsubscribe();}
        if (this._errorsWorkers !== null) {this._errorsWorkers.unsubscribe();}
        if (this._errorsWorkerWorks !== null) {this._errorsWorkerWorks.unsubscribe();}
        if (this._errorsMaterials !== null) {this._errorsMaterials.unsubscribe();}
        if (this._errorsOperations !== null) {this._errorsOperations.unsubscribe();}
        if (this._errorsProjects !== null) {this._errorsProjects.unsubscribe();}
        if (this._errorsProjectWorks !== null) {this._errorsProjectWorks.unsubscribe();}
        if (this._errorsProjectMaterials !== null) {this._errorsProjectMaterials.unsubscribe();}
        if (this._projects !== null) {this._projects.unsubscribe();}
    }
}

export interface ProjectMenuItem {
    name: string,
    link: string
}