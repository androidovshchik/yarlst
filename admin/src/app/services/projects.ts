import {Injectable} from '@angular/core';
import {BaseService, CollectionModel} from './base';
import {Subject} from 'rxjs/Subject';
import {ReplaySubject} from 'rxjs/ReplaySubject';

@Injectable()
export class ProjectsService extends BaseService {

    _documents = new ReplaySubject<ProjectModel>();

    _document = new Subject<ProjectModel>();

    loadAll() {
        this._spinner.next(true);
        let subscription = this.store.collection<ProjectModel>("projects", ref => {
            return ref.orderBy("name");
        }).snapshotChanges(null)
            .first()
            .map(actions => {
                return actions.map(action => {
                    return {
                        _uid: action.payload.doc.id,
                        ...action.payload.doc.data()
                    };
                });
            })
            .subscribe((models: ProjectModel[]) => {
                subscription.unsubscribe();
                this.onSuccess("Project", models.length);
                models.forEach(model => {
                    this._documents.next(model);
                });
                this._documents.next(null);
                this._spinner.next(false);
            }, error => {
                subscription.unsubscribe();
                this._documents.next(null);
                this.onError(error, "Не удалось загрузить проекты");
            });
    }

    loadSingle(uid: string) {
        this._spinner.next(true);
        let subscription = this.store.doc<ProjectModel>("projects/" + uid)
            .snapshotChanges()
            .first()
            .map(action => {
                return {
                    _uid: action.payload.id,
                    ...action.payload.data()
                };
            })
            .subscribe((model: ProjectModel) => {
                subscription.unsubscribe();
                console.log(model);
                this._spinner.next(true);
                this._document.next(ProjectsService.validProject(model));
                this._spinner.next(false);
            }, error => {
                subscription.unsubscribe();
                this._document.next(null);
                this.onError(error, "Не удалось загрузить проект");
            });
    }

    static validProject(model) {
        BaseService.addIfMissed(model, "cost", null);
        BaseService.addIfMissed(model, "location", null);
        BaseService.addIfMissed(model, "distance", null);
        BaseService.addIfMissed(model, "temps", []);
        BaseService.addIfMissed(model, "fuel", []);
        BaseService.addIfMissed(model, "others", []);
        return model;
    }
}

export interface ProjectModel extends CollectionModel {
    name: string,
    cost: number,
    location: string,
    distance: number,
    temps: ProjectWorkTemp[],
    fuel: ProjectFuel[],
    others: ProjectOther[]
}

export interface ProjectWorkTemp {
    timestamp: number,
    text: string,
    payment: number
}

export interface ProjectFuel {
    auto: string,
    trips: number,
    expense: number,
    price: number
}

export interface ProjectOther {
    timestamp: number,
    description: string,
    payment: number
}