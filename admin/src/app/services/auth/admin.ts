import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {AngularFireAuth} from "angularfire2/auth";
import {catchError, mergeMap} from "rxjs/operators";
import {admins} from "../../settings";

@Injectable()
export class AuthAdmin implements CanActivate {

    constructor(private auth: AngularFireAuth, private router: Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.auth.authState.pipe(
            mergeMap(user => {
                if (user == null) {
                    this.router.navigate(['login']);
                    return Observable.of<boolean>(false);
                } else {
                    if (admins.indexOf(user.uid) != -1) {
                        return Observable.of<boolean>(true);
                    } else {
                        Observable.fromPromise(this.auth.auth.signOut()).pipe(
                            mergeMap(() => {
                                this.router.navigate(['login']);
                                return Observable.of<boolean>(false);
                            }),
                            catchError(() => {
                                return Observable.of<boolean>(false);
                            })
                        );
                        return Observable.of<boolean>(false);
                    }
                }
            })
        );
    }
}