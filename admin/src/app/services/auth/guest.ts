import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {AngularFireAuth} from "angularfire2/auth";
import {mergeMap} from "rxjs/operators";

@Injectable()
export class AuthGuest implements CanActivate {
    
    constructor(private auth: AngularFireAuth, private router: Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.auth.authState.pipe(
            mergeMap(user => {
                if (user == null) {
                    return Observable.of<boolean>(true);
                } else {
                    this.router.navigate(['information/0']);
                    return Observable.of<boolean>(false);
                }
            })
        );
    }
}