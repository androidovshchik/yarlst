import {Injectable} from '@angular/core';
import {BaseService, CollectionModel} from '../base';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class ProjectMaterialsService extends BaseService {

    _documents = new Subject<ProjectMaterial>();

    loadPart(uid: string, material?: string) {
        this._spinner.next(true);
        let subscription = this.store.collection<ProjectMaterial>(`projects/${uid}/materials`, ref => {
            let query = ref.limit(BaseService.LIMIT)
                .orderBy("_material")
                .where('_prod', '==', BaseService.isProd());
            if (material != null) {
                return query.startAfter(material);
            }
            return query;
        }).snapshotChanges(null)
            .first()
            .map(actions => {
                return actions.map(action => {
                    return {
                        _uid: action.payload.doc.id,
                        ...action.payload.doc.data()
                    };
                });
            })
            .subscribe((models: ProjectMaterial[]) => {
                subscription.unsubscribe();
                this.onSuccess("Project: Material", models.length);
                models.forEach(model => {
                    this._documents.next(model);
                });
                if (models.length >= BaseService.LIMIT) {
                    this.loadPart(uid, models[models.length - 1]._material);
                } else {
                    this._documents.next(null);
                    this._spinner.next(false);
                }
            }, error => {
                subscription.unsubscribe();
                this._documents.next(null);
                this.onError(error, "Не удалось загрузить смету материалов");
            });
    }
}

export interface ProjectMaterial extends CollectionModel {
    _prod: boolean,
    _material: string,
    _quantity: number,
    price: number
}