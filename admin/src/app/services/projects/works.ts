import {Injectable} from '@angular/core';
import {BaseService, CollectionModel} from '../base';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class ProjectWorksService extends BaseService {

    _workshops = new Subject<ProjectWorkPerm>();

    _objects = new Subject<ProjectWorkPerm>();

    loadPart(uid: string, collection: string, operation?: string) {
        this._spinner.next(true);
        const isWorkshop: boolean = collection === 'workshops';
        let subscription = this.store.collection<ProjectWorkPerm>(`projects/${uid}/${collection}`, ref => {
            let query = ref.limit(BaseService.LIMIT)
                .orderBy("_operation")
                .where('_prod', '==', BaseService.isProd());
            if (operation != null) {
                return query.startAfter(operation);
            }
            return query;
        }).snapshotChanges(null)
            .first()
            .map(actions => {
                return actions.map(action => {
                    return {
                        _uid: action.payload.doc.id,
                        ...action.payload.doc.data()
                    };
                });
            })
            .subscribe((models: ProjectWorkPerm[]) => {
                subscription.unsubscribe();
                this.onSuccess("Project: " + (isWorkshop ? 'Workshop' : 'Object'), models.length);
                models.forEach(model => {
                    if (isWorkshop) {
                        this._workshops.next(model);
                    } else {
                        this._objects.next(model);
                    }
                });
                if (models.length >= BaseService.LIMIT) {
                    this.loadPart(uid, collection, models[models.length - 1]._operation);
                } else {
                    if (isWorkshop) {
                        this._workshops.next(null);
                    } else {
                        this._objects.next(null);
                    }
                    this._spinner.next(false);
                }
            }, error => {
                subscription.unsubscribe();
                if (isWorkshop) {
                    this._workshops.next(null);
                } else {
                    this._objects.next(null);
                }
                this.onError(error, "Не удалось загрузить смету работ");
            });
    }
}

export interface ProjectWorkPerm extends CollectionModel {
    _prod: boolean,
    _date: string,
    _operation: string,
    payment: number
}