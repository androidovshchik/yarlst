import {Injectable} from '@angular/core';
import {BaseService, CollectionModel} from './base';
import {ReplaySubject} from 'rxjs/ReplaySubject';

@Injectable()
export class OperationsService extends BaseService {

    _documents = new ReplaySubject<OperationModel>();

    loadAll() {
        this._spinner.next(true);
        let subscription = this.store.collection<OperationModel>("operations", ref => {
            return ref.orderBy("name");
        }).snapshotChanges(null)
            .first()
            .map(actions => {
                return actions.map(action => {
                    return {
                        _uid: action.payload.doc.id,
                        ...action.payload.doc.data()
                    };
                });
            })
            .subscribe((models: OperationModel[]) => {
                subscription.unsubscribe();
                this.onSuccess("Operation", models.length);
                models.forEach(model => {
                    this._documents.next(model);
                });
                this._documents.next(null);
                this._spinner.next(false);
            }, error => {
                subscription.unsubscribe();
                this._documents.next(null);
                this.onError(error, "Не удалось загрузить операции");
            });
    }
}

export interface OperationModel extends CollectionModel {
    name: string,
    description: string
}