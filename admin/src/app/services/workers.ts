import {Injectable} from '@angular/core';
import {BaseService, CollectionModel} from './base';
import {ReplaySubject} from 'rxjs/ReplaySubject';

@Injectable()
export class WorkersService extends BaseService {

    _documents = new ReplaySubject<WorkerModel>();

    clearDocuments() {
        this._documents.complete();
        this._documents = null;
        this._documents = new ReplaySubject();
    }

    loadAll() {
        this._spinner.next(true);
        let subscription = this.store.collection<WorkerModel>("workers", ref => {
            return ref.where('_prod', '==', BaseService.isProd());
        }).snapshotChanges(null)
            .first()
            .map(actions => {
                return actions.map(action => {
                    return {
                        _uid: action.payload.doc.id,
                        ...action.payload.doc.data()
                    };
                });
            })
            .subscribe((models: WorkerModel[]) => {
                subscription.unsubscribe();
                this.onSuccess("Workers", models.length);
                models.forEach(model => {
                    BaseService.addIfMissed(model, 'enabled', true);
                    this._documents.next(model);
                });
                this._documents.next(null);
                this._spinner.next(false);
            }, error => {
                subscription.unsubscribe();
                this._documents.next(null);
                this.onError(error, "Не удалось загрузить работников");
            });
    }
}

export interface WorkerModel extends CollectionModel {
    _prod: boolean,
    _timestamp: number,
    _version: number,
    name: string,
    phone: string,
    enabled: boolean
}
