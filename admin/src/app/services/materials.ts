import {Injectable} from '@angular/core';
import {BaseService, CollectionModel} from './base';
import {ReplaySubject} from 'rxjs/ReplaySubject';

@Injectable()
export class MaterialsService extends BaseService {

    _documents = new ReplaySubject<MaterialModel>();

    loadAll() {
        this._spinner.next(true);
        let subscription = this.store.collection<MaterialModel>("materials", ref => {
            return ref.orderBy("name");
        }).snapshotChanges(null)
            .first()
            .map(actions => {
                return actions.map(action => {
                    return {
                        _uid: action.payload.doc.id,
                        ...action.payload.doc.data()
                    };
                });
            })
            .subscribe((models: MaterialModel[]) => {
                subscription.unsubscribe();
                this.onSuccess("Material", models.length);
                models.forEach(model => {
                    this._documents.next(model);
                });
                this._documents.next(null);
                this._spinner.next(false);
            }, error => {
                subscription.unsubscribe();
                this._documents.next(null);
                this.onError(error, "Не удалось загрузить материалы");
            });
    }
}

export interface MaterialModel extends CollectionModel {
    name: string,
    dimension: string
}