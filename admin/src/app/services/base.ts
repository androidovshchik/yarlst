import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Subject} from 'rxjs/Subject';
import {environment} from '../../environments/environment';
import {AngularFirestore} from 'angularfire2/firestore';

@Injectable()
export class BaseService {

    static LIMIT: number = 250;

    _spinner = new BehaviorSubject<boolean>(false);

    _errors = new Subject<string>();

    constructor(public store: AngularFirestore) {}

    onSuccess(name: string, length: number) {
        console.log(name + "'s length " + length);
        this._spinner.next(true);
    }

    onError(error, message: string) {
        console.log(error);
        this._spinner.next(false);
        this._errors.next(message);
    }

    static isProd(): boolean {
        return environment.production;
    }

    static addIfMissed(model, name, value) {
        if (!model.hasOwnProperty(name)) {
            model[name] = value;
        }
    }
}

export interface CollectionModel {
    _uid: string
}