import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {CollectionModel} from '../base';

@Injectable()
export class WorkService {

    _delete = new Subject<WorkDeleteEvent>();

    _update = new Subject<WorkUpdateEvent>();
}

export interface WorkDeleteEvent extends CollectionModel {
    _folder: string
}

export interface WorkUpdateEvent extends CollectionModel {
    _folder: string,
    _worker: string,
    date: string,
    project: string,
    workspace: string,
    operation: string
}