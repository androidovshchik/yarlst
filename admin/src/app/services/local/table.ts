import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class TableService {

    _event = new Subject<TableEvent>();
}

export interface TableEvent {
    path: string,
    array: object[]
}

export interface RowEvent {
    position: number,
    edit?: object,
    del?: object
}