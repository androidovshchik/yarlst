import {Injectable} from '@angular/core';
import {BaseService, CollectionModel} from '../base';
import {WorkerModel} from '../workers';
import {ReplaySubject} from 'rxjs/ReplaySubject';
import {Subject} from 'rxjs/Subject';
import {AngularFirestore} from "angularfire2/firestore";
import {LocalStorageService} from "angular-2-local-storage";
import {Settings} from "../../settings";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class WorkerWorksService extends BaseService {

    _documents = new ReplaySubject<WorkModel>();

    _document = new Subject<WorkModel>();

    _suggestion = new Subject<boolean>();

    _refresh = new BehaviorSubject<boolean>(true);

    constructor(public store: AngularFirestore, public localStorageService: LocalStorageService) {
        super(store);
    }

    clearDocuments() {
        this._documents.complete();
        this._documents = null;
        this._documents = new ReplaySubject();
        this._refresh.next(true);
    }

    loadPart(worker: WorkerModel, minTimestamp, maxTimestamp) {
        this._spinner.next(true);
        if (this.localStorageService.get(Settings.MODE_LOAD_DEMO_WORKS)) {
            this.loadFake(worker, minTimestamp, maxTimestamp);
            return;
        }
        let subscription = this.store.collection<WorkModel>(`workers/${worker._uid}/works`, ref => {
            return ref.limit(BaseService.LIMIT)
                .orderBy("_timestamp", "desc")
                .where('_timestamp', '<=', maxTimestamp)
                .where('_timestamp', '>=', minTimestamp);
        }).snapshotChanges(null)
            .first()
            .map(actions => {
                return actions.map(action => {
                    return {
                        _uid: action.payload.doc.id,
                        ...action.payload.doc.data()
                    };
                });
            })
            .subscribe((models: WorkModel[]) => {
                subscription.unsubscribe();
                this.onSuccess("Worker: Work", models.length);
                models.forEach(model => {
                    if (WorkerWorksService.checkEvents(model.events)) {
                        BaseService.addIfMissed(model, '_worker', worker._uid);
                        model._folder = worker._uid;
                        this._documents.next(model);
                    }
                });
                if (models.length >= BaseService.LIMIT) {
                    this.loadPart(worker, minTimestamp, models[models.length - 1]._timestamp);
                } else {
                    this._documents.next(null);
                    this._spinner.next(false);
                }
            }, error => {
                subscription.unsubscribe();
                this._documents.next(null);
                this.onError(error, "Не удалось загрузить работы");
            });
    }

    loadFake(worker: WorkerModel, minTimestamp, maxTimestamp) {
        this.onSuccess("Worker: Work", 100);
        for (let w = 0; w < 100; w++) {
            const endWork = minTimestamp + w * 60 * 1000, endEvent = maxTimestamp - w * 60 * 60 * 1000;
            this._documents.next({
                _uid: "_NO_UID",
                _timestamp: maxTimestamp,
                _version: 0,
                _worker: worker._uid,
                _folder: worker._uid,
                comment: null,
                project: "Проект",
                workspace: w % 2 == 0 ? "Цех" : "Объект",
                operation: "Операция",
                events: [{
                    start: minTimestamp,
                    end: endEvent < minTimestamp ? minTimestamp : endEvent,
                    type: "Работа"
                }, {
                    start: minTimestamp,
                    end: endWork > maxTimestamp ? maxTimestamp : endWork,
                    type: "Событие"
                }],
                expenses: [{
                    dimension: "шт.",
                    material: "Материал",
                    quantity: "" + (w + 1)
                }],
                photos: []
            });
        }
        this._documents.next(null);
        this._spinner.next(false);
    }

    loadSingle(worker: string, work: string) {
        this._spinner.next(true);
        let subscription = this.store.doc<WorkModel>(`workers/${worker}/works/${work}`)
            .snapshotChanges()
            .first()
            .map(action => {
                return {
                    _uid: action.payload.id,
                    ...action.payload.data()
                };
            })
            .subscribe((model: WorkModel) => {
                subscription.unsubscribe();
                console.log(model);
                this._spinner.next(true);
                if (WorkerWorksService.checkEvents(model.events)) {
                    this._document.next(model);
                } else {
                    this._document.next(null);
                }
                this._spinner.next(false);
            }, error => {
                subscription.unsubscribe();
                this._document.next(null);
                this.onError(error, "Не удалось загрузить работу");
            });
    }

    static checkEvents(events: WorkEvent[]): boolean {
        if (events == null) {
            return false;
        }
        let length = events.length;
        if (length <= 0 || events[0].type !== "Работа" || events[0].end < events[0].start) {
            console.log("Ошибка парсинга событий работы");
            console.log(events);
            return false;
        }
        for (let i = 1; i < length - 1; i++) {
            if (events[i].type === "Работа" || events[i].end < events[i].start) {
                console.log("Ошибка парсинга других событий");
                console.log(events);
                return false;
            }
        }
        return true;
    }
}

export interface WorkModel extends CollectionModel {
    _timestamp: number,
    _version: number,
    _worker?: string,
    _folder?: string,
    comment: string,
    project: string,
    workspace: string,
    operation: string,
    events: WorkEvent[],
    expenses: WorkExpense[],
    photos: WorkPhoto[]
}

export interface WorkEvent {
    start: number,
    end: number,
    type: string
}

export interface WorkExpense {
    dimension: string,
    material: string,
    quantity: string
}

export interface WorkPhoto {
    name: string
}