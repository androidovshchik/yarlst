import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RootComponent} from './scripts';
import {APP_ROUTES} from './routes';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LocationStrategy, PathLocationStrategy} from '@angular/common';
import {AngularFireModule} from 'angularfire2';
import {
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatTooltipModule
} from '@angular/material';
import {InformationModule} from './blocks/information/scripts';
import {FinanceModule} from './blocks/finance/scripts';
import {ProjectsModule} from './blocks/projects/scripts';
import {ReportsModule} from './blocks/reports/scripts';
import {StoreModule} from './blocks/store/scripts';
import {WorkerWorksService} from './services/workers/works';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {WorkersService} from './services/workers';
import {DataModule} from './blocks/data/scripts';
import {firebase} from '../environments/firebase';
import {MaterialsService} from './services/materials';
import {OperationsService} from './services/operations';
import {ProjectsService} from './services/projects';
import {FormsModule} from '@angular/forms';
import {ProjectMaterialsService} from './services/projects/materials';
import {ProjectWorksService} from './services/projects/works';
import {AngularFireStorageModule} from 'angularfire2/storage';
import {TableService} from './services/local/table';
import {WorkService} from './services/local/work';
import {SettingsModule} from "./blocks/settings/scripts";
import {LocalStorageModule} from "angular-2-local-storage";
import {LoginModule} from "./blocks/login/scripts";
import {AuthAdmin} from "./services/auth/admin";
import {AuthGuest} from "./services/auth/guest";
import {AngularFireAuthModule} from "angularfire2/auth";

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        LoginModule,
        InformationModule,
        DataModule,
        ProjectsModule,
        StoreModule,
        FinanceModule,
        ReportsModule,
        SettingsModule,
        MatProgressSpinnerModule,
        MatSidenavModule,
        MatListModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatTooltipModule,
        MatToolbarModule,
        MatIconModule,
        MatSnackBarModule,
        LocalStorageModule.withConfig({
            prefix: 'yarlst',
            storageType: 'localStorage'
        }),
        RouterModule.forRoot(APP_ROUTES),
        AngularFireModule.initializeApp(firebase),
        AngularFirestoreModule,
        AngularFireStorageModule,
        AngularFireAuthModule
    ],
    declarations: [
        RootComponent
    ],
    providers: [
        {
            provide: LocationStrategy, useClass: PathLocationStrategy
        },
        WorkersService,
        WorkerWorksService,
        MaterialsService,
        OperationsService,
        ProjectsService,
        ProjectWorksService,
        ProjectMaterialsService,
        TableService,
        WorkService,
        AuthAdmin,
        AuthGuest
    ],
    exports: [
        RouterModule
    ],
    bootstrap: [
        RootComponent
    ]
})
export class AppModule {}
