import {Routes} from '@angular/router';
import {InformationComponent} from './blocks/information/scripts';
import {ProjectsComponent} from './blocks/projects/scripts';
import {ReportsComponent} from './blocks/reports/scripts';
import {FinanceComponent} from './blocks/finance/scripts';
import {StoreComponent} from './blocks/store/scripts';
import {DataComponent} from './blocks/data/scripts';
import {SettingsComponent} from "./blocks/settings/scripts";
import {LoginComponent} from "./blocks/login/scripts";
import {AuthGuest} from "./services/auth/guest";
import {AuthAdmin} from "./services/auth/admin";

export const APP_ROUTES: Routes = [
    {
        path: '',
        redirectTo: 'information/0',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: LoginComponent,
        canActivate: [AuthGuest]
    },
    {
        path: 'information/:tab',
        component: InformationComponent,
        canActivate: [AuthAdmin]
    },
    {
        path: 'data/:tab',
        component: DataComponent,
        canActivate: [AuthAdmin]
    },
    {
        path: 'projects/:uid/:name/:tab',
        component: ProjectsComponent,
        canActivate: [AuthAdmin]
    },
    {
        path: 'store',
        component: StoreComponent,
        canActivate: [AuthAdmin]
    },
    {
        path: 'finance/:tab',
        component: FinanceComponent,
        canActivate: [AuthAdmin]
    },
    {
        path: 'reports',
        component: ReportsComponent,
        canActivate: [AuthAdmin]
    },
    {
        path: 'settings',
        component: SettingsComponent,
        canActivate: [AuthAdmin]
    },
    {
        path: '**',
        redirectTo: 'information/0'
    }
];
