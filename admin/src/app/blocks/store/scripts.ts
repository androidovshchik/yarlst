import {Component, NgModule} from '@angular/core';
import {BaseBlock} from '../base/block';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'store',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class StoreComponent extends BaseBlock {

    constructor(public router: Router, public route: ActivatedRoute) {
        super(router, route);
    }
}

@NgModule({
    imports: [],
    exports: [StoreComponent],
    declarations: [StoreComponent]
})
export class StoreModule {}
