import {Component, NgModule, OnDestroy} from '@angular/core';
import {BaseBlock} from '../base/block';
import {
    DateAdapter,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
    MatButtonModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatSlideToggleModule,
    MatSnackBar,
    MatSnackBarModule
} from "@angular/material";
import {FormsModule} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {LocalStorageService} from "angular-2-local-storage";
import {CommonModule} from "@angular/common";
import {WorkerModel, WorkersService} from "../../services/workers";
import {Subscription} from "rxjs/Subscription";
import {Settings} from "../../settings";
import {AngularFirestore} from "angularfire2/firestore";
import {APP_DATE_FORMATS, AppDateAdapter} from "../../table/picker";

@Component({
    selector: 'settings',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss'],
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'ru-RU'},
        {provide: DateAdapter, useClass: AppDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
    ]
})
export class SettingsComponent extends BaseBlock implements OnDestroy {

    periodWorksStart: Date;
    periodWorksEnd: Date;
    periodFinanceStart: Date;
    periodFinanceEnd: Date;

    _workers: Subscription;
    workers: WorkerModel[] = [];

    hasEnabledMode: boolean;

    constructor(public router: Router, public route: ActivatedRoute, public localStorageService: LocalStorageService,
                public workersService: WorkersService, public snackbar: MatSnackBar, public store: AngularFirestore) {
        super(router, route);
        this.periodWorksStart = new Date(this.localStorageService.get(Settings.MIN_WORKS_DATE));
        this.periodWorksEnd = new Date(this.localStorageService.get(Settings.MAX_WORKS_DATE));
        this._workers = this.workersService._documents.asObservable()
            .subscribe(worker => {
                if (worker != null) {
                    this.workers.push(worker);
                }
            });
        this.hasEnabledMode = this.localStorageService.get(Settings.MODE_LOAD_DEMO_WORKS);
    }

    onPeriodChangedFrugally(clearBuffer: boolean) {
        //if (clearBuffer && !this.hasPeriodChanges) {
            //this.showSnackbar("Период не изменялся");
            //return;
        //}
        this.onPeriodChanged(clearBuffer);
    }

    onPeriodChanged(clearBuffer: boolean) {
        /*this.showRefresh = this.enablePeriods = this.hasPeriodChanges = false;
        if (clearBuffer) {
            this.workerWorksService.clearDocuments();
        }
        this._workers = this.workersService._documents.asObservable()
            .subscribe(worker => {
                if (worker != null) {
                    this.workerWorksService.loadPart(worker, this.periodStart.getTime(), this.periodEnd.getTime());
                } else {
                    if (this._workers != null) {
                        this._workers.unsubscribe();
                    }
                    this.enablePeriods = true;
                }
            });*/
    }

    onToggleWorker(event, i) {
        this.updateDocument('workers', this.workers[i]._uid, {enabled: event.selected})
            .then(() => this.showSnackbar("Изменения сохранены"))
            .catch(() => this.showSnackbar("Не удалось сохранить изменения"));
    }

    onMode(event) {
        this.hasEnabledMode = event.checked;
        this.localStorageService.set(Settings.MODE_LOAD_DEMO_WORKS, this.hasEnabledMode);
    }

    updateDocument(collection: string, document: string, object: object, merge: boolean = true): Promise<void> {
        return this.store.collection(collection)
            .doc(document).ref
            .set(object, {merge: merge});
    }

    showSnackbar = (message) => {
        this.snackbar.open(message, null,{
            duration: 1500
        });
    };

    ngOnDestroy() {
        console.log('ngOnDestroy');
        /*if (this.hasEnabledMode != this.localStorageService.get(Settings.MODE_LOAD_DEMO_WORKS)) {
            this.localStorageService.set(Settings.MODE_LOAD_DEMO_WORKS, this.hasEnabledMode);
            if (this.hasEnabledMode) {
                this.workerWorksService.clearDocuments();
                //this.workerWorksService.loadPart(worker, periodStart.getTime(), periodEnd.getTime());
            }
        }*/
        if (this._workers != null) {
            this._workers.unsubscribe();
        }
    }
}

@NgModule({
    imports: [
        MatListModule,
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        MatButtonModule,
        MatIconModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        CommonModule,
        FormsModule
    ],
    exports: [SettingsComponent],
    declarations: [SettingsComponent]
})
export class SettingsModule {}
