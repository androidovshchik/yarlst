import {ChangeDetectorRef, Component, NgModule, OnInit} from '@angular/core';
import {BaseTab} from '../../base/tab';
import {MasterTableModule} from '../../../table/scripts';
import {MatSnackBar, MatSnackBarModule} from '@angular/material';
import {AngularFirestore} from 'angularfire2/firestore';
import {OperationModel, OperationsService} from '../../../services/operations';
import {Column, InputType} from "../../../table/models";

@Component({
  selector: 'data-operations',
  templateUrl: './index.html',
  styleUrls: ['./styles.scss']
})
export class DataOperationsComponent extends BaseTab implements OnInit {

    columns: Column[] = [
        {name: 'name', title: 'Наименование', changeable: false, input: InputType.TEXT},
        {name: 'description', title: 'Описание', input: InputType.MILTILINE}
    ];
    displayedColumns = this.columns.map(x => x.name);

    constructor(public store: AngularFirestore, public snackbar: MatSnackBar, public operationsService: OperationsService,
                public cdr: ChangeDetectorRef) {
        super(store, snackbar);
    }

    ngOnInit() {
        setTimeout(() => {
            this.cdr.detach();
            this._subscription = this.operationsService._documents.asObservable()
                .subscribe((model: OperationModel) => {
                    if (model != null) {
                        this.items.data.push(model);
                    } else {
                        this.items._updateChangeSubscription();
                        this.cdr.detectChanges();
                        this.cdr.reattach();
                    }
                });
        }, 0);
    }
}

@NgModule({
  imports: [
      MasterTableModule,
      MatSnackBarModule
  ],
  exports: [DataOperationsComponent],
  declarations: [DataOperationsComponent]
})
export class DataOperationsModule {}
