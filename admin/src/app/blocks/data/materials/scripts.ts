import {ChangeDetectorRef, Component, NgModule, OnInit} from '@angular/core';
import {BaseTab} from '../../base/tab';
import {MasterTableModule} from '../../../table/scripts';
import {MatSnackBar, MatSnackBarModule} from '@angular/material';
import {AngularFirestore} from 'angularfire2/firestore';
import {MaterialModel, MaterialsService} from '../../../services/materials';
import {Column, InputType} from "../../../table/models";

@Component({
  selector: 'data-materials',
  templateUrl: './index.html',
  styleUrls: ['./styles.scss']
})
export class DataMaterialsComponent extends BaseTab implements OnInit {

    columns: Column[] = [
        {name: 'name', title: 'Наименование', changeable: false, input: InputType.TEXT},
        {name: 'dimension', title: 'Размерность', input: InputType.TEXT}
    ];
    displayedColumns = this.columns.map(x => x.name);

    constructor(public store: AngularFirestore, public snackbar: MatSnackBar, public materialsService: MaterialsService,
                public cdr: ChangeDetectorRef) {
        super(store, snackbar);
    }

    ngOnInit() {
        setTimeout(() => {
            this.cdr.detach();
            this._subscription = this.materialsService._documents.asObservable()
                .subscribe((model: MaterialModel) => {
                    if (model != null) {
                        this.items.data.push(model);
                    } else {
                        this.items._updateChangeSubscription();
                        this.cdr.detectChanges();
                        this.cdr.reattach();
                    }
                });
        }, 0);
    }
}

@NgModule({
  imports: [
      MasterTableModule,
      MatSnackBarModule
  ],
  exports: [DataMaterialsComponent],
  declarations: [DataMaterialsComponent]
})
export class DataMaterialsModule {}
