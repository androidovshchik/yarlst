import {Component, NgModule} from '@angular/core';
import {MatButtonModule, MatIconModule, MatTabsModule, MatTooltipModule} from '@angular/material';
import {BaseBlock} from '../base/block';
import {DataMaterialsModule} from './materials/scripts';
import {DataOperationsModule} from './operations/scripts';
import {DataProjectsModule} from './projects/scripts';
import {CommonModule} from '@angular/common';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'data',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class DataComponent extends BaseBlock {

    constructor(public router: Router, public route: ActivatedRoute) {
        super(router, route);
    }
}

@NgModule({
    imports: [
        MatTabsModule,
        MatButtonModule,
        MatIconModule,
        MatTooltipModule,
        DataMaterialsModule,
        DataOperationsModule,
        DataProjectsModule,
        CommonModule
    ],
    exports: [DataComponent],
    declarations: [DataComponent]
})
export class DataModule {}
