import {Component, NgModule} from '@angular/core';
import {BaseTab} from '../../base/tab';
import {MasterTableModule} from '../../../table/scripts';
import {MatDialog, MatSnackBar, MatSnackBarModule} from '@angular/material';
import {DialogEditWorkerWork} from '../../../dialogs/work/scripts';
import {AngularFirestore} from 'angularfire2/firestore';
import {WorkerWorksService, WorkModel} from '../../../services/workers/works';
import {BaseBlock} from '../../base/block';
import {WorkDeleteEvent, WorkService, WorkUpdateEvent} from '../../../services/local/work';
import {Subscription} from 'rxjs/Subscription';
import {Column} from "../../../table/models";

@Component({
    selector: 'information-events',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class InformationEventsComponent extends BaseTab {

    _update: Subscription;
    _delete: Subscription;

    staticColumns: Column[] = [
        {name: 'date', title: 'Дата'},
        {name: 'event', title: 'Событие'}
    ];
    staticDisplayedColumns = this.staticColumns.map(x => x.name);
    stickyCount: number = this.staticColumns.length;

    columns: Column[] = this.staticColumns;
    displayedColumns: any[] = this.staticDisplayedColumns;

    constructor(public store: AngularFirestore, public snackbar: MatSnackBar, public dialog: MatDialog,
                public workerWorksService: WorkerWorksService, public workService: WorkService) {
        super(store, snackbar);
        this._update = workService._update.asObservable()
            .subscribe((update: WorkUpdateEvent) => {
                for (let i = this.items.data.length - 1; i >= 0; i--) {
                    if (this.items.data[i]._folder === update._folder && this.items.data[i]._work == update._uid) {
                        this.items.data[i].date = update.date;
                        if (this.items.data[i]._worker !== update._worker) {
                            this.items.data[i]['__b' + this.items.data[i]._worker] = '';
                            this.items.data[i]['__e' + this.items.data[i]._worker] = '';
                            this.items.data[i]['__d' + this.items.data[i]._worker] = '';
                        }
                        this.items.data[i]._worker = update._worker;
                        this.items.data[i]['__b' + update._worker] = BaseBlock.getTime(this.items.data[i]._start);
                        this.items.data[i]['__e' + update._worker] = BaseBlock.getTime(this.items.data[i]._end);
                        this.items.data[i]['__d' + update._worker] =
                            BaseBlock.getDuration(this.items.data[i]._end - this.items.data[i]._start);
                    }
                }
                this.items._updateChangeSubscription();
            });
        this._delete = workService._delete.asObservable()
            .subscribe((del: WorkDeleteEvent) => {
                for (let i = this.items.data.length - 1; i >= 0; i--) {
                    if (this.items.data[i]._folder === del._folder && this.items.data[i]._work == del._uid) {
                        this.items.data.splice(i, 1);
                    }
                }
                this.items._updateChangeSubscription();
            });
    }

    onRowEvent(event: any) {
        console.log(event);
        this._subscription = this.workerWorksService._document.asObservable()
            .subscribe((work: WorkModel) => {
                if (work != null) {
                    if (event.hasOwnProperty('del')) {
                        for (let e = 0; e < work.events.length; e++) {
                            if (work.events[e].start === event.del._start &&
                                work.events[e].end === event.del._end &&
                                work.events[e].type === event.del.event) {
                                work.events.splice(e, 1);
                                break;
                            }
                        }
                        delete work._uid;
                        this.updateDocument(`workers/${event.del._folder}/works/`, event.del._work, work)
                            .then(() => {
                                this.items.data.splice(event.position, 1);
                                this.items._updateChangeSubscription();
                                this.workerWorksService._suggestion.next(true);
                                this.showSnackbar("Событие удалено. Для корректной работы потребуется обновление данных");
                            })
                            .catch((error) => {
                                console.log(error);
                                this.showSnackbar("Не удалось удалить событие");
                            });
                    } else if (event.hasOwnProperty('edit')) {
                        if (!work.hasOwnProperty('_worker')) {
                            work._worker = event.edit._worker;
                        }
                        let position = -1;
                        for (let e = 0; e < work.events.length; e++) {
                            if (work.events[e].start === event.edit._start &&
                                work.events[e].end === event.edit._end &&
                                work.events[e].type === event.edit.event) {
                                position = e;
                                break;
                            }
                        }
                        // position 0 is for work event only
                        if (position < 1) {
                            this.showSnackbar("Ошибка при обработке событий");
                            return;
                        }
                        this.dialog.open(DialogEditWorkerWork, {
                            width: '280px',
                            data: {
                                tab: 1,
                                position: position,
                                work: work
                            }
                        }).afterClosed().subscribe(result => {
                            if (result == null) {
                                return;
                            }
                            if (work.events[position].end < work.events[position].start) {
                                this.showSnackbar("Невалидный промежуток времени события");
                                return;
                            }
                            delete work._uid;
                            console.log(result);
                            this.updateDocument(`workers/${event.edit._folder}/works/`, event.edit._work, work)
                                .then(() => {
                                    this.items.data[event.position].event = work.events[position].type;
                                    this.items.data[event.position]._start = work.events[position].start;
                                    this.items.data[event.position]._end = work.events[position].end;
                                    this.items.data[event.position]['__b' + event.edit._worker] =
                                        BaseBlock.getTime(work.events[position].start);
                                    this.items.data[event.position]['__e' + event.edit._worker] =
                                        BaseBlock.getTime(work.events[position].end);
                                    this.items.data[event.position]['__d' + event.edit._worker] =
                                        BaseBlock.getDuration(work.events[position].end -
                                            work.events[position].start);
                                    this.items._updateChangeSubscription();
                                    this.workerWorksService._suggestion.next(true);
                                    this.showSnackbar("Событие отредактировано. Для корректной работы потребуется обновление данных");
                                })
                                .catch((error) => {
                                    console.log(error);
                                    this.showSnackbar("Не удалось отредактировать событие");
                                });
                        });
                    }
                } else {
                    this.showSnackbar("Событие было удалено. Попробуйте обновить данные");
                }
                if (this._subscription != null) {
                    this._subscription.unsubscribe();
                    this._subscription = null;
                }
            });
        if (event.hasOwnProperty('del')) {
            this.showSnackbar("Подождите...");
            this.workerWorksService.loadSingle(event.del._folder, event.del._work);
        } else if (event.hasOwnProperty('edit')) {
            this.workerWorksService.loadSingle(event.edit._folder, event.edit._work);
        } else {
            this.showSnackbar("Что-то не так");
        }
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        if (this._update != null) {
            this._update.unsubscribe();
        }
        if (this._delete != null) {
            this._delete.unsubscribe();
        }
    }
}

@NgModule({
    imports: [
        MasterTableModule,
        MatSnackBarModule
    ],
    exports: [InformationEventsComponent],
    entryComponents: [
        DialogEditWorkerWork
    ],
    declarations: [
        InformationEventsComponent
    ]
})
export class InformationEventsModule {}
