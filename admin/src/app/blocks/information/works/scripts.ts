import {Component, NgModule} from '@angular/core';
import {BaseTab} from '../../base/tab';
import {MasterTableModule} from '../../../table/scripts';
import {MatDialog, MatSnackBar, MatSnackBarModule} from '@angular/material';
import {DialogEditWorkerWork} from '../../../dialogs/work/scripts';
import {AngularFirestore} from 'angularfire2/firestore';
import {WorkerWorksService, WorkModel, WorkPhoto} from '../../../services/workers/works';
import {AngularFireStorage} from 'angularfire2/storage';
import {BaseBlock} from '../../base/block';
import {WorkService} from '../../../services/local/work';
import {Column} from "../../../table/models";

@Component({
  selector: 'information-works',
  templateUrl: './index.html',
  styleUrls: ['./styles.scss']
})
export class InformationWorksComponent extends BaseTab {

    staticColumns: Column[] = [
        {name: 'date', title: 'Дата'},
        {name: 'project', title: 'Проект'},
        {name: 'workspace', title: 'Место'},
        {name: 'operation', title: 'Операция'}
    ];
    staticDisplayedColumns = this.staticColumns.map(x => x.name);
    stickyCount: number = this.staticColumns.length;

    columns: Column[] = this.staticColumns;
    displayedColumns: any[] = this.staticDisplayedColumns;
    iconColumns: Column[] = [
        {name: '__icon1', title: ' ',
            calculate: function(cell): string {
                if (cell._photos.length > 0) {
                    return 'photo_library';
                }
                if (cell._comment) {
                    return 'comment';
                }
                return 'block';
            }
        }
    ];

    constructor(public store: AngularFirestore, public snackbar: MatSnackBar, public dialog: MatDialog,
                public workerWorksService: WorkerWorksService, public storage: AngularFireStorage,
                public workService: WorkService) {
        super(store, snackbar);
    }

    onRowEvent(event: any) {
        console.log(event);
        this._subscription = this.workerWorksService._document.asObservable()
            .subscribe((work: WorkModel) => {
                if (work != null) {
                    if (event.hasOwnProperty('del')) {
                        this.deleteWork(event.del._folder, event.del._work, work.photos, event.position,
                            work.photos.length === 3 || work.photos.length === 5 ? -1 : 0);
                    } else if (event.hasOwnProperty('edit')) {
                        if (!work.hasOwnProperty('_worker')) {
                            work._worker = event.edit._worker;
                        }
                        this.dialog.open(DialogEditWorkerWork, {
                            width: '280px',
                            data: {
                                tab: 0,
                                date: new Date(work.events[0].end),
                                work: work
                            }
                        }).afterClosed().subscribe(result => {
                            if (result == null) {
                                return;
                            }
                            if (work.events[0].end < work.events[0].start) {
                                this.showSnackbar("Невалидный промежуток времени работы");
                                return;
                            }
                            delete work._uid;
                            console.log(result);
                            this.updateDocument(`workers/${event.edit._folder}/works/`, event.edit._work, work)
                                .then(() => {
                                    const workEndDate = BaseBlock.getDate(work.events[0].end);
                                    this.items.data[event.position].project = work.project;
                                    this.items.data[event.position].workspace = work.workspace;
                                    this.items.data[event.position].operation = work.operation;
                                    this.items.data[event.position].date = workEndDate;
                                    if (work._worker !== event.edit._worker) {
                                        this.items.data[event.position]['__b' + event.edit._worker] = '';
                                        this.items.data[event.position]['__e' + event.edit._worker] = '';
                                        this.items.data[event.position]['__d' + event.edit._worker] = '';
                                    }
                                    this.items.data[event.position]['__b' + work._worker] =
                                        BaseBlock.getTime(work.events[0].start);
                                    this.items.data[event.position]['__e' + work._worker] =
                                        BaseBlock.getTime(work.events[0].end);
                                    this.items.data[event.position]['__d' + work._worker] =
                                        BaseBlock.getDuration(work.events[0].end - work.events[0].start);
                                    this.items.data[event.position]._worker = work._worker;
                                    this.items._updateChangeSubscription();
                                    this.workService._update.next({
                                        _folder: event.edit._folder,
                                        _uid: event.edit._work,
                                        _worker: work._worker,
                                        date: workEndDate,
                                        project: work.project,
                                        workspace: work.workspace,
                                        operation: work.operation
                                    });
                                    this.workerWorksService._suggestion.next(true);
                                    this.showSnackbar("Работа отредактирована. Для корректной работы потребуется обновление данных");
                                })
                                .catch((error) => {
                                    console.log(error);
                                    this.showSnackbar("Не удалось отредактировать работу");
                                });
                        });
                    }
                } else {
                    this.showSnackbar("Работа была удалена. Попробуйте обновить данные");
                }
                if (this._subscription != null) {
                    this._subscription.unsubscribe();
                    this._subscription = null;
                }
            });
        if (event.hasOwnProperty('del')) {
            this.showSnackbar("Подождите...");
            this.workerWorksService.loadSingle(event.del._folder, event.del._work);
        } else if (event.hasOwnProperty('edit')) {
            this.workerWorksService.loadSingle(event.edit._folder, event.edit._work);
        } else {
            this.showSnackbar("Что-то не так");
        }
    }

    deleteWork(worker: string, work: string, photos: WorkPhoto[], position: number, counter: number) {
        if (photos.length > 0) {
            console.log(`Deleting path photos/${worker}/${photos[0].name}`);
            this.storage.storage.ref(`photos/${worker}/${photos[0].name}`)
                .delete()
                .then(() => {
                    photos.splice(0, 1);
                    counter++;
                    if (counter === photos.length) {
                        this.showSnackbar("Удаляются фотографии...");
                    }
                    this.deleteWork(worker, work, photos, position, counter);
                })
                .catch(() => {
                    this.showSnackbar("Не удалось удалить фото работы");
                });
        } else {
            this.deleteDocument(`workers/${worker}/works/`, work)
                .then(() => {
                    this.items.data.splice(position, 1);
                    this.items._updateChangeSubscription();
                    this.workService._delete.next({
                        _folder: worker,
                        _uid: work
                    });
                    this.workerWorksService._suggestion.next(true);
                    this.showSnackbar("Работа удалена. Для корректной работы потребуется обновление данных");
                })
                .catch(() => {
                    this.showSnackbar("Не удалось удалить работу");
                });
        }
    }
}

@NgModule({
    imports: [
        MasterTableModule,
        MatSnackBarModule
    ],
    exports: [InformationWorksComponent],
    entryComponents: [
        DialogEditWorkerWork
    ],
    declarations: [
        InformationWorksComponent
    ]
})
export class InformationWorksModule {}
