import {Component, NgModule} from '@angular/core';
import {BaseTab} from '../../base/tab';
import {MasterTableModule} from '../../../table/scripts';
import {MatDialog, MatSnackBar, MatSnackBarModule} from '@angular/material';
import {DialogEditWorkerWork} from '../../../dialogs/work/scripts';
import {AngularFirestore} from 'angularfire2/firestore';
import {WorkerWorksService, WorkModel} from '../../../services/workers/works';
import {WorkDeleteEvent, WorkService, WorkUpdateEvent} from '../../../services/local/work';
import {Subscription} from 'rxjs/Subscription';
import {Column} from "../../../table/models";

@Component({
  selector: 'information-expenses',
  templateUrl: './index.html',
  styleUrls: ['./styles.scss']
})
export class InformationExpensesComponent extends BaseTab {

    _update: Subscription;
    _delete: Subscription;

    columns: Column[] = [
        {name: 'date', title: 'Дата'},
        {name: 'project', title: 'Проект'},
        {name: 'workspace', title: 'Место'},
        {name: 'operation', title: 'Операция'},
        {name: 'material', title: 'Материал'},
        {name: 'quantity', title: 'Количество'},
        {name: 'dimension', title: 'Размерность'}
    ];
    displayedColumns = this.columns.map(x => x.name);

    constructor(public store: AngularFirestore, public snackbar: MatSnackBar, public dialog: MatDialog,
                public workerWorksService: WorkerWorksService, public workService: WorkService) {
        super(store, snackbar);
        this._update = workService._update.asObservable()
            .subscribe((update: WorkUpdateEvent) => {
                for (let i = this.items.data.length - 1; i >= 0; i--) {
                    if (this.items.data[i]._folder === update._folder && this.items.data[i]._work == update._uid) {
                        this.items.data[i].date = update.date;
                        this.items.data[i].project = update.project;
                        this.items.data[i].workspace = update.workspace;
                        this.items.data[i].operation = update.operation;
                    }
                }
                this.items._updateChangeSubscription();
            });
        this._delete = workService._delete.asObservable()
            .subscribe((del: WorkDeleteEvent) => {
                for (let i = this.items.data.length - 1; i >= 0; i--) {
                    if (this.items.data[i]._folder === del._folder && this.items.data[i]._work == del._uid) {
                        this.items.data.splice(i, 1);
                    }
                }
                this.items._updateChangeSubscription();
            });
    }

    onRowEvent(event: any) {
        console.log(event);
        this._subscription = this.workerWorksService._document.asObservable()
            .subscribe((work: WorkModel) => {
                if (work != null) {
                    if (event.hasOwnProperty('del')) {
                        for (let e = 0; e < work.expenses.length; e++) {
                            if (work.expenses[e].material === event.del.material &&
                                work.expenses[e].quantity === event.del.quantity &&
                                work.expenses[e].dimension === event.del.dimension) {
                                work.expenses.splice(e, 1);
                                break;
                            }
                        }
                        delete work._uid;
                        this.updateDocument(`workers/${event.del._folder}/works/`, event.del._work, work)
                            .then(() => {
                                this.items.data.splice(event.position, 1);
                                this.items._updateChangeSubscription();
                                this.workerWorksService._suggestion.next(true);
                                this.showSnackbar("Затраты удалены. Для корректной работы потребуется обновление данных");
                            })
                            .catch((error) => {
                                console.log(error);
                                this.showSnackbar("Не удалось удалить затраты");
                            });
                    } else if (event.hasOwnProperty('edit')) {
                        if (!work.hasOwnProperty('_worker')) {
                            work._worker = event.edit._worker;
                        }
                        let position = -1;
                        for (let e = 0; e < work.expenses.length; e++) {
                            if (work.expenses[e].material === event.edit.material &&
                                work.expenses[e].dimension === event.edit.dimension) {
                                // may be number or string type
                                if (work.expenses[e].quantity == event.edit.quantity) {
                                    position = e;
                                    break;
                                }
                            }
                        }
                        if (position < 0) {
                            this.showSnackbar("Ошибка при обработке затрат");
                            return;
                        }
                        this.dialog.open(DialogEditWorkerWork, {
                            width: '280px',
                            data: {
                                tab: 2,
                                position: position,
                                work: work
                            }
                        }).afterClosed().subscribe(result => {
                            if (result == null) {
                                return;
                            }
                            const quantity = work.expenses[position].quantity.replace(',', '.');
                            if (isNaN(+quantity)) {
                                this.showSnackbar("Невалидное количество материала");
                                return;
                            }
                            delete work._uid;
                            console.log(result);
                            this.updateDocument(`workers/${event.edit._folder}/works/`, event.edit._work, work)
                                .then(() => {
                                    this.items.data[event.position].material = work.expenses[position].material;
                                    this.items.data[event.position].quantity = quantity;
                                    this.items.data[event.position].dimension = work.expenses[position].dimension;
                                    this.items._updateChangeSubscription();
                                    this.workerWorksService._suggestion.next(true);
                                    this.showSnackbar("Затраты отредактированы. Для корректной работы потребуется обновление данных");
                                })
                                .catch((error) => {
                                    console.log(error);
                                    this.showSnackbar("Не удалось отредактировать затраты");
                                });
                        });
                    }
                } else {
                    this.showSnackbar("Затраты были удалены. Попробуйте обновить данные");
                }
                if (this._subscription != null) {
                    this._subscription.unsubscribe();
                    this._subscription = null;
                }
            });
        if (event.hasOwnProperty('del')) {
            this.showSnackbar("Подождите...");
            this.workerWorksService.loadSingle(event.del._folder, event.del._work);
        } else if (event.hasOwnProperty('edit')) {
            this.workerWorksService.loadSingle(event.edit._folder, event.edit._work);
        } else {
            this.showSnackbar("Что-то не так");
        }
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        if (this._update != null) {
            this._update.unsubscribe();
        }
        if (this._delete != null) {
            this._delete.unsubscribe();
        }
    }
}

@NgModule({
    imports: [
        MasterTableModule,
        MatSnackBarModule
    ],
    exports: [InformationExpensesComponent],
    entryComponents: [
        DialogEditWorkerWork
    ],
    declarations: [
        InformationExpensesComponent
    ]
})
export class InformationExpensesModule {}
