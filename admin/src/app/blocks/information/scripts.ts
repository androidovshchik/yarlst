import {ChangeDetectorRef, Component, NgModule, OnDestroy, ViewChild} from '@angular/core';
import {
    MatButtonModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatTabsModule
} from '@angular/material';
import {InformationEventsComponent, InformationEventsModule} from './events/scripts';
import {InformationExpensesComponent, InformationExpensesModule} from './expenses/scripts';
import {InformationWorksComponent, InformationWorksModule} from './works/scripts';
import {BaseBlock} from '../base/block';
import {WorkerModel, WorkersService} from '../../services/workers';
import {WorkerWorksService, WorkModel} from '../../services/workers/works';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {DialogEditWorkerWork} from '../../dialogs/work/scripts';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {TextMaskModule} from 'angular2-text-mask';
import {Column, WorkerItem} from "../../table/models";

@Component({
    selector: 'information',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class InformationComponent extends BaseBlock implements OnDestroy {

    @ViewChild(InformationWorksComponent)
    worksComponent: InformationWorksComponent;
    @ViewChild(InformationEventsComponent)
    eventsComponent: InformationEventsComponent;
    @ViewChild(InformationExpensesComponent)
    expensesComponent: InformationExpensesComponent;

    _workers: Subscription;
    _works: Subscription;

    constructor(public router: Router, public route: ActivatedRoute, public workerWorksService: WorkerWorksService,
                public workersService: WorkersService, private cdr: ChangeDetectorRef) {
        super(router, route);
    }

    ngOnInit() {
        super.ngOnInit();
        setTimeout(() => {
            this.cdr.detach();
            let hasAttached: boolean = false;
            this._workers = this.workersService._documents.asObservable()
                .subscribe(worker => {
                    if (worker != null) {
                        this.parseWorker(worker);
                    } else {
                        for (let i = 0; i < this.worksComponent.iconColumns.length; i++) {
                            this.worksComponent.displayedColumns.push('__icon' + (i + 1));
                        }
                        this._works = this.workerWorksService._refresh.asObservable()
                            .switchMap(() => {
                                if (this._works != null) {
                                    this._works.unsubscribe();
                                }
                                this.worksComponent.items.data = [];
                                this.eventsComponent.items.data = [];
                                this.expensesComponent.items.data = [];
                                if (hasAttached) {
                                    this.cdr.detach();
                                }
                                this.cdr.detectChanges();
                                return this.workerWorksService._documents.asObservable();
                            })
                            .subscribe(work => {
                                if (work != null) {
                                    this.parseWorkerWork(work);
                                } else {
                                    this.worksComponent.items._updateChangeSubscription();
                                    this.eventsComponent.items._updateChangeSubscription();
                                    this.expensesComponent.items._updateChangeSubscription();
                                    this.cdr.detectChanges();
                                    this.cdr.reattach();
                                    hasAttached = true;
                                }
                            });
                    }
                });
        }, 0);
    }

    parseWorker(worker: WorkerModel) {
        const names = ['__b' + worker._uid, '__e' + worker._uid, '__d' + worker._uid];
        const titles = ['Начало', 'Конец', 'Длительность'];
        const workerItem: WorkerItem = {
            _uid: worker._uid,
            name: worker.hasOwnProperty("name") ? worker.name : worker.phone
        };
        this.worksComponent.workers.push(workerItem);
        this.eventsComponent.workers.push(workerItem);
        for (let n = 0; n < names.length; n++) {
            const column: Column = {name: names[n], title: titles[n]};
            this.worksComponent.columns.push(column);
            this.eventsComponent.columns.push(column);
            this.worksComponent.displayedColumns.push(names[n]);
            this.eventsComponent.displayedColumns.push(names[n]);
        }
    }

    parseWorkerWork(work: WorkModel) {
        const workersLength = this.worksComponent.workers.length;
        const workEndDate = BaseBlock.getDate(work.events[0].end);
        const workRow = {
            _folder: work._folder,
            _photos: work.photos,
            _worker: work._worker,
            _work: work._uid,
            _comment: work.comment,
            _timestamp: work.events[0].end,
            date: workEndDate,
            project: work.project,
            workspace: work.workspace,
            operation: work.operation
        };
        for (let w = 0; w < workersLength; w++) {
            const worker = this.worksComponent.workers[w]._uid;
            const current = work._worker === worker;
            workRow['__b' + worker] = current ? BaseBlock.getTime(work.events[0].start) : '';
            workRow['__e' + worker] = current ? BaseBlock.getTime(work.events[0].end) : '';
            workRow['__d' + worker] = current ? BaseBlock.getDuration(work.events[0].end - work.events[0].start) : '';
        }
        this.worksComponent.items.data.splice(BaseBlock.findInsertPoint(this.worksComponent.items.data,
            workRow), 0, workRow);
        for (let e = 1; e < work.events.length; e++) {
            const eventRow = {
                _folder: work._folder,
                _worker: work._worker,
                _work: work._uid,
                _timestamp: work.events[0].end,
                _start: work.events[e].start,
                _end: work.events[e].end,
                date: workEndDate,
                event: work.events[e].type
            };
            for (let ew = 0; ew < this.eventsComponent.workers.length; ew++) {
                const worker = this.eventsComponent.workers[ew]._uid;
                const current = work._worker === worker;
                eventRow['__b' + worker] = current ? BaseBlock.getTime(work.events[e].start) : '';
                eventRow['__e' + worker] = current ? BaseBlock.getTime(work.events[e].end) : '';
                eventRow['__d' + worker] = current ? BaseBlock.getDuration(work.events[e].end - work.events[e].start) : '';
            }
            this.eventsComponent.items.data.splice(BaseBlock.findInsertPoint(this.eventsComponent.items.data,
                eventRow), 0, eventRow);
        }
        for (let e = 0; e < work.expenses.length; e++) {
            const expenseRow = {
                _folder: work._folder,
                _worker: work._worker,
                _work: work._uid,
                _timestamp: work.events[0].end,
                date: workEndDate,
                project: work.project,
                workspace: work.workspace,
                operation: work.operation,
                material: work.expenses[e].material,
                quantity: work.expenses[e].quantity,
                dimension: work.expenses[e].dimension
            };
            this.expensesComponent.items.data.splice(BaseBlock.findInsertPoint(this.expensesComponent.items.data,
                expenseRow), 0, expenseRow);
        }
    }

    ngOnDestroy() {
        if (this._works != null) {
            this._works.unsubscribe();
        }
        if (this._workers != null) {
            this._workers.unsubscribe();
        }
    }
}

@NgModule({
    imports: [
        MatTabsModule,
        InformationEventsModule,
        InformationExpensesModule,
        InformationWorksModule,
        MatDialogModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatSelectModule,
        MatOptionModule,
        MatInputModule,
        MatButtonModule,
        TextMaskModule,
        CommonModule,
        FormsModule
    ],
    exports: [InformationComponent],
    declarations: [
        InformationComponent,
        DialogEditWorkerWork
    ]
})
export class InformationModule {}
