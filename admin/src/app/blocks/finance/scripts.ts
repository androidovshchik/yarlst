import {Component, NgModule} from '@angular/core';
import {BaseBlock} from '../base/block';
import {ActivatedRoute, Router} from "@angular/router";
import {MatTabsModule} from "@angular/material";
import {FinanceExpensesModule} from "./expenses/scripts";
import {FinanceIncomesModule} from "./incomes/scripts";
import {FinanceCategoriesModule} from "./categories/scripts";
import {FinanceSubcategoriesModule} from "./subcategories/scripts";

@Component({
    selector: 'finance',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class FinanceComponent extends BaseBlock {

    constructor(public router: Router, public route: ActivatedRoute) {
        super(router, route);
    }
}

@NgModule({
    imports: [
        MatTabsModule,
        FinanceExpensesModule,
        FinanceIncomesModule,
        FinanceCategoriesModule,
        FinanceSubcategoriesModule
    ],
    exports: [FinanceComponent],
    declarations: [FinanceComponent]
})
export class FinanceModule {}
