import {ChangeDetectorRef, Component, NgModule, OnInit} from '@angular/core';
import {BaseTab} from '../../base/tab';
import {MasterTableModule} from '../../../table/scripts';
import {MatSnackBar, MatSnackBarModule} from '@angular/material';
import {AngularFirestore} from 'angularfire2/firestore';
import {MaterialsService} from '../../../services/materials';
import {Column, InputType} from "../../../table/models";

@Component({
  selector: 'finance-expenses',
  templateUrl: './index.html',
  styleUrls: ['./styles.scss']
})
export class FinanceExpensesComponent extends BaseTab implements OnInit {

    columns: Column[] = [
        {name: 'timestamp', title: 'Дата', input: InputType.DATE},
        {name: 'name', title: 'Наименование', changeable: false, input: InputType.TEXT},
        {name: 'total', title: 'Стоимость', input: InputType.TEXT},
        {name: 'quantity', title: 'Количество', input: InputType.TEXT},
        {name: '__price', title: 'Цена', input: InputType.FUNCTION,
            calculate: function(cell): string {
                if (!cell.total || !cell.quantity) {
                    return '0 ₽';
                }
                return Math.round(cell.total / cell.quantity) + ' ₽';
            }
        },
        {name: 'place', title: 'Место покупки', input: InputType.TEXT},
        {name: 'comment', title: 'Комментарий', input: InputType.TEXT}
    ];
    displayedColumns = this.columns.map(x => x.name);

    constructor(public store: AngularFirestore, public snackbar: MatSnackBar, public materialsService: MaterialsService,
                public cdr: ChangeDetectorRef) {
        super(store, snackbar);
    }

    ngOnInit() {
        /*setTimeout(() => {
            this.cdr.detach();
            this._subscription = this.materialsService._documents.asObservable()
                .subscribe((model: MaterialModel) => {
                    if (model != null) {
                        this.items.data.push(model);
                    } else {
                        this.items._updateChangeSubscription();
                        this.cdr.detectChanges();
                        this.cdr.reattach();
                    }
                });
        }, 0);*/
    }
}

@NgModule({
  imports: [
      MasterTableModule,
      MatSnackBarModule
  ],
  exports: [FinanceExpensesComponent],
  declarations: [FinanceExpensesComponent]
})
export class FinanceExpensesModule {}
