import {HostBinding, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatTabChangeEvent} from '@angular/material';

export class BaseBlock implements OnInit {

    @HostBinding('@.disabled')
    public animationsDisabled = true;

    tab: number = 0;

    constructor(public router: Router, public route: ActivatedRoute) {}

    ngOnInit() {
        this.tab = this.route.snapshot.params['tab'];
    }

    onTab(event: MatTabChangeEvent) {
        this.tab = event.index;
        const url = decodeURIComponent(this.router.url);
        this.router.navigate([url.substr(0, url.lastIndexOf("/")), event.index]);
    }

    static getDuration(difference): string {
        const structure = {day: 86400, hour: 3600, minute: 60};
        const dimensions = {day: ' д ', hour: ' ч ', minute: ' м'};
        difference = Math.abs(difference) / 1000;
        let duration: string = "";
        Object.keys(structure).forEach(function(key) {
            let count: number = Math.floor(difference / structure[key]);
            difference -= count * structure[key];
            if (count > 0 || duration === "" && key === 'minute') {
                duration += count + dimensions[key];
            }
        });
        return duration;
    }

    static getDate(timestamp): string {
        let date = new Date(timestamp);
        let year = date.getFullYear();
        let month = ("0" + (date.getMonth() + 1)).substr(-2);
        let day = ("0" + date.getDate()).substr(-2);
        return day + "." + month + "." + year;
    }

    static getTime(timestamp): string {
        let date = new Date(timestamp);
        let hour = ("0" + date.getHours()).substr(-2);
        let minutes = ("0" + date.getMinutes()).substr(-2);
        return hour + ":" + minutes;
    }

    // e.g. '17.09.2013 10:08'
    static datetime2Timestamp(datetime: string): number {
        const datetimeParts = datetime.split(' '), timeParts = datetimeParts[1].split(':'),
            dateParts = datetimeParts[0].split('.');
        let date = new Date(+dateParts[2], +dateParts[1] - 1, +dateParts[0], +timeParts[0], +timeParts[1]);
        return date.getTime();
    }

    // Due to https://firebase.google.com/docs/firestore/quotas
    static validDocId(docId): string {
        docId = docId.replace(/\./g, "");
        docId = docId.replace(/\//g, "");
        docId = docId.length > 1500 ? docId.substring(0, 1500) : docId;
        return docId;
    }

    static findInsertPoint(array, value): number {
        let low = 0, high = array.length, mid = -1, c = 0;
        while (low < high)   {
            mid = Math.floor((low + high) / 2);
            c = BaseBlock.timestampComparator(array[mid], value);
            if (c < 0)   {
                low = mid + 1;
            } else if(c > 0) {
                high = mid;
            } else {
                return mid;
            }
        }
        return low;
    }

    static timestampComparator = (val1, val2) => {
        return val2._timestamp - val1._timestamp;
    };
}