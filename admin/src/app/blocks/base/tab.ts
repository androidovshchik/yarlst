import {OnDestroy} from '@angular/core';
import {MatSnackBar, MatTableDataSource} from '@angular/material';
import {AngularFirestore} from 'angularfire2/firestore';
import {Subscription} from 'rxjs/Subscription';
import {WorkerItem} from "../../table/models";

export class BaseTab implements OnDestroy {

    _subscription: Subscription;

    items: MatTableDataSource<any> = new MatTableDataSource();

    workers: WorkerItem[] = [];

    constructor(public store: AngularFirestore, public snackbar: MatSnackBar) {}

    updateDocument(collection: string, document: string, object: object, merge: boolean = true): Promise<void> {
        return this.store.collection(collection)
            .doc(document).ref
            .set(object, {merge: merge});
    }

    deleteDocument(collection: string, document: string): Promise<void> {
        return this.store.collection(collection)
            .doc(document)
            .delete();
    }

    showSnackbar = (message) => {
        this.snackbar.open(message, null,{
            duration: 1500
        });
    };

    ngOnDestroy() {
        if (this._subscription != null) {
            this._subscription.unsubscribe();
        }
    }
}