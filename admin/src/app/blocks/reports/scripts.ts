import {Component, NgModule} from '@angular/core';
import {BaseBlock} from '../base/block';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'reports',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class ReportsComponent extends BaseBlock {

    constructor(public router: Router, public route: ActivatedRoute) {
        super(router, route);
    }
}

@NgModule({
    imports: [],
    exports: [ReportsComponent],
    declarations: [ReportsComponent]
})
export class ReportsModule {}
