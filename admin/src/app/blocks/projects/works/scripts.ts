import {Component, Input, NgModule, OnInit, ViewChild} from '@angular/core';
import {BaseTab} from '../../base/tab';
import {MatSnackBar, MatSnackBarModule, MatTableDataSource} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {MasterTableComponent, MasterTableModule} from '../../../table/scripts';
import {CommonModule} from '@angular/common';
import {AngularFirestore} from 'angularfire2/firestore';
import {WorkerWorksService, WorkModel} from '../../../services/workers/works';
import {WorkerModel, WorkersService} from '../../../services/workers';
import {Subscription} from 'rxjs/Subscription';
import {ProjectWorkPerm, ProjectWorksService} from '../../../services/projects/works';
import {BaseBlock} from '../../base/block';
import {BaseService} from '../../../services/base';
import {Column, InputType, WorkerItem} from "../../../table/models";

@Component({
    selector: 'projects-works',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class ProjectsWorksComponent extends BaseTab implements OnInit {

    @ViewChild(MasterTableComponent)
    masterTable: MasterTableComponent;

    @Input()
    projectUid: string;

    @Input()
    projectName: string;

    @Input()
    collection: string;

    _objects: Subscription;
    @Input()
    _workers: Subscription;
    @Input()
    _workerWorks: Subscription;

    @Input()
    myItems: MatTableDataSource<ProjectWorkItem>;

    @Input()
    myColumns: Column[];
    @Input()
    myDisplayedColumns: string[];

    @Input()
    myWorkers: WorkerItem[];

    constructor(public store: AngularFirestore, public snackbar: MatSnackBar, public workersService: WorkersService,
                public workerWorksService: WorkerWorksService, public projectWorksService: ProjectWorksService) {
        super(store, snackbar);
    }

    ngOnInit() {
        const worksPerm: ProjectWorkPerm[] = [];
        if (this.collection === 'workshops') {
            this._subscription = this.projectWorksService._workshops.asObservable()
                .subscribe((model: ProjectWorkPerm) => {
                    if (model != null) {
                        worksPerm.push(model);
                    } else {
                        this._workers = this.loadWorkers(worksPerm);
                    }
                });
        } else {
            this._objects = this.projectWorksService._objects.asObservable()
                .subscribe((model: ProjectWorkPerm) => {
                    if (model != null) {
                        worksPerm.push(model);
                    } else {
                        this._workers = this.loadWorkers(worksPerm);
                    }
                });
        }
        this.projectWorksService.loadPart(this.projectUid, this.collection, null);
    }

    loadWorkers(worksPerm: ProjectWorkPerm[]): Subscription {
        const allWorkers: WorkerModel[] = [];
        return this.workersService._documents.asObservable()
            .subscribe(worker => {
                if (worker != null) {
                    allWorkers.push(worker);
                } else {
                    this._workerWorks = this.loadWorkerWorks(worksPerm, allWorkers);
                }
            });
    }

    loadWorkerWorks(worksPerm: ProjectWorkPerm[], allWorkers: WorkerModel[]): Subscription {
        let projectWorkers: string[], laterUpdate: boolean;
        return this.workerWorksService._documents.asObservable()
            .subscribe(newWork => {
                if (!laterUpdate) {
                    laterUpdate = true;
                    setTimeout(() => {
                        this.myItems._updateChangeSubscription();
                    }, 0);
                }
                if (newWork != null) {
                    if (newWork.project.replace(/\//g, "") === this.projectName &&
                        newWork.workspace === (this.collection === 'workshops' ? 'Цех' : 'Объект')) {
                        let p = 0, newWorker: boolean = false;
                        for (; p < projectWorkers.length; p++) {
                            if (projectWorkers[p] === newWork._worker) {
                                break;
                            }
                        }
                        if (p === projectWorkers.length) {
                            projectWorkers.push(newWork._worker);
                            for (let a = 0; a < allWorkers.length; a++) {
                                if (allWorkers[a]._uid === newWork._worker) {
                                    newWorker = true;
                                    this.parseWorker(allWorkers[a]);
                                    break;
                                }
                            }
                        }
                        this.parseWorkerWork(worksPerm, projectWorkers, newWorker, newWork);
                    }
                } else {
                    this.masterTable.onEmitDataEvents();
                }
            });
    }

    parseWorker(worker: WorkerModel) {
        const names = ['__r' + worker._uid, '__d' + worker._uid, '__y' + worker._uid];
        const titles = ['Участие', 'Длительность', 'Сумма'];
        this.myWorkers.push({
            _uid: worker._uid,
            name: worker.hasOwnProperty("name") ? worker.name : worker.phone
        });
        const columns: Column[] = [
            {name: names[0], title: titles[0]},
            {name: names[1], title: titles[1]},
            {name: names[2], title: titles[2], input: InputType.FUNCTION,
                calculate: function(cell): string {
                    if (cell.payment == null || !cell[names[0]]) {
                        return '';
                    }
                    return Math.round(cell.payment * +cell[names[0]].replace('%', '') / 100) + ' ₽';
                }
            }
        ];
        for (let c = 0; c < columns.length; c++) {
            this.myColumns.push(columns[c]);
            this.myDisplayedColumns.push(columns[c].name);
        }
    }

    parseWorkerWork(worksPerm: ProjectWorkPerm[], projectWorkers: string[], newWorker: boolean, newWork: WorkModel) {
        const newWorkEndDate = BaseBlock.getDate(newWork.events[0].end);
        const newWorkDurationMillis = newWork.events[0].end - newWork.events[0].start;
        if (this.myItems.data.length <= 0) {
            // zero case
            this.writeMyItemRow(projectWorkers, this.insertNewWorkerWork(worksPerm, newWork));
            return;
        }
        for (let m = 0, hasTheSame = false; m < this.myItems.data.length; m++) {
            // found the same row (date and operation)
            if (this.myItems.data[m]._date === newWorkEndDate && this.myItems.data[m]._operation === newWork.operation) {
                hasTheSame = true;
                this.myItems.data[m]._duration += newWorkDurationMillis;
                let w = 0;
                // adding or updating worker of current row (date and operation)
                for (; w < this.myItems.data[m]._workers.length; w++) {
                    if (this.myItems.data[m]._workers[w]._worker === newWork._worker) {
                        this.myItems.data[m]._workers[w]._duration += newWorkDurationMillis;
                        break;
                    }
                }
                if (w === this.myItems.data[m]._workers.length) {
                    this.myItems.data[m]._workers.push({_worker: newWork._worker, _duration: newWorkDurationMillis});
                }
                if (!newWorker) {
                    this.writeMyItemRow(projectWorkers, m);
                    return;
                }
            }
            // new row (date and operation)
            if (!hasTheSame && m === this.myItems.data.length - 1) {
                this.writeMyItemRow(projectWorkers, this.insertNewWorkerWork(worksPerm, newWork));
                return;
            }
            // if with current work was added new worker
            // it's important to override all rows
            if (newWorker) {
                this.writeMyItemRow(projectWorkers, m);
            }
        }
    }

    insertNewWorkerWork(worksPerm: ProjectWorkPerm[], newWork: WorkModel): number {
        const newWorkEndDate = BaseBlock.getDate(newWork.events[0].end);
        const newWorkDurationMillis = newWork.events[0].end - newWork.events[0].start;
        let prod, uid, payment;
        prod = uid = payment = null;
        for (let w = 0; w < worksPerm.length; w++) {
            if (worksPerm[w]._date === newWorkEndDate && worksPerm[w]._operation === newWork.operation) {
                prod = worksPerm[w]._prod;
                uid = worksPerm[w]._uid;
                payment = worksPerm[w].payment;
                break;
            }
        }
        const workRow = {
            _timestamp: newWork.events[0].end,
            _prod: prod === null ? BaseService.isProd() : prod,
            _uid: uid === null ? MasterTableComponent.generateUid() : uid,
            _date: newWorkEndDate,
            _operation: newWork.operation,
            payment: payment,
            _duration: newWorkDurationMillis,
            _workers: [{_worker: newWork._worker, _duration: newWorkDurationMillis}]
        };
        let point = BaseBlock.findInsertPoint(this.myItems.data, workRow);
        this.myItems.data.splice(point, 0, workRow);
        return point;
    }

    writeMyItemRow(projectWorkers: string[], m: number) {
        const commonDuration = this.myItems.data[m]._duration;
        // projectWorkers includes all workers of project
        for (let p = 0; p < projectWorkers.length; p++) {
            const names = ['__r' + projectWorkers[p], '__d' + projectWorkers[p]];
            let w = 0;
            // this.myItems.data[m]._workers includes all workers of current row (date and operation)
            for (; w < this.myItems.data[m]._workers.length; w++) {
                if (this.myItems.data[m]._workers[w]._worker === projectWorkers[p]) {
                    const workerDuration = this.myItems.data[m]._workers[w]._duration;
                    // duration less than 1 minute is not calculated
                    if (workerDuration < 60000) {
                        this.myItems.data[m][names[0]] = '';
                        this.myItems.data[m][names[1]] = BaseBlock.getDuration(workerDuration);
                        return;
                    }
                    this.myItems.data[m][names[0]] = Math.round((workerDuration / commonDuration) * 100) + '%';
                    this.myItems.data[m][names[1]] = BaseBlock.getDuration(workerDuration);
                    break;
                }
            }
            // cases when worker of project does not participate in current row
            if (w === this.myItems.data[m]._workers.length) {
                this.myItems.data[m][names[0]] = '';
                this.myItems.data[m][names[1]] = '';
            }
        }
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        if (this._objects != null) {
            this._objects.unsubscribe();
        }
        if (this._workers != null) {
            this._workers.unsubscribe();
        }
        if (this._workerWorks != null) {
            this._workerWorks.unsubscribe();
        }
    }
}

export interface ProjectWorkItem {
    _timestamp: number,
    _date: string,
    _operation: string,
    payment: number,
    _duration: number,
    _workers: ProjectWorkWorker[]
}

export interface ProjectWorkWorker {
    _worker: string,
    _duration: number
}

@NgModule({
    imports: [
        MatSnackBarModule,
        FormsModule,
        CommonModule,
        MasterTableModule
    ],
    exports: [ProjectsWorksComponent],
    declarations: [ProjectsWorksComponent]
})
export class ProjectsWorksModule {}
