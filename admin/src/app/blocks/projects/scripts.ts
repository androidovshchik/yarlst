import {Component, ElementRef, NgModule, OnDestroy, ViewChild} from '@angular/core';
import {BaseBlock} from '../base/block';
import {MatTableDataSource, MatTabsModule} from '@angular/material';
import {ProjectsFuelComponent, ProjectsFuelModule} from './fuel/scripts';
import {ProjectsInformationComponent, ProjectsInformationModule} from './information/scripts';
import {ProjectsMaterialsModule} from './materials/scripts';
import {ProjectsOthersComponent, ProjectsOthersModule} from './others/scripts';
import {ProjectsWorksModule, ProjectWorkItem} from './works/scripts';
import {CommonModule} from '@angular/common';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, Router} from '@angular/router';
import {ProjectModel, ProjectsService} from '../../services/projects';
import {ProjectsTempsComponent, ProjectsTempsModule} from './temps/scripts';
import {Column, InputType, WorkerItem} from "../../table/models";

@Component({
    selector: 'projects',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class ProjectsComponent extends BaseBlock implements OnDestroy {

    @ViewChild('tabsContainer', {read: ElementRef})
    matTabGroup;
    @ViewChild(ProjectsInformationComponent)
    informationComponent: ProjectsInformationComponent;
    @ViewChild(ProjectsTempsComponent)
    tempsComponent: ProjectsTempsComponent;
    @ViewChild(ProjectsFuelComponent)
    fuelComponent: ProjectsFuelComponent;
    @ViewChild(ProjectsOthersComponent)
    othersComponent: ProjectsOthersComponent;

    correctProject: boolean = true;

    _project: Subscription;

    projectUid: string;

    projectName: string;

    _workersWorkshop: Subscription;
    _workersObject: Subscription;
    _workerWorksWorkshop: Subscription;
    _workerWorksObject: Subscription;
    itemsWorkshop: MatTableDataSource<ProjectWorkItem> = new MatTableDataSource();
    itemsObject: MatTableDataSource<ProjectWorkItem> = new MatTableDataSource();
    workersWorkshop: WorkerItem[] = [];
    workersObject: WorkerItem[] = [];
    columnsWorkshop: Column[] = [
        {name: '_date', title: 'Дата'},
        {name: '_operation', title: 'Операция'},
        {name: 'payment', title: 'Общая сумма', input: InputType.FLOAT, dimension: "₽"}
    ];
    displayedWorkshopColumns = this.columnsWorkshop.map(x => x.name);
    columnsObject: Column[] = [
        {name: '_date', title: 'Дата'},
        {name: '_operation', title: 'Операция'},
        {name: 'payment', title: 'Общая сумма', input: InputType.FLOAT, dimension: "₽"}
    ];
    displayedObjectColumns = this.columnsObject.map(x => x.name);

    constructor(public router: Router, public route: ActivatedRoute, public projectService: ProjectsService) {
        super(router, route);
    }

    ngOnInit() {
        super.ngOnInit();
        this.projectUid = BaseBlock.validDocId(this.route.snapshot.params['uid']);
        this.projectName = this.route.snapshot.params['name'];
        this._project = this.projectService._document.asObservable()
            .subscribe(project => {
                if (project == null) {
                    this.matTabGroup.nativeElement.style.display = 'none';
                    this.correctProject = false;
                    return;
                }
                this.parseProject(project);
            });
        this.projectService.loadSingle(this.projectUid);
    }

    parseProject(project: ProjectModel) {
        this.informationComponent.costProject = project.cost;
        for (let t = 0; t < project.temps.length; t++) {
            let timestamp = project.temps[t].timestamp == null ? null : new Date(project.temps[t].timestamp);
            this.tempsComponent.items.data.push({
                timestamp: timestamp,
                text: project.temps[t].text,
                payment: project.temps[t].payment
            });
        }
        this.fuelComponent.location = project.location;
        this.fuelComponent.distance = project.distance;
        this.fuelComponent.items.data = project.fuel;
        for (let o = 0; o < project.others.length; o++) {
            let timestamp = project.others[o].timestamp == null ? null : new Date(project.others[o].timestamp);
            this.othersComponent.items.data.push({
                timestamp: timestamp,
                description: project.others[o].description,
                payment: project.others[o].payment,
            });
        }
        this.tempsComponent.items._updateChangeSubscription();
        this.fuelComponent.items._updateChangeSubscription();
        this.othersComponent.items._updateChangeSubscription();
    }

    ngOnDestroy() {
        if (this._project != null) {
            this._project.unsubscribe();
        }
    }
}

@NgModule({
    imports: [
        ProjectsInformationModule,
        ProjectsWorksModule,
        ProjectsTempsModule,
        ProjectsMaterialsModule,
        ProjectsFuelModule,
        ProjectsOthersModule,
        MatTabsModule,
        CommonModule
    ],
    exports: [ProjectsComponent],
    declarations: [ProjectsComponent]
})
export class ProjectsModule {}
