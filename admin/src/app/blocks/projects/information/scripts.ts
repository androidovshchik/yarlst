import {Component, Input, NgModule, OnInit} from '@angular/core';
import {BaseTab} from '../../base/tab';
import {
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatSnackBar,
    MatSnackBarModule
} from '@angular/material';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ProjectModel, ProjectsService} from '../../../services/projects';
import {Subscription} from 'rxjs/Subscription';
import {TableEvent, TableService} from '../../../services/local/table';
import {AngularFirestore} from 'angularfire2/firestore';

@Component({
    selector: 'projects-information',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class ProjectsInformationComponent extends BaseTab implements OnInit {

    @Input()
    projectUid: string;

    _data: Subscription;
    workshopDurations?: number = null;
    objectDurations?: number = null;

    costProject?: number = null;

    list: any[] = [
        {name: 'Расходы по цеху', amount: null},
        {name: 'Расходы по объекту', amount: null},
        {name: 'Расходы на постоянных работников', amount: null},
        {name: 'Расходы на временных работников', amount: null},
        {name: 'Расходы на оплату труда', amount: null},
        {name: 'Расходы на материалы', amount: null},
        {name: 'Расходы на топливо', amount: null},
        {name: 'Расходы на прочее', amount: null},
        {name: 'Расходы по проекту', amount: null},
        {name: 'Стоимость проекта'},
        {name: 'Прибыль', amount: null},
        {name: 'Трудозатраты', amount: null, dimension: 'чел/дн'}
    ];

    constructor(public store: AngularFirestore, public snackbar: MatSnackBar, public tableService: TableService) {
        super(store, snackbar);
    }

    ngOnInit() {
        this._subscription = this.store.doc<ProjectModel>("projects/" + this.projectUid)
            .valueChanges()
            .subscribe((model: ProjectModel) => {
                model = ProjectsService.validProject(model);
                this.list[3].amount = 0;
                for (let t = 0; t < model.temps.length; t++) {
                    this.list[3].amount += model.temps[t].payment;
                }
                this.list[6].amount = 0;
                for (let f = 0; f < model.fuel.length; f++) {
                    this.list[6].amount += (!model.distance ? 0 : model.distance) * 2 * model.fuel[f].trips
                        * model.fuel[f].expense / 100 * model.fuel[f].price;
                }
                this.list[7].amount = 0;
                for (let o = 0; o < model.others.length; o++) {
                    this.list[7].amount += model.others[o].payment;
                }
                this.list[9].amount = model.cost;
                this.calculateCommon();
            }, error => {
                console.log(error);
            });
        this._data = this.tableService._event.asObservable()
            .filter((event: TableEvent) => event.path.indexOf(this.projectUid) !== -1)
            .subscribe((event: TableEvent) => {
                if (event.path.indexOf('workshops') !== -1) {
                    this.workshopDurations = 0;
                    this.list[0].amount = 0;
                    for (let a = 0; a < event.array.length; a++) {
                        this.workshopDurations += event.array[a]['_duration'];
                        this.list[0].amount += event.array[a]['payment'];
                    }
                } else if (event.path.indexOf('objects') !== -1) {
                    this.objectDurations = 0;
                    this.list[1].amount = 0;
                    for (let a = 0; a < event.array.length; a++) {
                        this.objectDurations += event.array[a]['_duration'];
                        this.list[1].amount += event.array[a]['payment'];
                    }
                } else if (event.path.indexOf('materials') !== -1) {
                    this.list[5].amount = 0;
                    for (let a = 0; a < event.array.length; a++) {
                        this.list[5].amount += event.array[a]['_quantity'] * event.array[a]['price'];
                    }
                }
                this.calculateCommon();
            })
    }

    calculateCommon() {
        this.list[2].amount = this.list[0].amount + this.list[1].amount;
        this.list[4].amount = this.list[2].amount + this.list[3].amount;
        this.list[8].amount = this.list[4].amount + this.list[5].amount + this.list[6].amount
            + this.list[7].amount;
        let income = this.list[9].amount - this.list[8].amount;
        this.list[10].amount = income < 0 ? null : income;
        if (this.workshopDurations !== null && this.objectDurations !== null) {
            this.list[11].amount = Math.round((this.workshopDurations + this.objectDurations) / 60 / 8 / 60) / 1000;
        }
    }

    calculateEarnings() {
        let projectCost = this.costProject === null ? null : +this.costProject;
        this.updateDocument('projects', this.projectUid, {cost: projectCost})
            .then(() => this.showSnackbar("Изменения сохранены"))
            .catch((error) => {
                console.log(error);
                this.showSnackbar("Не удалось сохранить изменения");
            });
        let income = projectCost - +this.list[8].amount;
        this.list[10].amount = income < 0 ? null : income;
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        if (this._data != null) {
            this._data.unsubscribe();
        }
    }
}

@NgModule({
    imports: [
        MatListModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        MatSnackBarModule,
        CommonModule,
        FormsModule
    ],
    exports: [ProjectsInformationComponent],
    declarations: [ProjectsInformationComponent]
})
export class ProjectsInformationModule {}