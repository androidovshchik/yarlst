import {Component, Input, NgModule, OnInit, ViewChild} from '@angular/core';
import {BaseTab} from '../../base/tab';
import {MasterTableComponent, MasterTableModule} from '../../../table/scripts';
import {MatSnackBar, MatSnackBarModule} from '@angular/material';
import {AngularFirestore} from 'angularfire2/firestore';
import {ProjectMaterial, ProjectMaterialsService} from '../../../services/projects/materials';
import {WorkerWorksService, WorkModel} from '../../../services/workers/works';
import {Subscription} from 'rxjs/Subscription';
import {BaseService} from '../../../services/base';
import {Column, InputType} from "../../../table/models";

@Component({
    selector: 'projects-materials',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class ProjectsMaterialsComponent extends BaseTab implements OnInit {

    @ViewChild(MasterTableComponent)
    masterTable: MasterTableComponent;

    @Input()
    projectUid: string;

    @Input()
    projectName: string;

    _works: Subscription;

    columns: Column[] = [
        {name: '_material', title: 'Материал'},
        {name: '_quantity', title: 'Количество'},
        {name: 'dimension', title: 'Размерность'},
        {name: 'price', title: 'Цена', input: InputType.FLOAT, dimension: "₽"},
        {name: 'total', title: 'Стоимость', input: InputType.FUNCTION,
            calculate: function(cell): string {
                return (cell._quantity * (cell.price == null ? 0 : cell.price)) + ' ₽';
            }
        }
    ];
    displayedColumns = this.columns.map(x => x.name);

    constructor(public store: AngularFirestore, public snackbar: MatSnackBar, public workerWorksService: WorkerWorksService,
                public projectMaterialsService: ProjectMaterialsService) {
        super(store, snackbar);
    }

    ngOnInit() {
        let materials: ProjectMaterial[] = [];
        this._subscription = this.projectMaterialsService._documents.asObservable()
            .subscribe((model: ProjectMaterial) => {
                if (model != null) {
                    materials.push(model);
                } else {
                    this._works = this.workerWorksService._documents.asObservable()
                        .subscribe((work: WorkModel) => {
                            if (work != null) {
                                if (work.project.replace(/\//g, "") === this.projectName) {
                                    this.parseExpenses(materials, work);
                                }
                            } else {
                                this.items._updateChangeSubscription();
                                this.masterTable.onEmitDataEvents();
                            }
                        });
                }
            });
        this.projectMaterialsService.loadPart(this.projectUid, null);
    }

    parseExpenses(materials: ProjectMaterial[], work: WorkModel) {
        for (let e = 0, i, prod, uid, price; e < work.expenses.length; e++) {
            let material = work.expenses[e].material;
            for (i = 0; i < this.items.data.length; i++) {
                if (this.items.data[i]._material === material) {
                    this.items.data[i]._quantity += +work.expenses[e].quantity;
                    break;
                }
            }
            if (i === this.items.data.length) {
                prod = uid = price = null;
                for (let m = 0; m < materials.length; m++) {
                    if (materials[m]._material === material) {
                        prod = materials[m]._prod;
                        uid = materials[m]._uid;
                        price = materials[m].price;
                        break;
                    }
                }
                this.items.data.push({
                    _prod: prod === null ? BaseService.isProd() : prod,
                    _uid: uid === null ? MasterTableComponent.generateUid() : uid,
                    _material: material,
                    _quantity: +work.expenses[e].quantity,
                    dimension: work.expenses[e].dimension,
                    price: price
                });
            }
        }
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        if (this._works != null) {
            this._works.unsubscribe();
        }
    }
}

@NgModule({
    imports: [
        MasterTableModule,
        MatSnackBarModule
    ],
    exports: [ProjectsMaterialsComponent],
    declarations: [ProjectsMaterialsComponent]
})
export class ProjectsMaterialsModule {}
