import {Component, Input, NgModule} from '@angular/core';
import {BaseTab} from '../../base/tab';
import {MasterTableModule} from '../../../table/scripts';
import {MatSnackBar, MatSnackBarModule} from '@angular/material';
import {AngularFirestore} from "angularfire2/firestore";
import {Column, InputType} from "../../../table/models";

@Component({
    selector: 'projects-others',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class ProjectsOthersComponent extends BaseTab {

    @Input()
    projectUid: string;

    columns: Column[] = [
        {name: 'timestamp', title: 'Дата', input: InputType.DATE},
        {name: 'description', title: 'Описание', input: InputType.MILTILINE},
        {name: 'payment', title: 'Сумма', input: InputType.FLOAT, dimension: "₽"}
    ];
    displayedColumns = this.columns.map(x => x.name);

    constructor(public store: AngularFirestore, public snackbar: MatSnackBar) {
        super(store, snackbar);
    }
}

@NgModule({
    imports: [
        MasterTableModule,
        MatSnackBarModule
    ],
    exports: [ProjectsOthersComponent],
    declarations: [ProjectsOthersComponent]
})
export class ProjectsOthersModule {}