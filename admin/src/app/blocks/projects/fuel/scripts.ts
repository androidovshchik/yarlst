import {Component, Input, NgModule} from '@angular/core';
import {BaseTab} from '../../base/tab';
import {MatFormFieldModule, MatInputModule, MatSnackBar, MatSnackBarModule} from '@angular/material';
import {MasterTableModule} from '../../../table/scripts';
import {FormsModule} from '@angular/forms';
import {AngularFirestore} from "angularfire2/firestore";
import {Column, InputType} from "../../../table/models";

@Component({
    selector: 'projects-fuel',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class ProjectsFuelComponent extends BaseTab {

    @Input()
    projectUid: string;

    location: string;

    distance?: number = null;

    onLocation() {
        this.updateDocument('projects', this.projectUid, {location: this.location})
            .then(() => this.showSnackbar("Изменения сохранены"))
            .catch((error) => {
                console.log(error);
                this.showSnackbar("Не удалось сохранить изменения");
            });
    }

    calculateDistance() {
        this.updateDocument('projects', this.projectUid,
            {distance: this.distance === null ? null : +this.distance})
            .then(() => this.showSnackbar("Изменения сохранены"))
            .catch((error) => {
                console.log(error);
                this.showSnackbar("Не удалось сохранить изменения");
            });
    }

    columns: Column[] = [
        {name: 'auto', title: 'Автомобиль', input: InputType.TEXT},
        {name: 'trips', title: 'Количество поездок', input: InputType.NUMBER},
        {name: 'expense', title: 'Расход топлива', input: InputType.FLOAT, dimension: "л/100км"},
        {name: 'price', title: 'Цена топлива', input: InputType.FLOAT, dimension: "₽/л"},
        {name: 'total', title: 'Стоимость топлива', input: InputType.FUNCTION,
            calculate: function(cell, factor): string {
                if (cell.trips == null || cell.expense == null || cell.price == null) {
                    return '0 ₽';
                }
                return (factor * 2 * cell.trips * cell.expense / 100 * cell.price) + ' ₽';
            }
        }
    ];
    displayedColumns = this.columns.map(x => x.name);

    constructor(public store: AngularFirestore, public snackbar: MatSnackBar) {
        super(store, snackbar);
    }
}

@NgModule({
    imports: [
        MatFormFieldModule,
        MatInputModule,
        MasterTableModule,
        MatSnackBarModule,
        FormsModule
    ],
    exports: [ProjectsFuelComponent],
    declarations: [ProjectsFuelComponent]
})
export class ProjectsFuelModule {}