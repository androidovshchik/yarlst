import {Component, Input, NgModule} from '@angular/core';
import {BaseTab} from '../../base/tab';
import {MatFormFieldModule, MatSelectModule, MatSnackBar, MatSnackBarModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {MasterTableModule} from '../../../table/scripts';
import {CommonModule} from '@angular/common';
import {AngularFirestore} from "angularfire2/firestore";
import {Column, InputType} from "../../../table/models";

@Component({
    selector: 'projects-temps',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class ProjectsTempsComponent extends BaseTab {

    @Input()
    projectUid: string;

    columns: Column[] = [
        {name: 'timestamp', title: 'Дата', input: InputType.DATE},
        {name: 'text', title: 'Описание', input: InputType.MILTILINE},
        {name: 'payment', title: 'Сумма', input: InputType.FLOAT, dimension: "₽"}
    ];
    displayedColumns = this.columns.map(x => x.name);

    constructor(public store: AngularFirestore, public snackbar: MatSnackBar) {
        super(store, snackbar);
    }
}

@NgModule({
    imports: [
        MatFormFieldModule,
        MatSelectModule,
        MatSnackBarModule,
        FormsModule,
        CommonModule,
        MasterTableModule
    ],
    exports: [ProjectsTempsComponent],
    declarations: [ProjectsTempsComponent]
})
export class ProjectsTempsModule {}
