import {Component, NgModule, OnInit} from '@angular/core';
import {
    MatButtonModule,
    MatCardModule,
    MatDialog,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBar,
    MatSnackBarModule
} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {TextMaskModule} from 'angular2-text-mask';
import {AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {CommonModule} from '@angular/common';
import {BaseBlock} from "../base/block";
import {admins} from "../../settings";

@Component({
    selector: 'login',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class LoginComponent extends BaseBlock implements OnInit {

    phoneMask = ['+', '7', '(', /[1-9]/, /\d/, /\d/, ')', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];
    phone?: string = null;

    recaptcha: firebase.auth.RecaptchaVerifier;
    interval?: any = null;
    timeout?: number = null;

    confirmationResult?: any = null;
    verifyCode?: any = null;

    waitingCode: boolean = false;
    waitingConfirmation: boolean = false;

    constructor(public router: Router, public route: ActivatedRoute, public fireAuth: AngularFireAuth,
                public snackbar: MatSnackBar, public dialog: MatDialog, private auth: AngularFireAuth) {
        super(router, route);
    }
    
    getPhoneLength() {
        return this.phone ? this.phone.replace(/([^0-9]+)/gi, '').length : 0;
    }

    ngOnInit() {
        this.recaptcha = new firebase.auth.RecaptchaVerifier('recaptcha-container');
        this.recaptcha.render()
            .catch(() => this.showSnackbar('Не удалось загрузить капчу'));
        this.interval = setInterval(() => {
            if (this.timeout !== null) {
                this.timeout++;
                if (this.timeout >= 60) {
                    this.resetVerifyCode(false, "Истекло время ожидания");
                }
            }
        }, 1000);
    }

    sendCode() {
        this.fireAuth.auth.signInWithPhoneNumber(this.phone, this.recaptcha)
            .then( result => {
                this.confirmationResult = result;
                this.timeout = 0;
                this.waitingCode = true;
                this.showSnackbar("Отправлен СМС с кодом на телефон");
            })
            .catch(() => {
                this.resetVerifyCode(false, "Не удалось выполнить запрос");
            });
    }

    login() {
        this.waitingConfirmation = true;
        this.confirmationResult.confirm(this.verifyCode.toString())
            .then(result => {
                if (admins.indexOf(result.user.uid) != -1) {
                    window.location.href = window.location.origin + '/information/0';
                } else {
                    this.auth.auth.signOut()
                        .then(() => {
                            this.resetVerifyCode(false, "Попробуйте войти через другой телефон");
                            this.phone = null;
                        })
                        .catch(() => {
                            this.showSnackbar('Не удалось выйти из аккаунта');
                        });
                }
            })
            .catch(() => {
                this.resetVerifyCode(true, "Не удалось подтвердить код");
            });
    }

    resetVerifyCode = (waitingCode: boolean, message: string) => {
        if (!waitingCode) {
            this.timeout = null;
        }
        this.verifyCode = null;
        this.waitingCode = waitingCode;
        this.waitingConfirmation = false;
        this.showSnackbar(message);
    };

    showSnackbar = (message) => {
        this.snackbar.open(message, null,{
            duration: 1500
        });
    };
    
    ngOnDestroy() {
        if (this.interval != null) {
            clearInterval(this.interval);
        }
    }
}

@NgModule({
    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatSnackBarModule,
        MatCardModule,
        TextMaskModule,
        CommonModule,
        FormsModule
    ],
    exports: [LoginComponent],
    declarations: [LoginComponent]
})
export class LoginModule {}
