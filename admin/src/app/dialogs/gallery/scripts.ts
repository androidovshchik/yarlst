import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {NgxGalleryImage, NgxGalleryOptions} from "ngx-gallery";
import {AngularFireStorage} from "angularfire2/storage";
import {Subscription} from "rxjs/Subscription";

@Component({
    selector: 'dialog-gallery',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class DialogGallery implements OnInit, OnDestroy {

    options: NgxGalleryOptions[];

    _images: Subscription[] = [];
    images: NgxGalleryImage[] = [];

    constructor(public dialogRef: MatDialogRef<DialogGallery>, @Inject(MAT_DIALOG_DATA) public data: any,
                public storage: AngularFireStorage) {}

    ngOnInit() {
        this.options = [{
            height: '100px',
            image: false,
            previewCloseOnClick: true,
            thumbnailsColumns: 5
        }];
        for (let p = 0; p < this.data.cell._photos.length; p++) {
            this._images.push(this.storage.ref('photos/' + this.data.cell._folder + '/' + this.data.cell._photos[p].name)
                .getDownloadURL()
                .subscribe(url => {
                    this.images.push({
                        small: url,
                        medium: url,
                        big: url
                    })
                }));
        }
    }

    onCancelClick(): void {
        this.dialogRef.close();
    }

    ngOnDestroy() {
        for (let i = 0; i < this._images.length; i++) {
            if (this._images[i] != null) {
                this._images[i].unsubscribe();
            }
        }
    }
}
