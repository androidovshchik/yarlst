import {Component, Inject, OnInit} from '@angular/core';
import {
    DateAdapter,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
    MAT_DIALOG_DATA,
    MatDialogRef,
    MatSnackBar
} from '@angular/material';
import {WorkersService} from '../../services/workers';
import {Subscription} from 'rxjs/Subscription';
import {BaseTab} from '../../blocks/base/tab';
import {AngularFirestore} from 'angularfire2/firestore';
import {MaterialModel, MaterialsService} from '../../services/materials';
import {ProjectModel, ProjectsService} from '../../services/projects';
import {OperationModel, OperationsService} from '../../services/operations';
import {BaseBlock} from '../../blocks/base/block';
import {APP_DATE_FORMATS, AppDateAdapter} from '../../table/picker';
import {WorkerItem} from "../../table/models";

@Component({
    selector: 'dialog-edit-worker-work',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss'],
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'ru-RU'},
        {provide: DateAdapter, useClass: AppDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
    ]
})
export class DialogEditWorkerWork extends BaseTab implements OnInit {

    mask = [/[0-2]/, /\d/,':',/[0-5]/, /\d/];

    _projects: Subscription;
    _operations: Subscription;
    _materials: Subscription;

    workers: WorkerItem[] = [];
    projects: ProjectModel[] = [];
    workspaces: string[] = ['Объект', 'Цех'];
    eventTypes: string[] = ['Дорога', 'Обед', 'Перерыв'];
    operations: OperationModel[] = [];
    materials: MaterialModel[] = [];

    constructor(public store: AngularFirestore, public snackbar: MatSnackBar, public dialogRef: MatDialogRef<DialogEditWorkerWork>,
                @Inject(MAT_DIALOG_DATA) public data: any, public workersService: WorkersService, public materialsService: MaterialsService,
                public projectsService: ProjectsService, public operationsService: OperationsService) {
        super(store, snackbar);
    }

    ngOnInit() {
        this._subscription = this.workersService._documents.asObservable()
            .subscribe(worker => {
                if (worker != null) {
                    const workerItem: WorkerItem = {
                        _uid: worker._uid,
                        name: worker.hasOwnProperty("name") ? worker.name : worker.phone
                    };
                    this.workers.push(workerItem);
                }
            });
        this._projects = this.projectsService._documents.asObservable()
            .subscribe((model: ProjectModel) => {
                if (model != null) {
                    this.projects.push(model);
                }
            });
        this._operations = this.operationsService._documents.asObservable()
            .subscribe((model: OperationModel) => {
                if (model != null) {
                    this.operations.push(model);
                }
            });
        if (this.data.tab === 2) {
            this._materials = this.materialsService._documents.asObservable()
                .subscribe((model: MaterialModel) => {
                    if (model != null) {
                        this.materials.push(model);
                    }
                });
        }
    }

    getDate(position: number): string {
        return BaseBlock.getDate(this.data.work.events[position].end);
    }

    onDateChange() {
        const date: string = BaseBlock.getDate(this.data.date.getTime());
        this.data.work.events[0].start = BaseBlock.datetime2Timestamp(date + ' ' + this.getStartTime(0));
        this.data.work.events[0].end = BaseBlock.datetime2Timestamp(date + ' ' + this.getEndTime(0));
    }

    getStartTime(position: number): string {
        return BaseBlock.getTime(this.data.work.events[position].start);
    }

    onStartTime(position: number, value) {
        if (/^([0-1][0-9]|2[0-3]):([0-5][0-9])$/.test(value)) {
            this.data.work.events[position].start = BaseBlock.datetime2Timestamp(this.getDate(position) + ' ' + value);
        }
    }

    getEndTime(position: number): string {
        return BaseBlock.getTime(this.data.work.events[position].end);
    }

    onEndTime(position: number, value) {
        if (/^([0-1][0-9]|2[0-3]):([0-5][0-9])$/.test(value)) {
            this.data.work.events[position].end = BaseBlock.datetime2Timestamp(this.getDate(position) + ' ' + value);
        }
    }

    onMaterialChange() {
        for (let m = 0; m < this.materials.length; m++) {
            if (this.data.work.expenses[this.data.position].material === this.materials[m].name) {
                this.data.work.expenses[this.data.position].dimension = this.materials[m].dimension;
                break;
            }
        }
    }

    onCancelClick(): void {
        this.dialogRef.close();
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        if (this._projects != null) {
            this._projects.unsubscribe();
        }
        if (this._operations != null) {
            this._operations.unsubscribe();
        }
        if (this._materials != null) {
            this._materials.unsubscribe();
        }
    }
}
