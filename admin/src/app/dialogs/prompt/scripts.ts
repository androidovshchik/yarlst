import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
    selector: 'dialog-prompt',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class DialogPrompt {

    constructor(public dialogRef: MatDialogRef<DialogPrompt>, @Inject(MAT_DIALOG_DATA) public data: any) {}

    onCancelClick(): void {
        this.dialogRef.close();
    }
}
