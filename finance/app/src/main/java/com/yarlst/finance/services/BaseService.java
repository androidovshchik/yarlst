package com.yarlst.finance.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.yarlst.finance.R;
import com.yarlst.finance.data.Prefs;
import com.yarlst.finance.receivers.ToastTrigger;
import com.yarlst.finance.utils.AlarmUtil;
import com.yarlst.finance.utils.NetworkUtil;
import com.yarlst.finance.utils.VerifyUtil;

import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

public abstract class BaseService extends Service {

	public static final long MINUTE = 60 * 1000;

	public static final long HOUR = 60 * MINUTE;

	protected ConnectivityManager connectivityManager;

	protected CompositeDisposable disposable;

	protected Prefs prefs;

	protected FirebaseUser fireUser;

	private NotificationManager notificationManager;

	private PowerManager.WakeLock wakeLock;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	@SuppressWarnings("all")
	public void onCreate() {
		super.onCreate();
		connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationChannel channel = new NotificationChannel(getString(R.string.app_name),
				getString(R.string.app_name), NotificationManager.IMPORTANCE_LOW);
			notificationManager.createNotificationChannel(channel);
		}
		PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getString(R.string.app_name));
		wakeLock.acquire();
		disposable = new CompositeDisposable();
		prefs = new Prefs(getApplicationContext());
		AlarmUtil.recreateAlarm(getApplicationContext(), getClass());
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		fireUser = FirebaseAuth.getInstance().getCurrentUser();
		if (fireUser == null) {
			Timber.w(getClass().getSimpleName() + ": fireUser is null");
			onStopSync();
		} else if (!onCheckConditions()) {
			Timber.w(getClass().getSimpleName() + ": hasn't conditions");
			onStopSync();
		} else {
			afterStartCommand();
		}
		return START_NOT_STICKY;
	}

	protected abstract void afterStartCommand();

	protected boolean onCheckConditions() {
		Context context = getApplicationContext();
		return NetworkUtil.isConnected(connectivityManager) && !prefs.has(Prefs.LATEST_VERSION) &&
			VerifyUtil.isSystemTimeValid(getContentResolver()) && VerifyUtil.isGCMCompatible(context);
	}

	protected boolean ifNeedOnlyWifi(String pref) {
		return !prefs.getBoolean(pref) || NetworkUtil.isConnectedViaWifi(connectivityManager);
	}

	protected void onStartForeground(int id, @StringRes int string, @DrawableRes int image) {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),
			getString(R.string.app_name))
			.setSmallIcon(image)
			.setContentTitle(getString(string))
			.setSound(null);
		startForeground(id, builder.build());
	}

	protected void onShowNotification(int id, @StringRes int string, @DrawableRes int image, Intent activity) {
		activity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),
			getString(R.string.app_name))
			.setSmallIcon(image)
			.setAutoCancel(true)
			.setDefaults(Notification.DEFAULT_SOUND)
			.setContentTitle(getString(string))
			.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, activity, 0));
		notificationManager.notify(id, builder.build());
	}

	protected void onShowMessage(String message) {
		if (message == null) {
			Timber.e("Nullable error");
			return;
		}
		Timber.e(message);
		Intent intent = new Intent();
		intent.setAction(getPackageName() + ".TOAST");
		intent.putExtra(ToastTrigger.EXTRA_MESSAGE, getString(R.string.app_name) + ": "  + message);
		sendBroadcast(intent);
	}

	protected void onStopSync() {
		stopForeground(true);
		stopSelf();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		disposable.dispose();
		wakeLock.release();
	}
}
