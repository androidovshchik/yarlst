package com.yarlst.finance.ui.main;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.yarlst.finance.R;
import com.yarlst.finance.ui.auth.AuthActivity;
import com.yarlst.finance.ui.categories.CategoriesActivity;
import com.yarlst.finance.ui.money.MoneyActivity;

import butterknife.BindView;

public class MainActivity extends AuthActivity {

	@BindView(R.id.tabs)
	TabLayout tabs;
	@BindView(R.id.fragments)
	ViewPager fragments;

	private TabsAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		adapter = new TabsAdapter(getSupportFragmentManager());
		fragments.setAdapter(adapter);
		tabs.setupWithViewPager(fragments);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && getSupportActionBar() != null) {
			getSupportActionBar().setElevation(0f);
		}
		onCheckUpdates();
		toggleViews(true);
	}

	@Override
	public void toggleViews(boolean enable) {
		super.toggleViews(enable);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_add:
				onStartSafelyActivity(MoneyActivity.class);
				return true;
			case R.id.action_categories:
				onStartSafelyActivity(CategoriesActivity.class);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}
}