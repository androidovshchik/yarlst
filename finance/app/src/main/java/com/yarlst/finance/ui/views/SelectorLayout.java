package com.yarlst.finance.ui.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.IntegerRes;
import android.util.AttributeSet;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yarlst.finance.R;
import com.yarlst.finance.ui.select.SelectActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SelectorLayout extends RelativeLayout {

    public static final int TYPE_NONE = 0;
    public static final int TYPE_MATERIALS = 1;
    public static final int TYPE_OPERATIONS = 2;
    public static final int TYPE_PROJECTS = 3;

    @BindView(R.id.selected)
    public TextView selected;
    @BindView(R.id.select)
    public ImageButton selectButton;

    private int type = TYPE_NONE;

    private String emptyText = "";

    private Unbinder unbinder;

    public SelectorLayout(Context context) {
        super(context);
    }

    public SelectorLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SelectorLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressWarnings("unused")
    public SelectorLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setType(@IntegerRes int integer) {
        type = integer;
        switch (type) {
            case TYPE_PROJECTS:
                //emptyText = getContext().getString(R.string.empty_project);
                break;
            case TYPE_OPERATIONS:
                //emptyText = getContext().getString(R.string.empty_operation);
                break;
        }
        selected.setText(emptyText);
    }

    @OnClick(R.id.select)
    public void onSelect() {
        Intent intent = new Intent(getContext(), SelectActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(SelectActivity.EXTRA_TYPE, type);
        getContext().startActivity(intent);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        unbinder = ButterKnife.bind(this);
        selectButton.setImageResource(R.drawable.ic_view_list_black_24dp);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        unbinder.unbind();
    }

    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }
}
