package com.yarlst.finance.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.yarlst.finance.data.Prefs;
import com.yarlst.finance.utils.AlarmUtil;
import com.yarlst.finance.utils.ServiceUtil;

import timber.log.Timber;

public class ServiceTrigger extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Timber.d("ServiceTrigger: received");
		Prefs prefs = new Prefs(context);
		boolean launchedUpdate = ServiceUtil.launchUpdate(context, false, prefs);
		if (!launchedUpdate) {
			AlarmUtil.recreateAlarm(context, getClass());
		}
	}
}
