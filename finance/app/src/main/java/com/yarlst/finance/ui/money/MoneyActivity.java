package com.yarlst.finance.ui.money;

import android.os.Bundle;

import com.yarlst.finance.ui.auth.AuthActivity;

public class MoneyActivity extends AuthActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		toggleViews(true);
	}

	@Override
	public void toggleViews(boolean enable) {
		super.toggleViews(enable);
	}
}