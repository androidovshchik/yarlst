package com.yarlst.finance.ui.base;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public abstract class BaseAdapter<T, V extends BaseViewHolder> extends RecyclerView.Adapter<V> {

    public boolean hasEnabledUI = false;

    public ArrayList<T> items;

    public BaseAdapter() {
        items = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
