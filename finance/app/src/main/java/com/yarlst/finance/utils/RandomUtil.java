package com.yarlst.finance.utils;

import java.security.SecureRandom;

public class RandomUtil {

    private static final String CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    private static final SecureRandom RANDOM = new SecureRandom();

    public static String generateUid() {
        StringBuilder stringBuilder = new StringBuilder(24);
        for (int i = 0; i < 24; i++) {
            stringBuilder.append(CHARS.charAt(RANDOM.nextInt(CHARS.length())));
        }
        return stringBuilder.toString();
    }
}
