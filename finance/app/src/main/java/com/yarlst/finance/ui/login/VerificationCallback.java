package com.yarlst.finance.ui.login;

import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.yarlst.finance.R;

import timber.log.Timber;

public abstract class VerificationCallback extends PhoneAuthProvider.OnVerificationStateChangedCallbacks {

	public abstract void onSuccess(@Nullable PhoneAuthCredential credential,
								   @Nullable String verificationId);

	public abstract void onError(@StringRes int id);

	@Override
	public void onVerificationCompleted(PhoneAuthCredential credential) {
		Timber.d("credential: " + credential.toString());
		onSuccess(credential, null);
	}

	@Override
	public void onVerificationFailed(FirebaseException e) {
		Timber.e(e);
		if (e instanceof FirebaseAuthInvalidCredentialsException) {
			onError(R.string.error_phone_invalid_code);
		} else if (e instanceof FirebaseTooManyRequestsException) {
			onError(R.string.error_phone_too_many_requests);
		} else {
			onError(R.string.error_unknown);
		}
	}

	@Override
	public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
		Timber.d("verificationId: " + verificationId);
		onSuccess(null, verificationId);
	}
}
