package com.yarlst.finance.ui.gms;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.yarlst.finance.R;
import com.yarlst.finance.ui.auth.AuthActivity;
import com.yarlst.finance.ui.base.BaseActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class GMSActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gms);
        setTitle(R.string.title_update);
        ButterKnife.bind(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (GoogleApiAvailability.getInstance()
            .isGooglePlayServicesAvailable(getApplicationContext()) == ConnectionResult.SUCCESS) {
            onSwitchSafelyActivity(AuthActivity.class);
        }
    }

    @OnClick(R.id.update)
    public void onUpdate() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse("market://details?id=" +
                    GoogleApiAvailability.GOOGLE_PLAY_SERVICES_PACKAGE)));
        } catch (Exception e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=" +
                    GoogleApiAvailability.GOOGLE_PLAY_SERVICES_PACKAGE)));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onSwitchSafelyActivity(AuthActivity.class);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void toggleViews(boolean lock) {}
}