package com.yarlst.finance.ui.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

public class TabsAdapter extends FragmentStatePagerAdapter {

    final ArrayList<MainFragment> fragments = new ArrayList<>();

    public TabsAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        fragments.add(MainFragment.newInstance(MainFragment.TYPE_EXPENSES));
        fragments.add(MainFragment.newInstance(MainFragment.TYPE_INCOMES));
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 1:
                return "Расходы";
            default:
                return "Доходы";
        }
    }
}