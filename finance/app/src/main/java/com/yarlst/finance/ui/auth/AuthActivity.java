package com.yarlst.finance.ui.auth;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.yarlst.finance.BuildConfig;
import com.yarlst.finance.R;
import com.yarlst.finance.ui.base.BaseActivity;
import com.yarlst.finance.ui.gms.GMSActivity;
import com.yarlst.finance.ui.login.LoginActivity;
import com.yarlst.finance.ui.main.MainActivity;
import com.yarlst.finance.ui.money.MoneyActivity;
import com.yarlst.finance.ui.time.TimeActivity;
import com.yarlst.finance.ui.update.UpdateActivity;
import com.yarlst.finance.utils.VerifyUtil;
import com.yarlst.finance.utils.VersionUtil;

import butterknife.ButterKnife;
import timber.log.Timber;

public class AuthActivity extends BaseActivity {

    protected FirebaseUser fireUser;

    private boolean hasEnabledUI = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!onCheckConditions()) {
            return;
        }
        if (getClass().equals(MainActivity.class)) {
            setupContentView(R.layout.activity_main, R.string.title_main);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
            }
            toggleViews(false);
        } else if (getClass().equals(MoneyActivity.class)) {
            setupContentView(R.layout.activity_money, R.string.title_money);
            toggleViews(false);
        } else {
            toggleViews(false);
            onSwitchSafelyActivity(MainActivity.class);
        }
    }

    private void setupContentView(@LayoutRes int layout, @StringRes int title) {
        setContentView(layout);
        setTitle(title);
        ButterKnife.bind(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        onCheckConditions();
    }

    @SuppressWarnings("all")
    private boolean onCheckConditions() {
        if (VersionUtil.hasUpdateFromPrefs(getApplicationContext(), prefs)) {
            onSwitchSafelyActivity(UpdateActivity.class);
            return false;
        }
        if (!VerifyUtil.isGCMCompatible(getApplicationContext())) {
            onSwitchSafelyActivity(GMSActivity.class);
            return false;
        }
        if (!VerifyUtil.isSystemTimeValid(getContentResolver())) {
            onSwitchSafelyActivity(TimeActivity.class);
            return false;
        }
        fireUser = FirebaseAuth.getInstance().getCurrentUser();
        if (fireUser == null) {
            Timber.w(getClass().getSimpleName() + ": fireUser is null");
            onSwitchSafelyActivity(LoginActivity.class);
            return false;
        }
        return true;
    }

    @Override
    public void toggleViews(boolean enable) {
        hasEnabledUI = enable;
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(hasEnabledUI);
        }
        invalidateOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!getClass().equals(AuthActivity.class) && !getClass().equals(LoginActivity.class) &&
            !getClass().equals(MoneyActivity.class)) {
            getMenuInflater().inflate(R.menu.auth_menu, menu);
            return true;
        }
        return false;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return hasEnabledUI;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_info:
                new AlertDialog.Builder(this)
                    .setTitle(R.string.info)
                    .setMessage(getString(R.string.info_description, fireUser.getPhoneNumber(), BuildConfig.VERSION_CODE))
                    .setPositiveButton(getString(android.R.string.ok), null)
                    .create()
                    .show();
                return true;
            case R.id.action_logout:
                buildAlertDialog(R.string.confirm_action)
                    .setPositiveButton(getString(android.R.string.ok), (DialogInterface dialog, int id) -> {
                        try {
                            toggleViews(false);
                            disposable.clear();
                            FirebaseAuth.getInstance().signOut();
                        } finally {
                            onSwitchSafelyActivity(LoginActivity.class);
                        }
                    })
                    .create()
                    .show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected AlertDialog.Builder buildAlertDialog(@StringRes int title) {
        return new AlertDialog.Builder(this)
            .setTitle(title)
            .setNegativeButton(getString(android.R.string.cancel), null);
    }

    @Override
    public void onBackPressed() {
        if (hasEnabledUI) {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("all")
    protected void onCheckUpdates() {
        VersionUtil.onCheckAppVersion()
            .subscribe(snapshot -> {
                if (VersionUtil.hasUpdateFromFirebase(getApplicationContext(), snapshot, prefs)) {
                    onSwitchSafelyActivity(UpdateActivity.class);
                }
            }, throwable -> {});
    }
}
