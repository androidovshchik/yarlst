package com.yarlst.finance.ui.time;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.yarlst.finance.R;
import com.yarlst.finance.ui.auth.AuthActivity;
import com.yarlst.finance.ui.base.BaseActivity;
import com.yarlst.finance.utils.VerifyUtil;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class TimeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);
        setTitle(R.string.title_time);
        ButterKnife.bind(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        }
    }

    @Override
    @SuppressWarnings("all")
    public void onStart() {
        super.onStart();
        if (VerifyUtil.isSystemTimeValid(getContentResolver())) {
            onSwitchSafelyActivity(AuthActivity.class);
        }
    }

    @OnClick(R.id.settings)
    public void onSettings() {
        Intent intent = new Intent(android.provider.Settings.ACTION_DATE_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onSwitchSafelyActivity(AuthActivity.class);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void toggleViews(boolean lock) {}
}