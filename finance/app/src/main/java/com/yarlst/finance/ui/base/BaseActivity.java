package com.yarlst.finance.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.yarlst.finance.R;
import com.yarlst.finance.data.Prefs;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

public abstract class BaseActivity extends AppCompatActivity {

    @Nullable
    @BindView(R.id.contentView)
    ViewGroup contentView;

    protected CompositeDisposable disposable;

    protected Prefs prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        disposable = new CompositeDisposable();
        prefs = new Prefs(getApplicationContext());
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (contentView != null) {
            contentView.requestFocus();
        }
    }

    public abstract void toggleViews(boolean enable);

    public void toggleViewGroup(ViewGroup viewGroup, boolean enable) {
        for (int v = 0; v < viewGroup.getChildCount(); v++) {
            viewGroup.getChildAt(v).setEnabled(enable);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onShowMessage(@StringRes int id) {
        onShowMessage(getString(id), Snackbar.LENGTH_SHORT, null, null);
    }

    public void onShowMessage(@Nullable String message, int duration, @Nullable String action,
                              @Nullable View.OnClickListener listener) {
        if (message == null) {
            Timber.w("Showing message is null");
            return;
        }
        if (contentView == null) {
            Timber.w("Content view is null");
            return;
        }
        Timber.d(message);
        Snackbar snackbar = Snackbar.make(contentView, message, duration);
        if (action != null) {
            snackbar.setAction(action, listener);
        }
        snackbar.show();
    }

    protected boolean onSwitchSafelyActivity(Class activity) {
        if (onStartSafelyActivity(activity)) {
            finish();
            return true;
        }
        return false;
    }

    protected boolean onStartSafelyActivity(Class activity) {
        if (!getClass().equals(activity)) {
            Intent intent = new Intent(getApplicationContext(), activity);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return true;
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
}
