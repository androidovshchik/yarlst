package com.yarlst.finance.models.events;

public class DeleteEvent {

    public final int position;

    public DeleteEvent(int position) {
        this.position = position;
    }
}
