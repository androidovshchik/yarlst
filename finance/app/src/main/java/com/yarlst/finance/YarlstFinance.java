package com.yarlst.finance;

import android.support.multidex.MultiDexApplication;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.SetOptions;
import com.yarlst.finance.data.Prefs;
import com.yarlst.finance.models.Worker;
import com.yarlst.finance.utils.ProdUtil;
import com.yarlst.finance.utils.ServiceUtil;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import durdinapps.rxfirebase2.RxFirestore;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@ReportsCrashes(mailTo = "vladkalyuzhnyu@gmail.com",
	customReportContent = {
		ReportField.APP_VERSION_CODE,
		ReportField.APP_VERSION_NAME,
		ReportField.ANDROID_VERSION,
		ReportField.PHONE_MODEL,
		ReportField.BRAND,
		ReportField.PRODUCT,
		ReportField.USER_COMMENT,
		ReportField.USER_APP_START_DATE,
		ReportField.USER_CRASH_DATE,
		ReportField.STACK_TRACE,
		ReportField.LOGCAT
	},
	mode = ReportingInteractionMode.DIALOG,
	resDialogText = R.string.error_crash,
	resDialogCommentPrompt = R.string.error_comment,
	resDialogTheme = R.style.AppTheme_Dialog)
public class YarlstFinance extends MultiDexApplication {

	private Prefs prefs;

	@SuppressWarnings("all")
	@Override
	public void onCreate() {
		super.onCreate();
		if (ACRA.isACRASenderServiceProcess()) {
			return;
		}
		Timber.plant(new Timber.DebugTree());
		if (ProdUtil.getBoolean()) {
			ACRA.init(this);
		} else {
			StethoTool.init(getApplicationContext());
		}
		prefs = new Prefs(getApplicationContext());
		ServiceUtil.launchUpdate(getApplicationContext(), false, prefs);
		FirebaseUser fireUser = FirebaseAuth.getInstance().getCurrentUser();
		if (fireUser != null) {
			RxFirestore.setDocument(FirebaseFirestore.getInstance()
				.collection("workers")
				.document(fireUser.getUid()), new Worker(getApplicationContext(), fireUser), SetOptions.merge())
				.subscribeOn(Schedulers.io())
				.subscribe(() -> {}, throwable -> {
					if (throwable instanceof FirebaseFirestoreException) {
						Timber.e(throwable);
						ServiceUtil.launchUpdate(getApplicationContext(), true, prefs);
					}
				});
		}
	}
}