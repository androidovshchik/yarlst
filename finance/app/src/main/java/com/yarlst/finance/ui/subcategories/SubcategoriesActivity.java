package com.yarlst.finance.ui.subcategories;

import android.os.Bundle;

import com.yarlst.finance.ui.auth.AuthActivity;

public class SubcategoriesActivity extends AuthActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void toggleViews(boolean enable) {
		super.toggleViews(enable);
	}
}