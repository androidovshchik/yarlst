package com.yarlst.finance.ui.categories;

import android.os.Bundle;

import com.yarlst.finance.ui.auth.AuthActivity;

public class CategoriesActivity extends AuthActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		toggleViews(true);
	}

	@Override
	public void toggleViews(boolean enable) {
		super.toggleViews(enable);
	}
}