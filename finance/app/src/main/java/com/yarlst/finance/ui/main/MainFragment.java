package com.yarlst.finance.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yarlst.finance.R;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainFragment extends Fragment {

    public static final int TYPE_EXPENSES = 1;
    public static final int TYPE_INCOMES = 2;

    public boolean hasEnabledUI = false;

    private Unbinder unbinder;

    private int type;

    public static MainFragment newInstance(int type) {
        MainFragment fragment = new MainFragment();
        fragment.type = type;
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @SuppressWarnings("all")
    protected MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    @SuppressWarnings("all")
    protected Context getApplicationContext() {
        return getActivity().getApplicationContext();
    }
}