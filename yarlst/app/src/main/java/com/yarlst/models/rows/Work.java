package com.yarlst.models.rows;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.PropertyName;

import java.util.ArrayList;

public class Work extends Row {

	// workspaces
	public static final String WORKSHOP = "Цех";
	public static final String OBJECT = "Объект";

	// steps
	public static final int STEP_START = 10;
	public static final int STEP_END = 20;
	public static final int STEP_EXPENSES = 30;
	public static final int STEP_PHOTOS = 35;
	public static final int STEP_COMMENT = 40;
	public static final int STEP_DONE = 1000;
	public static final int STEP_FINISH = 9999;

	private static final String TABLE = "works";

	private static final String COLUMN_ID = "id";
	private static final String COLUMN_WORKER = "worker";
	private static final String COLUMN_DOCUMENT = "document";
	private static final String COLUMN_STEP = "step";
	private static final String COLUMN_PROJECT = "project";
	private static final String COLUMN_WORKSPACE = "workspace";
	private static final String COLUMN_OPERATION = "operation";
	private static final String COLUMN_DESCRIPTION = "description";
	private static final String COLUMN_COMMENT = "comment";

	@Exclude
	public int step = STEP_START;

	@Exclude
	public String uid;

	@Exclude
	public String worker;

	@PropertyName("_version")
	public int version = 0;

	@PropertyName("_timestamp")
	public long timestamp;

	/* start */

	@PropertyName("project")
	public String project;

	@PropertyName("workspace")
	public String workspace;

	@PropertyName("operation")
	public String operation;

	@Exclude
	public String description;

	/* start */

	/* end */

	@PropertyName("events")
	public ArrayList<Event> events;

	/* end */

	/* materials */

	@PropertyName("expenses")
	public ArrayList<Expense> expenses;

	/* materials */

	/* photos */

	@PropertyName("photos")
	public ArrayList<Photo> photos;

	/* photos */

	/* comment */

	@PropertyName("comment")
	public String comment;

	/* comment */

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		if (id != NONE) {
			values.put(COLUMN_ID, id);
		}
		values.put(COLUMN_STEP, step);
		values.put(COLUMN_WORKER, worker);
		values.put(COLUMN_DOCUMENT, uid);
		values.put(COLUMN_PROJECT, project);
		values.put(COLUMN_WORKSPACE, workspace);
		values.put(COLUMN_OPERATION, operation);
		values.put(COLUMN_DESCRIPTION, description);
		values.put(COLUMN_COMMENT, comment);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID));
		worker = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_WORKER));
		uid = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DOCUMENT));
		step = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_STEP));
		project = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_PROJECT));
		workspace = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_WORKSPACE));
		operation = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_OPERATION));
		description = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DESCRIPTION));
		comment = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_COMMENT));
	}

	@Exclude
	@Override
	public String getTable() {
		return TABLE;
	}

	@Override
	public String toString() {
		return "Work{" +
			"id=" + id +
			", step=" + step +
			", uid='" + uid + '\'' +
			", worker='" + worker + '\'' +
			", version=" + version +
			", timestamp=" + timestamp +
			", project='" + project + '\'' +
			", workspace='" + workspace + '\'' +
			", operation='" + operation + '\'' +
			", description='" + description + '\'' +
			", events=" + events +
			", expenses=" + expenses +
			", photos=" + photos +
			", comment='" + comment + '\'' +
			'}';
	}
}
