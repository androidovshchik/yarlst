package com.yarlst.models.rows;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.PropertyName;

public class Expense extends Row {

	private static final String TABLE = "expenses";

	private static final String COLUMN_ID = "id";
	private static final String COLUMN_WORK = "work";
	private static final String COLUMN_MATERIAL = "material";
	private static final String COLUMN_QUANTITY = "quantity";
	private static final String COLUMN_DIMENSION = "dimension";

	@Exclude
	public long work = NONE;

	@PropertyName("material")
	public String material;

	@PropertyName("quantity")
	public String quantity = "0";

	@PropertyName("dimension")
	public String dimension = "";

	public Expense() {}

	public Expense(long work, String material, String dimension) {
		this.work = work;
		this.material = material;
		this.dimension = dimension;
	}

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		if (id != NONE) {
			values.put(COLUMN_ID, id);
		}
		values.put(COLUMN_WORK, work);
		values.put(COLUMN_MATERIAL, material);
		values.put(COLUMN_QUANTITY, quantity);
		values.put(COLUMN_DIMENSION, dimension);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID));
		work = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_WORK));
		material = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_MATERIAL));
		quantity = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_QUANTITY));
		dimension = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DIMENSION));
	}

	@Exclude
	@Override
	public String getTable() {
		return TABLE;
	}

	@Override
	public String toString() {
		return "Expense{" +
			"id=" + id +
			", work=" + work +
			", material='" + material + '\'' +
			", quantity='" + quantity + '\'' +
			", dimension='" + dimension + '\'' +
			'}';
	}
}
