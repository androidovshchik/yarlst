package com.yarlst.models.rows;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.firebase.firestore.PropertyName;

public class Material extends Row {

	public static final String TABLE = "materials";

	private static final String COLUMN_ID = "id";
	private static final String COLUMN_NAME = "name";
	private static final String COLUMN_DIMENSION = "dimension";

	@PropertyName("name")
	public String name;

	@PropertyName("dimension")
	public String dimension;

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		if (id != NONE) {
			values.put(COLUMN_ID, id);
		}
		values.put(COLUMN_NAME, name);
		values.put(COLUMN_DIMENSION, dimension);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID));
		name = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME));
		dimension = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DIMENSION));
	}

	@Override
	public String getTable() {
		return TABLE;
	}
}
