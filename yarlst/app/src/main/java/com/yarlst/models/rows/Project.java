package com.yarlst.models.rows;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.firebase.firestore.PropertyName;

public class Project extends Row {

	public static final String TABLE = "projects";

	private static final String COLUMN_ID = "id";
	private static final String COLUMN_NAME = "name";

	@PropertyName("name")
	public String name;

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		if (id != NONE) {
			values.put(COLUMN_ID, id);
		}
		values.put(COLUMN_NAME, name);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID));
		name = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME));
	}

	@Override
	public String getTable() {
		return TABLE;
	}
}
