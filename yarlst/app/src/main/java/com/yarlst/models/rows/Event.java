package com.yarlst.models.rows;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.PropertyName;

public class Event extends Row {

	// types
	public static final String TYPE_WORK = "Работа";
	public static final String TYPE_TIMEOUT = "Перерыв";
	public static final String TYPE_LUNCH = "Обед";
	public static final String TYPE_ROAD = "Дорога";

	// stages
	public static final String STAGE_START = "Начало";
	public static final String STAGE_END = "Окончание";

	private static final String TABLE = "events";

	private static final String COLUMN_ID = "id";
	private static final String COLUMN_WORK = "work";
	private static final String COLUMN_TYPE = "type";
	private static final String COLUMN_STAGE = "stage";
	private static final String COLUMN_TIMESTAMP = "timestamp";

	@Exclude
	public long work = NONE;

	@PropertyName("type")
	public String type = TYPE_WORK;

	@Exclude
	public String stage = STAGE_START;

	@PropertyName("start")
	public long start;

	@PropertyName("end")
	public long end;

	@Exclude
	public long timestamp;

	public Event() {}

	public Event(long work, String type, String stage) {
		this.work = work;
		this.type = type;
		this.stage = stage;
	}

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		if (id != NONE) {
			values.put(COLUMN_ID, id);
		}
		values.put(COLUMN_WORK, work);
		values.put(COLUMN_TYPE, type);
		values.put(COLUMN_STAGE, stage);
		values.put(COLUMN_TIMESTAMP, System.currentTimeMillis());
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID));
		work = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_WORK));
		type = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TYPE));
		stage = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_STAGE));
		timestamp = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_TIMESTAMP));
	}

	@Exclude
	@Override
	public String getTable() {
		return TABLE;
	}

	@Override
	public String toString() {
		return "Event{" +
			"id=" + id +
			", work=" + work +
			", type='" + type + '\'' +
			", stage='" + stage + '\'' +
			", timestamp=" + timestamp +
			'}';
	}
}
