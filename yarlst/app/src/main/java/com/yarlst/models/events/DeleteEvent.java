package com.yarlst.models.events;

public class DeleteEvent {

    public final int position;

    public DeleteEvent(int position) {
        this.position = position;
    }
}
