package com.yarlst.models.rows;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.PropertyName;

public class Photo extends Row {

	private static final String TABLE = "photos";

	private static final String COLUMN_ID = "id";
	private static final String COLUMN_WORK = "work";
	// join field
	private static final String COLUMN_WORKER = "worker";
	private static final String COLUMN_NAME = "name";
	private static final String COLUMN_PATH = "path";
	private static final String COLUMN_EXPORTED = "exported";

	@Exclude
	public long work = NONE;

	// join field
	@Exclude
	public String worker;

	@PropertyName("name")
	public String name;

	@Exclude
	public String path;

	@Exclude
	public boolean exported = false;

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		if (id != NONE) {
			values.put(COLUMN_ID, id);
		}
		values.put(COLUMN_WORK, work);
		values.put(COLUMN_NAME, name);
		values.put(COLUMN_PATH, path);
		values.put(COLUMN_EXPORTED, exported ? 1 : 0);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID));
		work = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_WORK));
		try {
			worker = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_WORKER));
		} catch (IllegalArgumentException e) {
			worker = null;
		}
		name = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME));
		path = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_PATH));
		exported = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_EXPORTED)) == 1;
	}

	@Exclude
	@Override
	public String getTable() {
		return TABLE;
	}

	@Override
	public String toString() {
		return "Photo{" +
			"id=" + id +
			", work=" + work +
			", worker='" + worker + '\'' +
			", name='" + name + '\'' +
			", path='" + path + '\'' +
			", exported=" + exported +
			'}';
	}
}
