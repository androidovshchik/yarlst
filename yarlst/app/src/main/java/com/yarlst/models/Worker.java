package com.yarlst.models;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.PropertyName;
import com.yarlst.utils.ProdUtil;
import com.yarlst.utils.VersionUtil;

public class Worker {

	@SuppressWarnings("all")
	@PropertyName("phone")
	public String phone;

	@SuppressWarnings("unused")
	@PropertyName("_prod")
	public boolean prod = ProdUtil.getBoolean();

	@SuppressWarnings("unused")
	@PropertyName("_timestamp")
	public long timestamp = System.currentTimeMillis();

	@PropertyName("_version")
	public int version = 0;

	public Worker(Context context, @NonNull FirebaseUser fireUser) {
		this.phone = fireUser.getPhoneNumber();
		this.version = VersionUtil.getAppVersion(context);
	}
}
