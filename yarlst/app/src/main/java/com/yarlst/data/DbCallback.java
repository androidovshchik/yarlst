package com.yarlst.data;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import timber.log.Timber;

public class DbCallback extends SupportSQLiteOpenHelper.Callback {

    private static final int DATABASE_VERSION = 3;

    public static final String DATABASE_NAME = "db.sqlite";

    private static final String DATABASE_PATH_SUFFIX = "/databases/";

    private static final String CREATE_WORKS_TABLE = "CREATE TABLE works (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
        "worker TEXT NOT NULL, document VARCHAR (24) NOT NULL, step INTEGER NOT NULL DEFAULT (10), " +
        "project TEXT NOT NULL, workspace TEXT NOT NULL, operation TEXT NOT NULL, " +
        "description TEXT NOT NULL, comment TEXT);";

    private static final String CREATE_PHOTOS_TABLE = "CREATE TABLE photos (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
        "work INTEGER NOT NULL REFERENCES works (id) ON DELETE CASCADE ON UPDATE CASCADE, " +
        "name VARCHAR (24) NOT NULL, path TEXT NOT NULL, exported BOOLEAN NOT NULL DEFAULT (0));";

    private static final String CREATE_WORKS_BACKUP_TABLE = "CREATE TEMPORARY TABLE works_backup (id INTEGER NOT NULL, " +
        "worker TEXT NOT NULL, document VARCHAR (20) NOT NULL, step INTEGER NOT NULL, " +
        "project TEXT NOT NULL, workspace TEXT NOT NULL, operation TEXT NOT NULL, " +
        "description TEXT NOT NULL, comment TEXT);";

    private static final String CREATE_PHOTOS_BACKUP_TABLE = "CREATE TEMPORARY TABLE photos_backup (id INTEGER NOT NULL, " +
        "work INTEGER NOT NULL, name VARCHAR (20) NOT NULL, path TEXT NOT NULL, exported BOOLEAN NOT NULL);";

    public DbCallback() {
        super(DATABASE_VERSION);
    }

    @Override
    public void onCreate(SupportSQLiteDatabase db) {}

    @Override
    public void onUpgrade(SupportSQLiteDatabase db, int oldVersion, int newVersion) {
        Timber.d("oldVersion " + oldVersion + " newVersion " + newVersion);
        if (oldVersion < 2) {
            Timber.d("oldVersion < 2");
            db.execSQL(CREATE_PHOTOS_TABLE);
        }
        if (oldVersion < 3) {
            Timber.d("oldVersion < 3");
            db.execSQL(CREATE_WORKS_BACKUP_TABLE);
            db.execSQL(CREATE_PHOTOS_BACKUP_TABLE);
            db.execSQL("INSERT INTO works_backup SELECT * FROM works;");
            db.execSQL("INSERT INTO photos_backup SELECT * FROM photos;");
            db.execSQL("DROP TABLE works;");
            db.execSQL("DROP TABLE photos;");
            db.execSQL(CREATE_WORKS_TABLE);
            db.execSQL(CREATE_PHOTOS_TABLE);
            db.execSQL("INSERT INTO works SELECT * FROM works_backup;");
            db.execSQL("INSERT INTO photos SELECT * FROM photos_backup;");
            db.execSQL("DROP TABLE works_backup;");
            db.execSQL("DROP TABLE photos_backup;");
        }
    }

    public void openDatabase(Context context) {
        File file = context.getDatabasePath(DATABASE_NAME);
        if (!file.exists()) {
            try {
                copyDatabaseFromAssets(context);
            } catch (IOException e) {
                Timber.e(e.getMessage());
            }
        }
    }

    private void copyDatabaseFromAssets(Context context) throws IOException {
        InputStream input = context.getAssets().open(DATABASE_NAME);
        String outFilename = getDatabasePath(context);
        File file = new File(context.getApplicationInfo().dataDir + DATABASE_PATH_SUFFIX);
        boolean made;
        if (!file.exists()) {
            made = file.mkdir();
            if (!made) {
                return;
            }
        }
        OutputStream output = new FileOutputStream(outFilename);
        try {
            byte[] buffer = new byte[1024];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
        } finally {
            output.flush();
            output.close();
            input.close();
        }
    }

    private String getDatabasePath(Context context) {
        return context.getApplicationInfo().dataDir + DATABASE_PATH_SUFFIX + DATABASE_NAME;
    }
}