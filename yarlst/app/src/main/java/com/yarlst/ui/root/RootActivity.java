package com.yarlst.ui.root;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.yarlst.R;
import com.yarlst.ui.base.BaseActivity;
import com.yarlst.ui.work.WorkActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class RootActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        setTitle(R.string.title_root);
        ButterKnife.bind(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        }
    }

    @OnClick(R.id.delete)
    public void onDeleteApp() {
        Intent intent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE,
            Uri.parse("package:" + getPackageName()));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onSwitchSafelyActivity(WorkActivity.class);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void toggleViews(boolean lock) {}
}