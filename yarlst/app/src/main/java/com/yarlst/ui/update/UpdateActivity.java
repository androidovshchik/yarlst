package com.yarlst.ui.update;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.storage.FirebaseStorage;
import com.yarlst.R;
import com.yarlst.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import durdinapps.rxfirebase2.RxFirebaseStorage;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class UpdateActivity extends BaseActivity {

    @BindView(R.id.message)
    TextView message;
    @BindView(R.id.download)
    View download;

    private Uri uri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        setTitle(R.string.title_update);
        ButterKnife.bind(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        }
        RxFirebaseStorage.getDownloadUrl(FirebaseStorage.getInstance().getReference().child("yarlst-prod.apk"))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(uri -> {
                Timber.d(uri.toString());
                this.uri = uri;
                download.setEnabled(true);
            }, throwable -> {
                Timber.e(throwable);
                download.setEnabled(false);
                message.setText(R.string.error_download);
            });
    }

    @Override
    protected void onStart() {
        super.onStart();
        download.setEnabled(uri != null);
    }

    @OnClick(R.id.download)
    public void onDownload() {
        download.setEnabled(false);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.update_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_copy_link:
                if (uri != null) {
                    ClipboardManager clipboard = (ClipboardManager)
                        getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("", uri.toString());
                    if (clipboard != null) {
                        clipboard.setPrimaryClip(clip);
                        onShowMessage(R.string.copied);
                    } else {
                        onShowMessage(R.string.error_unknown);
                    }
                } else {
                    onShowMessage(R.string.wait);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void toggleViews(boolean lock) {}
}