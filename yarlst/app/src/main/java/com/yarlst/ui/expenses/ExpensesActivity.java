package com.yarlst.ui.expenses;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.yarlst.R;
import com.yarlst.models.events.DeleteEvent;
import com.yarlst.models.rows.Event;
import com.yarlst.models.rows.Expense;
import com.yarlst.models.rows.Material;
import com.yarlst.models.rows.Row;
import com.yarlst.models.rows.Work;
import com.yarlst.ui.select.SelectActivity;
import com.yarlst.ui.views.SelectorLayout;
import com.yarlst.ui.work.WorkActivity;
import com.yarlst.utils.ServiceUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class ExpensesActivity extends WorkActivity {

	private static final int MAX_EXPENSES_COUNT = 100;

	@BindView(R.id.recyclerView)
	RecyclerView recyclerView;
	@BindView(R.id.next)
	View next;

	private ExpensesAdapter adapter;

	private int expensesCount = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
		recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(),
			DividerItemDecoration.VERTICAL));
		ServiceUtil.launchMaterials(getApplicationContext(), false, prefs);
	}

	@Override
	protected void onInitWork() {
		adapter = new ExpensesAdapter();
		recyclerView.setAdapter(adapter);
		disposable.add(manager.onSelectTable("SELECT * FROM expenses WHERE work = " + work.id)
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe((Cursor cursor) -> {
				adapter.items.clear();
				expensesCount = cursor.getCount();
				try {
					while (cursor.moveToNext()) {
						Expense expense = new Expense();
						expense.parseCursor(cursor);
						adapter.items.add(expense);
					}
				} finally {
					cursor.close();
				}
				adapter.notifyDataSetChanged();
				checkLimit();
				toggleViews(true);
			}));
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@SuppressWarnings("unused")
	@Subscribe(sticky = true, threadMode = ThreadMode.POSTING)
	public void onMaterialEvent(Material material) {
		toggleViews(false);
		manager.onInsertRow(new Expense(work.id, material.name, material.dimension))
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe((Row row) -> {
				adapter.items.add((Expense) row);
				adapter.notifyDataSetChanged();
				expensesCount++;
				toggleViews(true);
			});
	}

	@SuppressWarnings("unused")
	@Subscribe(threadMode = ThreadMode.POSTING)
	public void onExpenseEvent(Expense expense) {
		toggleViews(false);
		manager.onUpdateRow(expense)
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe((Integer result) -> {
				adapter.notifyDataSetChanged();
				toggleViews(true);
			});
	}

	@SuppressWarnings("unused")
	@Subscribe(threadMode = ThreadMode.POSTING)
	public void onDeleteEvent(DeleteEvent event) {
		toggleViews(false);
		manager.onDeleteRow(adapter.items.get(event.position))
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe((Integer result) -> {
				adapter.items.remove(event.position);
				adapter.notifyDataSetChanged();
				expensesCount--;
				toggleViews(true);
			});
	}

	@Override
	public void onStop() {
		super.onStop();
		EventBus.getDefault().removeStickyEvent(Row.class);
		EventBus.getDefault().removeStickyEvent(Material.class);
		EventBus.getDefault().unregister(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.expenses_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_add:
				if (!checkLimit()) {
					return true;
				}
				Intent intent = new Intent(getApplicationContext(), SelectActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.putExtra(SelectActivity.EXTRA_TYPE, SelectorLayout.TYPE_MATERIALS);
				startActivity(intent);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@UiThread
	private boolean checkLimit() {
		if (expensesCount > MAX_EXPENSES_COUNT) {
			onShowMessage(R.string.error_expenses_max_count);
			return false;
		}
		return true;
	}

	@Override
	protected void onHomeClick() {
		manager.onExecSql("DELETE FROM expenses WHERE work = " + work.id)
			.subscribe((Boolean value1) -> manager.onExecSql("DELETE FROM events WHERE type = '" +
				Event.TYPE_WORK + "' AND stage = '" + Event.STAGE_END + "' AND work = " + work.id)
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe((Boolean value2) -> onUpdateWorkInDb(Work.STEP_END)));
	}

	@OnClick(R.id.next)
	public void onNext() {
		onUpdateWorkInDb(Work.STEP_PHOTOS);
	}

	@Override
	public void toggleViews(boolean enable) {
		super.toggleViews(enable);
		if (adapter != null) {
			adapter.hasEnabledUI = enable;
		}
		next.setEnabled(enable);
	}
}