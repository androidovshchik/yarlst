package com.yarlst.ui.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;

import com.yarlst.R;

public class SelectableButton extends AppCompatImageButton {

    public boolean isSelected = false;

    public SelectableButton(Context context) {
        super(context);
    }

    public SelectableButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SelectableButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public boolean onClick(boolean setSelected) {
        boolean wasSelected = isSelected;
        isSelected = setSelected && !isSelected;
        if (isSelected) {
            setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
        } else {
            setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button));
        }
        return wasSelected;
    }

    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }
}
