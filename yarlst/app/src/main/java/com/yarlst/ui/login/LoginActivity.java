package com.yarlst.ui.login;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.yarlst.R;
import com.yarlst.ui.root.RootActivity;
import com.yarlst.ui.start.StartActivity;
import com.yarlst.ui.work.WorkActivity;
import com.yarlst.utils.VerifyUtil;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import durdinapps.rxfirebase2.RxFirebaseAuth;
import timber.log.Timber;

public class LoginActivity extends WorkActivity {

    @BindView(R.id.phone)
    EditText phoneView;
    @BindView(R.id.get_verification_code)
    View getCodeButton;
    @BindView(R.id.verification_code)
    EditText codeView;
    @BindView(R.id.verify)
    View verifyButton;
    @BindView(R.id.attempt)
    TextView attempt;

    private String verificationId;

    private boolean attemptingCode = false;

    private VerificationCallback callback = new VerificationCallback() {

        @Override
        public void onSuccess(@Nullable PhoneAuthCredential credential,
                              @Nullable String verificationId) {
            if (credential != null) {
                signInWithPhoneAuthCredential(credential);
                return;
            }
            LoginActivity.this.verificationId = verificationId;
            toggleCode(true);
            downTimer.start();
            attemptingCode = true;
        }

        @Override
        public void onError(@StringRes int id) {
            onShowMessage(id);
            if (attemptingCode) {
                downTimer.cancel();
            } else {
                togglePhone(true);
                toggleCode(false);
            }
        }
    };

    private CountDownTimer downTimer = new CountDownTimer(60000, 1000) {

        @Override
        public void onTick(long millisUntilFinished) {
            attempt.setText(getString(R.string.next_attempt, millisUntilFinished / 1000));
        }

        @Override
        public void onFinish() {
            attemptingCode = false;
            togglePhone(true);
            toggleCode(false);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (VerifyUtil.isDeviceRooted(getApplicationContext())) {
            onSwitchSafelyActivity(RootActivity.class);
            return;
        }
        setContentView(R.layout.activity_login);
        setTitle(R.string.title_login);
        ButterKnife.bind(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        }
        MaskedTextChangedListener listener = new MaskedTextChangedListener(
            "+7([000])[000]-[00]-[00]", true, phoneView, null, null);
        phoneView.addTextChangedListener(listener);
        phoneView.setOnFocusChangeListener(listener);
        onCheckUpdates();
    }

    @OnClick(R.id.get_verification_code)
    public void onGetCode() {
        String phone = phoneView.getText().toString().trim();
        Timber.d("phone: %s", phone);
        if (phone.length() != 16) {
            onShowMessage(R.string.error_empty_phone);
            return;
        }
        PhoneAuthProvider.getInstance()
            .verifyPhoneNumber(phone, 60, TimeUnit.SECONDS, this, callback);
        togglePhone(false);
        toggleCode(false);
        codeView.setText("");
        codeView.clearFocus();
        attempt.setText(getString(R.string.wait));
    }

    @OnClick(R.id.verify)
    public void onVerify() {
        String code = codeView.getText().toString().trim();
        Timber.d("code: %s", code);
        if (code.length() <= 0) {
            onShowMessage(R.string.error_empty_code);
            return;
        }
        toggleCode(false);
        signInWithPhoneAuthCredential(PhoneAuthProvider.getCredential(verificationId, code));
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        disposable.add(RxFirebaseAuth.signInWithCredential(FirebaseAuth.getInstance(), credential)
            .map(authResult -> authResult.getUser() != null)
            .subscribe(logged -> {
                Timber.d("signInWithCredential: success");
                onSwitchSafelyActivity(StartActivity.class);
            }, error -> {
                Timber.e(error);
                if (error instanceof FirebaseAuthInvalidCredentialsException) {
                    onShowMessage(R.string.error_phone_invalid_code);
                }
                if (attemptingCode) {
                    toggleCode(true);
                }
            }));
    }

    private void togglePhone(boolean enable) {
        phoneView.setEnabled(enable);
        getCodeButton.setEnabled(enable);
        attempt.setVisibility(enable ? View.INVISIBLE : View.VISIBLE);
    }

    private void toggleCode(boolean enable) {
        codeView.setEnabled(enable);
        verifyButton.setEnabled(enable);
    }
}