package com.yarlst.ui.expenses;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.yarlst.R;
import com.yarlst.models.events.DeleteEvent;
import com.yarlst.models.rows.Expense;
import com.yarlst.ui.base.BaseAdapter;
import com.yarlst.ui.base.BaseViewHolder;
import com.yarlst.utils.EventUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ExpensesAdapter extends BaseAdapter<Expense, ExpensesAdapter.ViewHolder> {

    ExpensesAdapter() {
        super();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_expense,
            parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Expense expense = items.get(position);
        holder.name.setText(expense.material);
        holder.quantity.setText(expense.quantity);
        holder.dimension.setText(expense.dimension);
    }

    @SuppressWarnings("all")
    public class ViewHolder extends BaseViewHolder {

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.quantity)
        TextView quantity;
        @BindView(R.id.dimension)
        TextView dimension;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.container)
        public void onItem() {
            if (!hasEnabledUI) {
                return;
            }
            AlertDialog alertDialog = new AlertDialog.Builder(itemView.getContext())
                .setTitle(R.string.dialog_title)
                .setView(View.inflate(getApplicationContext(), R.layout.dialog_expense, null))
                .setNegativeButton(getString(android.R.string.cancel), null)
                .setNeutralButton(getString(R.string.delete), (DialogInterface dialog, int id) -> {
                    EventUtil.post(new DeleteEvent(getAdapterPosition()));
                })
                .setPositiveButton(getString(android.R.string.ok), null)
                .create();
            alertDialog.setOnShowListener((DialogInterface dialogInterface) -> {
                EditText quantityView = alertDialog.findViewById(R.id.quantity);
                quantityView.setText(items.get(getAdapterPosition()).quantity);
                quantityView.setSelection(quantityView.getText().length());
                Button positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                positiveButton.setOnClickListener((View view) -> {
                    String quantity = quantityView.getText().toString();
                    if (TextUtils.isEmpty(quantity) || !quantity.matches("\\d*\\.?\\d*")) {
                        return;
                    }
                    if (!items.get(getAdapterPosition()).quantity.equals(quantity)) {
                        items.get(getAdapterPosition()).quantity = quantity;
                        EventUtil.post(items.get(getAdapterPosition()));
                    }
                    alertDialog.cancel();
                });
            });
            alertDialog.show();
        }
    }
}
