package com.yarlst.ui.comment;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.EditText;

import com.yarlst.R;
import com.yarlst.data.Prefs;
import com.yarlst.models.rows.Work;
import com.yarlst.ui.settings.SettingsActivity;
import com.yarlst.ui.work.WorkActivity;
import com.yarlst.utils.NetworkUtil;

import butterknife.BindView;
import butterknife.OnClick;

public class CommentActivity extends WorkActivity {

	@BindView(R.id.comment)
	EditText comment;
	@BindView(R.id.done)
	View done;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ConnectivityManager connectivityManager = (ConnectivityManager)
			getSystemService(Context.CONNECTIVITY_SERVICE);
		if (prefs.getBoolean(Prefs.SYNC_ONLY_WIFI_WORKER_IMAGES) &&
			!NetworkUtil.isConnectedViaWifi(connectivityManager)) {
			onShowMessage(getString(R.string.photos_warning), Snackbar.LENGTH_LONG, getString(R.string.change),
				(View view) -> onStartSafelyActivity(SettingsActivity.class));
		}
	}

	@Override
	protected void onInitWork() {
		toggleViews(true);
	}

	@Override
	public void toggleViews(boolean enable) {
		super.toggleViews(enable);
		comment.setEnabled(enable);
		done.setEnabled(enable);
	}

	@OnClick(R.id.done)
	public void onDone() {
		work.comment = comment.getText().toString();
		buildAlertDialog(R.string.confirm_action)
			.setPositiveButton(getString(android.R.string.ok), (DialogInterface dialog, int id) ->
				onUpdateWorkInDb(Work.STEP_DONE))
			.create()
			.show();
	}

	@Override
	protected void onHomeClick() {
		onUpdateWorkInDb(Work.STEP_PHOTOS);
	}
}