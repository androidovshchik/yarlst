package com.yarlst.ui.photos;

import android.database.Cursor;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.yarlst.R;
import com.yarlst.data.Prefs;
import com.yarlst.models.rows.Work;
import com.yarlst.ui.work.WorkActivity;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import me.relex.circleindicator.CircleIndicator;

public class PhotosActivity extends WorkActivity {

	@BindView(R.id.images)
	ToggleViewPager images;
	@BindView(R.id.indicator)
	CircleIndicator indicator;
	@BindView(R.id.next)
	View next;

	private PhotosAdapter adapter;

	@Override
	protected void onInitWork() {
		adapter = new PhotosAdapter(getSupportFragmentManager());
		images.setAdapter(adapter);
		disposable.add(manager.onSelectTable("SELECT * FROM photos WHERE work = " +
			work.id + " LIMIT 5")
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe((Cursor cursor) -> {
				adapter.fragments.clear();
				try {
					while (cursor.moveToNext()) {
						adapter.fragments.add(PhotoFragment.newInstance(cursor, getNotice()));
					}
				} finally {
					cursor.close();
				}
				while (adapter.fragments.size() < 5) {
					adapter.fragments.add(PhotoFragment.newInstance(null, getNotice()));
				}
				adapter.notifyDataSetChanged();
				indicator.setViewPager(images);
				toggleViews(true);
			}));
	}

	@Override
	public void onStart() {
		super.onStart();
		if (!prefs.has(Prefs.HAS_SHOWN_PHOTO_DIALOG)) {
			new AlertDialog.Builder(this)
				.setTitle(R.string.attention)
				.setMessage(R.string.photos_available)
				.setPositiveButton(getString(android.R.string.ok), null)
				.create()
				.show();
			prefs.putBoolean(Prefs.HAS_SHOWN_PHOTO_DIALOG, true);
		}
	}

	@StringRes
	private int getNotice() {
		switch (adapter.fragments.size()) {
			case 0:
				return R.string.notice1;
			case 1:
				return R.string.notice2;
			case 2:
				return R.string.notice3;
			case 3:
				return R.string.notice4;
			default:
				return R.string.notice5;
		}
	}

	@OnClick(R.id.next)
	public void onNext() {
		onUpdateWorkInDb(Work.STEP_COMMENT);
	}

	@Override
	public void toggleViews(boolean enable) {
		super.toggleViews(enable);
		if (adapter != null) {
			for (int f = 0; f < adapter.fragments.size(); f++) {
				adapter.fragments.get(f).hasEnabledUI = enable;
			}
		}
		images.enabled = enable;
		next.setEnabled(enable);
	}

	@Override
	protected void onHomeClick() {
		manager.onExecSql("DELETE FROM photos WHERE work = " + work.id)
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe((Boolean value) -> onUpdateWorkInDb(Work.STEP_EXPENSES));
	}
}