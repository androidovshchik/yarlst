package com.yarlst.ui.photos;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.miguelbcr.ui.rx_paparazzo2.RxPaparazzo;
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData;
import com.miguelbcr.ui.rx_paparazzo2.entities.Response;
import com.miguelbcr.ui.rx_paparazzo2.entities.size.CustomMaxSize;
import com.yalantis.ucrop.UCrop;
import com.yarlst.R;
import com.yarlst.data.DbManager;
import com.yarlst.models.rows.Photo;
import com.yarlst.models.rows.Row;
import com.yarlst.ui.views.SelectableButton;
import com.yarlst.utils.RandomUtil;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class PhotoFragment extends Fragment {

    @BindView(R.id.notice)
    TextView noticeView;
    @BindView(R.id.camera)
    SelectableButton camera;
    @BindView(R.id.gallery)
    SelectableButton gallery;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.remove)
    View remove;

    public boolean hasEnabledUI = false;

    private Unbinder unbinder;

    private Photo photo;

    @StringRes
    private int notice;

    public static PhotoFragment newInstance(@Nullable Cursor cursor, @StringRes int notice) {
        PhotoFragment fragment = new PhotoFragment();
        fragment.photo = new Photo();
        if (cursor != null) {
            fragment.photo.parseCursor(cursor);
        }
        fragment.notice = notice;
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo, container, false);
        unbinder = ButterKnife.bind(this, view);
        noticeView.setText(notice);
        camera.setImageResource(R.drawable.ic_photo_camera_black_24dp);
        gallery.setImageResource(R.drawable.ic_photo_library_black_24dp);
        remove.setVisibility(photo.id > Row.NONE ? View.VISIBLE : View.GONE);
        if (photo.path != null) {
            displayImage(photo.path);
        }
        return view;
    }

    @OnClick(R.id.camera)
    void onCamera() {
        if (!hasEnabledUI) {
            return;
        }
        getPhotoActivity().toggleViews(false);
        processImage(pickSingle().usingCamera());
    }

    @OnClick(R.id.gallery)
    void onGallery() {
        if (!hasEnabledUI) {
            return;
        }
        getPhotoActivity().toggleViews(false);
        processImage(pickSingle().usingGallery());
    }

    private RxPaparazzo.SingleSelectionBuilder<PhotoFragment> pickSingle() {
        UCrop.Options options = new UCrop.Options();
        options.setToolbarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark));
        options.setToolbarTitle(getString(R.string.title_editor));
        return RxPaparazzo.single(this)
            .sendToMediaScanner()
            .setMimeType("image/*")
            .crop(options)
            .limitPickerToOpenableFilesOnly()
            .setMaximumFileSizeInBytes(3 * 1048576)
            .size(new CustomMaxSize(1024));
    }

    @SuppressWarnings("all")
    private void processImage(Observable<Response<PhotoFragment, FileData>> responseObservable) {
        responseObservable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(response -> {
                if (response.resultCode() == PhotosActivity.RESULT_OK) {
                    photo.path = response.data().getFile().getPath();
                    Timber.d("Photo path: " + photo.path);
                    if (photo.id > Row.NONE) {
                        Timber.d(photo.toString());
                        getDbManager().onUpdateRow(photo)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe((Integer result) -> {
                                displayImage(photo.path);
                                getPhotoActivity().toggleViews(true);
                            });
                    } else {
                        photo.work = getPhotoActivity().work.id;
                        photo.name = RandomUtil.generateUid();
                        getDbManager().onInsertRow(photo)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe((Row photo) -> {
                                Timber.d(photo.toString());
                                displayImage(((Photo) photo).path);
                                getPhotoActivity().toggleViews(true);
                            });
                    }
                } else {
                    getPhotoActivity().toggleViews(true);
                }
            }, throwable -> {
                Timber.e(throwable);
                getPhotoActivity().onShowMessage(R.string.error_photo_extra_size);
                getPhotoActivity().toggleViews(true);
            });
    }

    private void displayImage(String path) {
        remove.setVisibility(View.VISIBLE);
        image.setVisibility(View.VISIBLE);
        Glide.with(getApplicationContext())
            .load(new File(path))
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(image);
    }

    @OnClick(R.id.remove)
    void onRemove() {
        if (!hasEnabledUI) {
            return;
        }
        getPhotoActivity().toggleViews(false);
        getDbManager().onDeleteRow(photo)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe((Integer result) -> {
                photo.id = Row.NONE;
                photo.path = null;
                remove.setVisibility(View.GONE);
                image.setImageResource(0);
                image.setVisibility(View.GONE);
                getPhotoActivity().toggleViews(true);
            });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @SuppressWarnings("all")
    protected PhotosActivity getPhotoActivity() {
        return (PhotosActivity) getActivity();
    }

    @SuppressWarnings("all")
    protected DbManager getDbManager() {
        return getPhotoActivity().manager;
    }

    @SuppressWarnings("all")
    protected Context getApplicationContext() {
        return getActivity().getApplicationContext();
    }
}