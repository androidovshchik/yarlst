package com.yarlst.ui.end;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.TextView;

import com.yarlst.R;
import com.yarlst.models.rows.Event;
import com.yarlst.models.rows.Row;
import com.yarlst.models.rows.Work;
import com.yarlst.models.rx.Subscriber;
import com.yarlst.ui.views.SelectableButton;
import com.yarlst.ui.work.WorkActivity;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class EndActivity extends WorkActivity {

	private static final int MAX_EVENTS_COUNT = 200;

	@BindView(R.id.timer)
	TextView timer;
	@BindView(R.id.timeout)
	SelectableButton timeout;
	@BindView(R.id.lunch)
	SelectableButton lunch;
	@BindView(R.id.road)
	SelectableButton road;
	@BindView(R.id.description)
	TextView description;
	@BindView(R.id.finish)
	View finish;

	private Calendar startCalendar = Calendar.getInstance();

	private int eventsCount = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		timeout.setImageResource(R.drawable.ic_access_time_black_24dp);
		lunch.setImageResource(R.drawable.ic_free_breakfast_black_24dp);
		road.setImageResource(R.drawable.ic_time_to_leave_black_24dp);
		startCalendar.setTimeInMillis(0);
		disposable.add(Observable.interval(1, TimeUnit.SECONDS)
			.observeOn(AndroidSchedulers.mainThread())
			.subscribeWith(new Subscriber<Long>() {

				private boolean changedTextSize = false;

				@Override
				public void onNext(Long value) {
					if (startCalendar.getTimeInMillis() != 0) {
						if (!changedTextSize) {
							changedTextSize = true;
							timer.setTextSize(30);
						}
						timer.setText(DateUtils.formatElapsedTime((System.currentTimeMillis() -
							startCalendar.getTimeInMillis()) / 1000));
					}
				}
			}));
	}

	@Override
	protected void onInitWork() {
		description.setText(work.description);
		disposable.add(manager.onSelectTable("SELECT * FROM events WHERE work = " + work.id)
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe((Cursor cursor) -> {
				try {
					eventsCount = cursor.getCount();
					if (cursor.moveToFirst()) {
						Event startWorkEvent = new Event();
						startWorkEvent.parseCursor(cursor);
						startCalendar.setTimeInMillis(startWorkEvent.timestamp);
						if (cursor.moveToLast()) {
							Event lastEvent = new Event();
							lastEvent.parseCursor(cursor);
							switch (lastEvent.type) {
								case Event.TYPE_TIMEOUT:
									timeout.onClick(lastEvent.stage.equals(Event.STAGE_START));
									break;
								case Event.TYPE_LUNCH:
									lunch.onClick(lastEvent.stage.equals(Event.STAGE_START));
									break;
								case Event.TYPE_ROAD:
									road.onClick(lastEvent.stage.equals(Event.STAGE_START));
									break;
							}
						}
						checkLimit();
						toggleViews(true);
					} else {
						android.os.Process.killProcess(android.os.Process.myPid());
					}
				} finally {
					cursor.close();
				}
			}));
	}

	@OnClick({R.id.timeout, R.id.lunch, R.id.road})
	public void onButton(View view) {
		if (!checkLimit()) {
			return;
		}
		eventsCount++;
		boolean wasTimeout = timeout.onClick(view.getId() == timeout.getId());
		boolean wasLunch = lunch.onClick(view.getId() == lunch.getId());
		boolean wasRoad = road.onClick(view.getId() == road.getId());
		String prevType = wasTimeout ? Event.TYPE_TIMEOUT : (wasLunch ? Event.TYPE_LUNCH :
			(wasRoad ? Event.TYPE_ROAD : null));
		String currentType;
		switch (view.getId()) {
			case R.id.timeout:
				currentType = Event.TYPE_TIMEOUT;
				break;
			case R.id.lunch:
				currentType = Event.TYPE_LUNCH;
				break;
			case R.id.road:
				currentType = Event.TYPE_ROAD;
				break;
			default:
				android.os.Process.killProcess(android.os.Process.myPid());
				return;
		}
		insertToDb(prevType, currentType, false);
	}

	@OnClick(R.id.finish)
	public void onEnd() {
		if (timeout.isSelected) {
			insertToDb(Event.TYPE_TIMEOUT, Event.TYPE_TIMEOUT, true);
		} else if (lunch.isSelected) {
			insertToDb(Event.TYPE_LUNCH, Event.TYPE_LUNCH, true);
		} else if (road.isSelected) {
			insertToDb(Event.TYPE_ROAD, Event.TYPE_ROAD, true);
		} else {
			finishEnd();
		}
	}

	@SuppressWarnings("all")
	private void insertToDb(@Nullable String prevType, String newType, boolean finishEnd) {
		toggleViews(false);
		if (prevType != null) {
			if (prevType.equals(newType)) {
				manager.onInsertRow(new Event(work.id, newType, Event.STAGE_END))
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe((Row row) -> {
						if (finishEnd) {
							finishEnd();
						} else {
							toggleViews(true);
						}
					});
			} else {
				manager.onInsertRow(new Event(work.id, prevType, Event.STAGE_END))
					.subscribe((Row rowEnd) -> manager.onInsertRow(new Event(work.id, newType, Event.STAGE_START))
						.observeOn(AndroidSchedulers.mainThread())
						.subscribe((Row rowStart) -> toggleViews(true)));
			}
		} else {
			manager.onInsertRow(new Event(work.id, newType, Event.STAGE_START))
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe((Row row) -> toggleViews(true));
		}
	}

	private void finishEnd() {
		manager.onInsertRow(new Event(work.id, Event.TYPE_WORK, Event.STAGE_END))
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe((Row row) -> onUpdateWorkInDb(Work.STEP_EXPENSES));
	}

	@UiThread
	private boolean checkLimit() {
		if (eventsCount > MAX_EVENTS_COUNT - 1 - (!timeout.isSelected && !lunch.isSelected &&
			!road.isSelected ? 1 : 0)) {
			onShowMessage(R.string.error_events_max_count);
			return false;
		}
		return true;
	}

	@Override
	public void toggleViews(boolean enable) {
		super.toggleViews(enable);
		timeout.setEnabled(enable);
		lunch.setEnabled(enable);
		road.setEnabled(enable);
		finish.setEnabled(enable);
	}

	@Override
	protected void onHomeClick() {
		manager.onExecSql("DELETE FROM events WHERE work = " + work.id)
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe((Boolean value) -> onUpdateWorkInDb(Work.STEP_START));
	}
}