package com.yarlst.ui.settings;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.yarlst.R;

public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.prefs_settings);
    }
}