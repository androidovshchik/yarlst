package com.yarlst.ui.start;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;

import com.yarlst.R;
import com.yarlst.models.rows.Event;
import com.yarlst.models.rows.Operation;
import com.yarlst.models.rows.Project;
import com.yarlst.models.rows.Row;
import com.yarlst.models.rows.Work;
import com.yarlst.ui.root.RootActivity;
import com.yarlst.ui.views.SelectorLayout;
import com.yarlst.ui.work.WorkActivity;
import com.yarlst.utils.RandomUtil;
import com.yarlst.utils.ServiceUtil;
import com.yarlst.utils.VerifyUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

public class StartActivity extends WorkActivity {

	@BindView(R.id.project)
	SelectorLayout projectLayout;
	@BindView(R.id.operation)
	SelectorLayout operationLayout;
	@BindView(R.id.radio)
	RadioGroup radioGroup;
	@BindView(R.id.start)
	View start;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (VerifyUtil.isDeviceRooted(getApplicationContext())) {
			onSwitchSafelyActivity(RootActivity.class);
			return;
		}
		projectLayout.setType(SelectorLayout.TYPE_PROJECTS);
		operationLayout.setType(SelectorLayout.TYPE_OPERATIONS);
		ServiceUtil.launchProjects(getApplicationContext(), false, prefs);
		ServiceUtil.launchOperations(getApplicationContext(), false, prefs);
		onCheckUpdates();
	}

	@Override
	protected void onInitWork() {
		if (work.id != Row.NONE) {
			projectLayout.selected.setText(work.project);
			operationLayout.selected.setText(work.operation);
			radioGroup.check(work.workspace.equals(Work.WORKSHOP) ? R.id.workshop : R.id.object);
		}
		toggleViews(true);
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@SuppressWarnings("unused")
	@Subscribe(sticky = true, threadMode = ThreadMode.POSTING)
	public void onRowEvent(Row row) {
		if (row.getClass().equals(Project.class)) {
			work.project = ((Project) row).name;
			projectLayout.selected.setText(((Project) row).name);
		} else if (row.getClass().equals(Operation.class)) {
			work.operation = ((Operation) row).name;
			work.description = ((Operation) row).description;
			operationLayout.selected.setText(((Operation) row).name);
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		EventBus.getDefault().removeStickyEvent(Row.class);
		EventBus.getDefault().removeStickyEvent(Operation.class);
		EventBus.getDefault().removeStickyEvent(Project.class);
		EventBus.getDefault().unregister(this);
	}

	@OnClick(R.id.start)
	public void onStartAction() {
		if (work.project == null) {
			onShowMessage(R.string.empty_project);
			return;
		}
		if (work.operation == null) {
			onShowMessage(R.string.empty_operation);
			return;
		}
		switch (radioGroup.getCheckedRadioButtonId()) {
			case R.id.workshop:
				work.workspace = Work.WORKSHOP;
				break;
			case R.id.object:
				work.workspace = Work.OBJECT;
				break;
			default:
				onShowMessage(R.string.empty_workspace);
				return;
		}
		toggleViews(false);
		if (work.id > Row.NONE) {
			manager.onInsertRow(new Event(work.id, Event.TYPE_WORK, Event.STAGE_START))
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe((Row work) -> onUpdateWorkInDb(Work.STEP_END));
		} else {
			disposable.clear();
			work.step = Work.STEP_END;
			work.uid = RandomUtil.generateUid();
			work.worker = fireUser.getUid();
			Timber.d(work.toString());
			manager.onInsertRow(work)
				.subscribe((Row work) -> manager.onInsertRow(new Event(work.id, Event.TYPE_WORK, Event.STAGE_START))
					.subscribe((Row event) -> onSwitchSafelyActivity(WorkActivity.class)));
		}
	}

	@Override
	public void toggleViews(boolean enable) {
		super.toggleViews(enable);
		projectLayout.selectButton.setEnabled(enable);
		operationLayout.selectButton.setEnabled(enable);
		toggleViewGroup(radioGroup, enable);
		start.setEnabled(enable);
	}
}