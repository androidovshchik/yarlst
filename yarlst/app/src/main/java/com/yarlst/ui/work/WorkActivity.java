package com.yarlst.ui.work;

import android.content.DialogInterface;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.annotation.UiThread;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.yarlst.R;
import com.yarlst.models.rows.Work;
import com.yarlst.ui.base.BaseActivity;
import com.yarlst.ui.comment.CommentActivity;
import com.yarlst.ui.end.EndActivity;
import com.yarlst.ui.expenses.ExpensesActivity;
import com.yarlst.ui.gms.GMSActivity;
import com.yarlst.ui.login.LoginActivity;
import com.yarlst.ui.photos.PhotosActivity;
import com.yarlst.ui.settings.SettingsActivity;
import com.yarlst.ui.start.StartActivity;
import com.yarlst.ui.time.TimeActivity;
import com.yarlst.ui.update.UpdateActivity;
import com.yarlst.utils.ServiceUtil;
import com.yarlst.utils.VerifyUtil;
import com.yarlst.utils.VersionUtil;

import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

public class WorkActivity extends BaseActivity {

    public Work work = new Work();

    protected FirebaseUser fireUser;

    private boolean hasEnabledUI = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!onCheckConditions()) {
            return;
        }
        if (getClass().equals(StartActivity.class)) {
            setupContentView(R.layout.activity_start, R.string.title_start);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
            }
        } else if (getClass().equals(EndActivity.class)) {
            setupContentView(R.layout.activity_end, R.string.title_end);
        } else if (getClass().equals(ExpensesActivity.class)) {
            setupContentView(R.layout.activity_expenses, R.string.title_expenses);
        } else if (getClass().equals(PhotosActivity.class)) {
            setupContentView(R.layout.activity_photos, R.string.title_photos);
        } else if (getClass().equals(CommentActivity.class)) {
            setupContentView(R.layout.activity_comment, R.string.title_comment);
        }
        toggleViews(false);
        // Last work for current user
        String workerUid = DatabaseUtils.sqlEscapeString(fireUser.getUid());
        disposable.add(manager.onSelectTable("SELECT * FROM works WHERE worker = " + workerUid +
            " ORDER BY id DESC LIMIT 1")
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe((Cursor cursor) -> {
                try {
                    if (cursor.moveToFirst()) {
                        Work work = new Work();
                        work.parseCursor(cursor);
                        Timber.d(work.toString());
                        switch (work.step) {
                            case Work.STEP_START:
                                onInitWork(StartActivity.class, cursor);
                                break;
                            case Work.STEP_END:
                                onInitWork(EndActivity.class, cursor);
                                break;
                            case Work.STEP_EXPENSES:
                                onInitWork(ExpensesActivity.class, cursor);
                                break;
                            case Work.STEP_PHOTOS:
                                onInitWork(PhotosActivity.class, cursor);
                                break;
                            case Work.STEP_COMMENT:
                                onInitWork(CommentActivity.class, cursor);
                                break;
                            case Work.STEP_DONE:
                            case Work.STEP_FINISH:
                                onInitWork(StartActivity.class, null);
                                break;
                        }
                    } else {
                        onInitWork(StartActivity.class, null);
                    }
                } finally {
                    cursor.close();
                }
            }));
    }

    private void setupContentView(@LayoutRes int layout, @StringRes int title) {
        setContentView(layout);
        setTitle(title);
        ButterKnife.bind(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        onCheckConditions();
    }

    private boolean onCheckConditions() {
        if (VersionUtil.hasUpdateFromPrefs(getApplicationContext(), prefs)) {
            onSwitchSafelyActivity(UpdateActivity.class);
            return false;
        }
        if (!VerifyUtil.isGCMCompatible(getApplicationContext())) {
            onSwitchSafelyActivity(GMSActivity.class);
            return false;
        }
        if (!VerifyUtil.isSystemTimeValid(getContentResolver())) {
            onSwitchSafelyActivity(TimeActivity.class);
            return false;
        }
        fireUser = FirebaseAuth.getInstance().getCurrentUser();
        if (fireUser == null) {
            Timber.w(getClass().getSimpleName() + ": fireUser is null");
            onSwitchSafelyActivity(LoginActivity.class);
            return false;
        }
        return true;
    }

    @UiThread
    protected void onInitWork(Class activity, @Nullable Cursor cursor) {
        if (!onSwitchSafelyActivity(activity)) {
            if (cursor != null) {
                work.parseCursor(cursor);
            }
            onInitWork();
        }
    }

    @UiThread
    protected void onInitWork() {}

    @Override
    public void toggleViews(boolean enable) {
        hasEnabledUI = enable;
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(hasEnabledUI);
        }
        invalidateOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!getClass().equals(WorkActivity.class) && !getClass().equals(LoginActivity.class)) {
            getMenuInflater().inflate(R.menu.work_menu, menu);
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return hasEnabledUI;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getClass().equals(StartActivity.class) || getClass().equals(LoginActivity.class)) {
                    toggleViews(false);
                    onHomeClick();
                } else {
                    buildAlertDialog(R.string.confirm_action)
                        .setMessage(R.string.confirm_return)
                        .setPositiveButton(getString(android.R.string.ok), (DialogInterface dialog, int id) -> {
                            toggleViews(false);
                            onHomeClick();
                        })
                        .create()
                        .show();
                }
                return true;
            case R.id.action_settings:
                onStartSafelyActivity(SettingsActivity.class);
                return true;
            case R.id.action_account:
                new AlertDialog.Builder(this)
                    .setTitle(R.string.account)
                    .setMessage(getString(R.string.account_phone, fireUser.getPhoneNumber()))
                    .setPositiveButton(getString(android.R.string.ok), null)
                    .create()
                    .show();
                return true;
            case R.id.action_logout:
                buildAlertDialog(R.string.confirm_action)
                    .setPositiveButton(getString(android.R.string.ok), (DialogInterface dialog, int id) -> {
                        try {
                            toggleViews(false);
                            disposable.clear();
                            FirebaseAuth.getInstance().signOut();
                        } finally {
                            onSwitchSafelyActivity(LoginActivity.class);
                        }
                    })
                    .create()
                    .show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected AlertDialog.Builder buildAlertDialog(@StringRes int title) {
        return new AlertDialog.Builder(this)
            .setTitle(title)
            .setNegativeButton(getString(android.R.string.cancel), null);
    }

    @UiThread
    protected void onHomeClick() {
        finish();
    }

    @Override
    public void onBackPressed() {
        if (hasEnabledUI) {
            super.onBackPressed();
        }
    }

    @UiThread
    protected void onUpdateWorkInDb(int step) {
        toggleViews(false);
        disposable.clear();
        work.step = step;
        Timber.d(work.toString());
        manager.onUpdateRow(work)
            .subscribe((Integer result) -> {
                if (work.step == Work.STEP_PHOTOS) {
                    ServiceUtil.launchDeleteImages(getApplicationContext(), true, prefs);
                } else if (work.step >= Work.STEP_DONE) {
                    ServiceUtil.launchWorkerWorks(getApplicationContext(), true, prefs);
                    ServiceUtil.launchWorkerImages(getApplicationContext(), true, prefs);
                }
                onSwitchSafelyActivity(WorkActivity.class);
            });
    }

    protected void onCheckUpdates() {
        VersionUtil.onCheckAppVersion()
            .subscribe(snapshot -> {
                if (VersionUtil.hasUpdateFromFirebase(getApplicationContext(), snapshot, prefs)) {
                    onSwitchSafelyActivity(UpdateActivity.class);
                }
            }, throwable -> {});
    }
}
