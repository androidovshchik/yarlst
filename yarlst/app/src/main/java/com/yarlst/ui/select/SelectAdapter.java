package com.yarlst.ui.select;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yarlst.R;
import com.yarlst.models.rows.Material;
import com.yarlst.models.rows.Operation;
import com.yarlst.models.rows.Project;
import com.yarlst.models.rows.Row;
import com.yarlst.ui.base.BaseAdapter;
import com.yarlst.ui.base.BaseViewHolder;
import com.yarlst.utils.EventUtil;

import butterknife.BindView;
import butterknife.OnClick;

public class SelectAdapter extends BaseAdapter<Row, SelectAdapter.ViewHolder> {

    SelectAdapter() {
        super();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row,
            parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Row row = items.get(position);
        if (row.getClass().equals(Project.class)) {
            holder.name.setText(((Project) items.get(position)).name);
        } else if (row.getClass().equals(Operation.class)) {
            holder.name.setText(((Operation) items.get(position)).name);
        } else if (row.getClass().equals(Material.class)) {
            holder.name.setText(((Material) items.get(position)).name);
        }
    }

    @SuppressWarnings("all")
    public class ViewHolder extends BaseViewHolder {

        @BindView(R.id.name)
        TextView name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @OnClick(R.id.container)
        public void onItem() {
            EventUtil.postSticky(items.get(getAdapterPosition()));
        }
    }
}
