package com.yarlst.ui.select;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.yarlst.R;
import com.yarlst.data.Prefs;
import com.yarlst.models.events.ImportEvent;
import com.yarlst.models.rows.Material;
import com.yarlst.models.rows.Operation;
import com.yarlst.models.rows.Project;
import com.yarlst.models.rows.Row;
import com.yarlst.ui.base.BaseActivity;
import com.yarlst.ui.views.SelectorLayout;
import com.yarlst.utils.ServiceUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class SelectActivity extends BaseActivity {

	public static final String EXTRA_TYPE = "type";

	@BindView(R.id.recyclerView)
	RecyclerView recyclerView;

	private SelectAdapter adapter;

	private String table = null;

	private int type = SelectorLayout.TYPE_NONE;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select);
		type = getIntent().getIntExtra(EXTRA_TYPE, SelectorLayout.TYPE_NONE);
		switch (type) {
			case SelectorLayout.TYPE_PROJECTS:
				setTitle(R.string.title_select_project);
				table = Project.TABLE;
				break;
			case SelectorLayout.TYPE_OPERATIONS:
				setTitle(R.string.title_select_operation);
				table = Operation.TABLE;
				break;
			case SelectorLayout.TYPE_MATERIALS:
				setTitle(R.string.title_select_material);
				table = Material.TABLE;
				break;
		}
		ButterKnife.bind(this);
		if (getSupportActionBar() != null) {
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}
		recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
		recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(),
			DividerItemDecoration.VERTICAL));
		adapter = new SelectAdapter();
		recyclerView.setAdapter(adapter);
		onRefreshList(null);
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@SuppressWarnings("unused")
	@Subscribe(threadMode = ThreadMode.POSTING)
	public void onRowEvent(Row row) {
		finish();
	}

	@SuppressWarnings("all")
	@Subscribe(sticky = true, threadMode = ThreadMode.POSTING)
	public void onRefreshList(ImportEvent event) {
		disposable.clear();
		disposable.add(manager.onSelectTable("SELECT * FROM " + table)
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe((Cursor cursor) -> {
				adapter.items.clear();
				try {
					while (cursor.moveToNext()) {
						switch (type) {
							case SelectorLayout.TYPE_PROJECTS:
								addRow(new Project(), cursor);
								break;
							case SelectorLayout.TYPE_OPERATIONS:
								addRow(new Operation(), cursor);
								break;
							case SelectorLayout.TYPE_MATERIALS:
								addRow(new Material(), cursor);
								break;
						}
					}
				} finally {
					cursor.close();
				}
				if (adapter.items.size() <= 0) {
					recyclerView.setVisibility(View.INVISIBLE);
					launchForceImport();
				} else {
					recyclerView.setVisibility(View.VISIBLE);
				}
				adapter.notifyDataSetChanged();
			}));
	}

	private void addRow(Row row, Cursor cursor) {
		row.parseCursor(cursor);
		adapter.items.add(row);
	}

	private void launchForceImport() {
		switch (type) {
			case SelectorLayout.TYPE_PROJECTS:
				ServiceUtil.launchProjects(getApplicationContext(), true,
					new Prefs(getApplicationContext()));
				break;
			case SelectorLayout.TYPE_OPERATIONS:
				ServiceUtil.launchOperations(getApplicationContext(), true,
					new Prefs(getApplicationContext()));
				break;
			case SelectorLayout.TYPE_MATERIALS:
				ServiceUtil.launchMaterials(getApplicationContext(), true,
					new Prefs(getApplicationContext()));
				break;
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		EventBus.getDefault().removeStickyEvent(ImportEvent.class);
		EventBus.getDefault().unregister(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.select_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_force_import:
				launchForceImport();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void toggleViews(boolean lock) {}
}