package com.yarlst.services;

import com.yarlst.R;
import com.yarlst.models.rows.Material;

import timber.log.Timber;

public class MaterialsService extends ImportService {

	public static final long INTERVAL = 216 * MINUTE;

	@Override
	public void onCreate() {
		super.onCreate();
		onStartForeground(1001, R.string.sync_materials, R.drawable.ic_cloud_download_white_24dp);
	}

	@Override
	public void afterStartCommand() {
		Timber.d("onStartCommand: importing materials");
		onImportCollection("materials", Material.class);
	}
}
