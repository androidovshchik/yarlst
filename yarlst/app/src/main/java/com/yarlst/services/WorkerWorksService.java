package com.yarlst.services;

import android.database.DatabaseUtils;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.squareup.sqlbrite3.BriteDatabase;
import com.yarlst.R;
import com.yarlst.data.Prefs;
import com.yarlst.models.rows.Event;
import com.yarlst.models.rows.Expense;
import com.yarlst.models.rows.Photo;
import com.yarlst.models.rows.Row;
import com.yarlst.models.rows.Work;
import com.yarlst.models.rx.Subscriber;
import com.yarlst.utils.VersionUtil;

import java.util.ArrayList;

import durdinapps.rxfirebase2.RxFirestore;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class WorkerWorksService extends BaseService {

	public static final long INTERVAL = 30 * MINUTE;

	@Override
	public void onCreate() {
		super.onCreate();
		onStartForeground(102, R.string.sync_worker_works, R.drawable.ic_cloud_upload_white_24dp);
	}

	@Override
	protected boolean onCheckConditions() {
		return super.onCheckConditions() && ifNeedOnlyWifi(Prefs.SYNC_ONLY_WIFI_WORKER_WORKS);
	}

	@Override
	public void afterStartCommand() {
		Timber.d("Selecting works from sqlite");
		disposable.add(Observable.fromCallable(() -> {
			ArrayList<Work> works = new ArrayList<>();
			int appVersion = VersionUtil.getAppVersion(getApplicationContext());
			String workerUid = DatabaseUtils.sqlEscapeString(fireUser.getUid());
			BriteDatabase.Transaction transaction = manager.db.newTransaction();
			try {
				works.addAll(Row.getRows(manager.db.query("SELECT * FROM works WHERE step = " +
					Work.STEP_DONE + " AND worker = " + workerUid + " LIMIT 15"), Work.class));
				for (int w = works.size() - 1; w >= 0; w--) {
					works.get(w).events = processEvents(Row.getRows(manager.db.query("SELECT * FROM" +
						" events WHERE work = " + works.get(w).id + " LIMIT 202"), Event.class));
					if (works.get(w).events.size() <= 0) {
						Timber.w("Empty events for " + works.get(w).uid);
						manager.deleteRow(works.get(w));
						works.remove(w);
						continue;
					}
					works.get(w).expenses = Row.getRows(manager.db.query("SELECT * FROM expenses" +
						" WHERE work = " + works.get(w).id + " LIMIT 100"), Expense.class);
					works.get(w).photos = Row.getRows(manager.db.query("SELECT * FROM photos" +
						" WHERE work = " + works.get(w).id + " LIMIT 5"), Photo.class);
					works.get(w).version = appVersion;
				}
				transaction.markSuccessful();
			} finally {
				transaction.end();
			}
			return works;
		}).subscribeOn(Schedulers.io())
			.subscribeWith(new Subscriber<ArrayList<Work>>() {

				@Override
				public void onNext(ArrayList<Work> works) {
					processWorks(works);
				}

				@Override
				public void onError(Throwable e) {
					onShowMessage(e.getLocalizedMessage());
					onStopSync();
				}
			}));
	}

	private void processWorks(ArrayList<Work> works) {
		if (works.isEmpty()) {
			Timber.d("Empty works to export");
			onStopSync();
			return;
		}
		if (!onCheckConditions()) {
			Timber.w("Hasn't conditions to continue");
			onStopSync();
			return;
		}
		Work work = works.get(0);
		work.timestamp = System.currentTimeMillis();
		works.remove(0);
		Timber.d("Writing to firestore " + work.uid);
		RxFirestore.setDocument(FirebaseFirestore.getInstance()
			.collection("workers/" + work.worker + "/works")
			.document(work.uid), work)
			.subscribe(() -> {
				Timber.d("Finishing in sqlite " + work.uid);
				work.step = Work.STEP_FINISH;
				manager.onUpdateRow(work)
					.subscribe((Integer result) -> processWorks(works));
			}, throwable -> {
				if (throwable instanceof FirebaseFirestoreException) {
					onShowMessage(throwable.getLocalizedMessage());
					processWorks(works);
				} else {
					onStopSync();
				}
			});
	}

	private ArrayList<Event> processEvents(ArrayList<Event> events) {
		int length = events.size();
		ArrayList<Event> validEvents = new ArrayList<>();
		if (length < 2 || !events.get(0).type.equals(Event.TYPE_WORK) || !events.get(length - 1).type
			.equals(Event.TYPE_WORK) || events.get(length - 1).timestamp < events.get(0).timestamp) {
			onShowMessage(getString(R.string.error_parse_work_events));
			return validEvents;
		}
		events.get(0).start = events.get(0).timestamp;
		events.get(0).end = events.get(length - 1).timestamp;
		validEvents.add(events.get(0));
		for (int i = 1; i < length - 2; i += 2) {
			if (!events.get(i).type.equals(events.get(i + 1).type) || !events.get(i).stage
				.equals(Event.STAGE_START) || !events.get(i + 1).stage.equals(Event.STAGE_END) ||
				events.get(i + 1).timestamp < events.get(i).timestamp) {
				onShowMessage(getString(R.string.error_parse_other_events));
				return validEvents;
			}
		}
		for (int i = 1; i < length - 2; i += 2) {
			events.get(i).start = events.get(i).timestamp;
			events.get(i).end = events.get(i + 1).timestamp;
			validEvents.add(events.get(i));
		}
		return validEvents;
	}
}
