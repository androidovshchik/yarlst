package com.yarlst.services;

import com.yarlst.R;
import com.yarlst.models.rows.Project;

import timber.log.Timber;

public class ProjectsService extends ImportService {

	public static final long INTERVAL = 267 * MINUTE;

	@Override
	public void onCreate() {
		super.onCreate();
		onStartForeground(1003, R.string.sync_projects, R.drawable.ic_cloud_download_white_24dp);
	}

	@Override
	public void afterStartCommand() {
		Timber.d("onStartCommand: importing projects");
		onImportCollection("projects", Project.class);
	}
}
