package com.yarlst.services;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.Nullable;

import com.squareup.sqlbrite3.BriteDatabase;
import com.yarlst.R;
import com.yarlst.Yarlst;
import com.yarlst.models.rows.Photo;
import com.yarlst.models.rows.Row;
import com.yarlst.models.rx.Subscriber;

import java.io.File;
import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class DeleteImagesService extends BaseService {

	public static final long INTERVAL = 27 * MINUTE;

	@Override
	public void onCreate() {
		super.onCreate();
		onStartForeground(101, R.string.sync_worker_images, R.drawable.ic_cloud_upload_white_24dp);
	}

	@Override
	protected boolean onCheckConditions() {
		return checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
			PackageManager.PERMISSION_GRANTED;
	}

	@Override
	public void afterStartCommand() {
		Timber.d("onStartCommand: deleting unused images");
		disposable.add(Observable.fromCallable(() -> {
			ArrayList<Photo> requiredPhotos = new ArrayList<>();
			BriteDatabase.Transaction transaction = manager.db.newTransaction();
			try {
				requiredPhotos.addAll(Row.getRows(manager.db.query("SELECT * FROM photos" +
					" WHERE exported = 0"), Photo.class));
				transaction.markSuccessful();
			} finally {
				transaction.end();
			}
			return requiredPhotos;
		}).subscribeOn(Schedulers.io())
			.subscribeWith(new Subscriber<ArrayList<Photo>>() {

				@Override
				public void onNext(ArrayList<Photo> photos) {
					processUnusedPhotos(photos);
				}

				@Override
				public void onError(Throwable e) {
					onShowMessage(e.getLocalizedMessage());
					onStopSync();
				}
			}));
	}

	@SuppressWarnings("all")
	private void processUnusedPhotos(ArrayList<Photo> requiredPhotos) {
		File appDir = getAppDir();
		if (appDir == null) {
			onShowMessage(getString(R.string.error_access_app_dir));
			onStopSync();
			return;
		}
		File[] allFiles = appDir.listFiles();
		Timber.d("Files count in app dir: "+ allFiles.length);
		for (int f = 0, p; f < allFiles.length; f++) {
			for (p = 0; p < requiredPhotos.size(); p++) {
				if (requiredPhotos.get(p).path.equals(allFiles[f].getPath())) {
					break;
				}
			}
			if (p >= requiredPhotos.size()) {
				Timber.d("Deleting " + allFiles[f].getPath());
				if (allFiles[f].delete()) {
					sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
						Uri.fromFile(allFiles[f])));
				} else {
					onShowMessage(getString(R.string.error_delete_photo));
				}
			}
		}
		onStopSync();
	}

	@Nullable
	private File getAppDir() {
		File storageDir = null;
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			storageDir = new File(Environment.getExternalStorageDirectory(), Yarlst.APP_DIR);
			if (!storageDir.exists() && !storageDir.mkdirs()) {
				return null;
			}
		}
		return storageDir;
	}
}
