package com.yarlst.services;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.DatabaseUtils;
import android.net.Uri;

import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;
import com.squareup.sqlbrite3.BriteDatabase;
import com.yarlst.R;
import com.yarlst.data.Prefs;
import com.yarlst.models.rows.Photo;
import com.yarlst.models.rows.Row;
import com.yarlst.models.rows.Work;
import com.yarlst.models.rx.Subscriber;

import java.io.File;
import java.util.ArrayList;

import durdinapps.rxfirebase2.RxFirebaseStorage;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class WorkerImagesService extends BaseService {

	public static final long INTERVAL = 34 * MINUTE;

	@Override
	public void onCreate() {
		super.onCreate();
		onStartForeground(103, R.string.sync_worker_images, R.drawable.ic_cloud_upload_white_24dp);
	}

	@Override
	protected boolean onCheckConditions() {
		return super.onCheckConditions() && ifNeedOnlyWifi(Prefs.SYNC_ONLY_WIFI_WORKER_IMAGES) &&
			checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
				PackageManager.PERMISSION_GRANTED;
	}

	@Override
	public void afterStartCommand() {
		Timber.d("onStartCommand: uploading images");
		disposable.add(Observable.fromCallable(() -> {
			ArrayList<Photo> photos = new ArrayList<>();
			String workerUid = DatabaseUtils.sqlEscapeString(fireUser.getUid());
			BriteDatabase.Transaction transaction = manager.db.newTransaction();
			try {
				photos.addAll(Row.getRows(manager.db.query("SELECT photos.*, works.worker AS worker" +
					" FROM photos INNER JOIN works ON photos.work = works.id WHERE photos.exported = 0" +
					" AND works.worker = " + workerUid + " AND works.step >= " + Work.STEP_DONE +
					" LIMIT 75"), Photo.class));
				transaction.markSuccessful();
			} finally {
				transaction.end();
			}
			return photos;
		}).subscribeOn(Schedulers.io())
			.subscribeWith(new Subscriber<ArrayList<Photo>>() {

				@Override
				public void onNext(ArrayList<Photo> photos) {
					processPhotos(photos);
				}

				@Override
				public void onError(Throwable e) {
					onShowMessage(e.getLocalizedMessage());
					onStopSync();
				}
			}));
	}

	private void processPhotos(ArrayList<Photo> photos) {
		if (photos.isEmpty()) {
			Timber.d("Empty photos to export");
			onStopSync();
			return;
		}
		if (!onCheckConditions()) {
			Timber.w("Hasn't conditions to continue");
			onStopSync();
			return;
		}
		Photo photo = photos.get(0);
		photos.remove(0);
		File file = new File(photo.path);
		if (file.exists()) {
			Timber.d("Writing to storage " + photo.name);
			RxFirebaseStorage.putFile(FirebaseStorage.getInstance().getReference("photos/" +
				photo.worker + "/" + photo.name), Uri.fromFile(file))
				.subscribe((UploadTask.TaskSnapshot taskSnapshot) -> {
					Timber.d("Exporting in sqlite " + photo.name);
					photo.exported = true;
					manager.onUpdateRow(photo)
						.subscribe((Integer result) -> processPhotos(photos));
				}, throwable -> {
					if (throwable instanceof FirebaseFirestoreException) {
						onShowMessage(throwable.getLocalizedMessage());
						processPhotos(photos);
					} else {
						onStopSync();
					}
				});
		} else {
			Timber.w("Doesn't exist " + photo.name);
			photo.exported = true;
			manager.onUpdateRow(photo)
				.subscribe((Integer result) -> processPhotos(photos));
		}
	}
}
