package com.yarlst.services;

import android.content.Intent;

import com.yarlst.R;
import com.yarlst.ui.update.UpdateActivity;
import com.yarlst.utils.NetworkUtil;
import com.yarlst.utils.VersionUtil;

import timber.log.Timber;

public class UpdateService extends BaseService {

	public static final long INTERVAL = 8 * HOUR;

	@Override
	public void onCreate() {
		super.onCreate();
		onStartForeground(1, R.string.sync_update, R.drawable.ic_cloud_download_white_24dp);
	}

	@Override
	protected boolean onCheckConditions() {
		return NetworkUtil.isConnected(connectivityManager);
	}

	@Override
	public void afterStartCommand() {
		Timber.d("onStartCommand: checking updates");
		if (VersionUtil.hasUpdateFromPrefs(getApplicationContext(), prefs)) {
			onShowNotification(2, R.string.title_update, R.drawable.ic_system_update_white_24dp,
				new Intent(getApplicationContext(), UpdateActivity.class));
			onStopSync();
		} else {
			disposable.add(VersionUtil.onCheckAppVersion()
				.subscribe(snapshot -> {
					if (VersionUtil.hasUpdateFromFirebase(getApplicationContext(), snapshot, prefs)) {
						onShowNotification(2, R.string.title_update, R.drawable.ic_system_update_white_24dp,
							new Intent(getApplicationContext(), UpdateActivity.class));
					}
					onStopSync();
				}, throwable -> onStopSync()));
		}
	}
}
