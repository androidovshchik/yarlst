package com.yarlst.services;

import com.yarlst.R;
import com.yarlst.models.rows.Operation;

import timber.log.Timber;

public class OperationsService extends ImportService {

	public static final long INTERVAL = 240 * MINUTE;

	@Override
	public void onCreate() {
		super.onCreate();
		onStartForeground(1002, R.string.sync_operations, R.drawable.ic_cloud_download_white_24dp);
	}

	@Override
	public void afterStartCommand() {
		Timber.d("onStartCommand: importing operations");
		onImportCollection("operations", Operation.class);
	}
}
