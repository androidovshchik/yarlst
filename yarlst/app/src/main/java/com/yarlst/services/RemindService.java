package com.yarlst.services;

import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.provider.Settings;

import com.yarlst.R;
import com.yarlst.data.Prefs;
import com.yarlst.models.rows.Work;
import com.yarlst.utils.NetworkUtil;
import com.yarlst.utils.ServiceUtil;

import timber.log.Timber;

public class RemindService extends BaseService {

	public static final long INTERVAL = 5 * HOUR;

	@Override
	public void onCreate() {
		super.onCreate();
		onStartForeground(3, R.string.sync_remind, R.drawable.ic_cloud_upload_white_24dp);
	}

	@Override
	protected boolean onCheckConditions() {
		return true;
	}

	@Override
	public void afterStartCommand() {
		Timber.d("onStartCommand: checking counts");
		checkWorksExport();
	}

	private void checkWorksExport() {
		Timber.d("Checking works export");
		String workerUid = DatabaseUtils.sqlEscapeString(fireUser.getUid());
		disposable.add(manager.onSelectTable("SELECT COUNT(id) FROM works WHERE worker = " +
			workerUid + " AND step = " + Work.STEP_DONE)
			.subscribe((Cursor cursor) -> {
				int count = 0;
				try {
					if (cursor.moveToFirst()) {
						count = cursor.getInt(0);
						Timber.d("Work's count: " + count);
						if (count >= 15 && !hasShownNotification(Prefs.SYNC_ONLY_WIFI_WORKER_WORKS)) {
							ServiceUtil.launchWorkerWorks(getApplicationContext(), true, prefs);
						}
					}
				} finally {
					cursor.close();
				}
				if (count < 15) {
					checkImagesExport();
				} else {
					onStopSync();
				}
			}));
	}

	private void checkImagesExport() {
		Timber.d("Checking images export");
		String workerUid = DatabaseUtils.sqlEscapeString(fireUser.getUid());
		disposable.add(manager.onSelectTable("SELECT COUNT(photos.id) FROM photos INNER JOIN works" +
			" ON photos.work = works.id WHERE photos.exported = 0 AND works.worker = " + workerUid +
			" AND works.step >= " + Work.STEP_DONE)
			.subscribe((Cursor cursor) -> {
				try {
					if (cursor.moveToFirst()) {
						int count = cursor.getInt(0);
						Timber.d("Photos's count: " + count);
						if (count >= 75 && !hasShownNotification(Prefs.SYNC_ONLY_WIFI_WORKER_IMAGES)) {
							ServiceUtil.launchWorkerImages(getApplicationContext(), true, prefs);
						}
						ServiceUtil.launchDeleteImages(getApplicationContext(), false, prefs);
					}
				} finally {
					cursor.close();
				}
				onStopSync();
			}));
	}

	private boolean hasShownNotification(String pref) {
		if (ifNeedOnlyWifi(pref)) {
			if (!NetworkUtil.isConnectedViaWifi(connectivityManager)) {
				onShowNotification(4, R.string.title_remind, R.drawable.ic_cloud_off_white_24dp,
					new Intent(Settings.ACTION_WIFI_SETTINGS));
				return true;
			}
		} else {
			if (!NetworkUtil.isConnected(connectivityManager)) {
				onShowNotification(4, R.string.title_remind, R.drawable.ic_cloud_off_white_24dp,
					new Intent(Settings.ACTION_SETTINGS));
				return true;
			}
		}
		return false;
	}
}
