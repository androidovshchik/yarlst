package com.yarlst.services;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.sqlbrite3.BriteDatabase;
import com.yarlst.models.events.ImportEvent;
import com.yarlst.models.rows.Row;
import com.yarlst.utils.EventUtil;

import java.util.List;

import durdinapps.rxfirebase2.RxFirestore;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public abstract class ImportService extends BaseService {

	@SuppressWarnings("all")
	protected void onImportCollection(String collection, Class<? extends Row> clss) {
		RxFirestore.getCollection(FirebaseFirestore.getInstance()
			.collection(collection)
			.orderBy("name", Query.Direction.ASCENDING))
			.subscribeOn(Schedulers.io())
			.subscribe((QuerySnapshot querySnapshot) -> {
				onSaveImport(querySnapshot.toObjects(clss));
				EventUtil.postSticky(new ImportEvent());
				onStopSync();
			}, throwable -> onStopSync());
	}

	@SuppressWarnings("all")
	private void onSaveImport(List<? extends Row> rows) {
		if (rows.size() <= 0) {
			Timber.w(getClass().getSimpleName() + ": empty imports");
			return;
		}
		BriteDatabase.Transaction transaction = manager.db.newTransaction();
		try {
			manager.deleteTable(rows.get(0).getTable());
			for (int i = 0; i < rows.size(); i++) {
				manager.insertRow(rows.get(i));
			}
			transaction.markSuccessful();
			Timber.d(getClass().getSimpleName() + ": written imports to sqlite");
		} finally {
			transaction.close();
		}
	}
}