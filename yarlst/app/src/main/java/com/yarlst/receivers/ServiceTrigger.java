package com.yarlst.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.yarlst.data.Prefs;
import com.yarlst.utils.AlarmUtil;
import com.yarlst.utils.ServiceUtil;

import timber.log.Timber;

public class ServiceTrigger extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Timber.d("ServiceTrigger: received");
		Prefs prefs = new Prefs(context);
		boolean launchedMaterials = ServiceUtil.launchMaterials(context, false, prefs);
		boolean launchedOperations = ServiceUtil.launchOperations(context, false, prefs);
		boolean launchedProjects = ServiceUtil.launchProjects(context, false, prefs);
		boolean launchedWorkerWorks = ServiceUtil.launchWorkerWorks(context, false, prefs);
		boolean launchedWorkerImages = ServiceUtil.launchWorkerImages(context, false, prefs);
		boolean launchedDeleteImages = ServiceUtil.launchDeleteImages(context, false, prefs);
		boolean launchedUpdate = ServiceUtil.launchUpdate(context, false, prefs);
		boolean launchedRemind = ServiceUtil.launchRemind(context, prefs);
		if (!launchedMaterials && !launchedOperations && !launchedProjects && !launchedDeleteImages &&
			!launchedWorkerWorks && !launchedWorkerImages && !launchedUpdate && !launchedRemind) {
			AlarmUtil.recreateAlarm(context, getClass());
		}
	}
}
