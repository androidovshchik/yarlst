package com.yarlst;

import android.support.multidex.MultiDexApplication;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.SetOptions;
import com.miguelbcr.ui.rx_paparazzo2.RxPaparazzo;
import com.yarlst.data.Prefs;
import com.yarlst.models.Worker;
import com.yarlst.utils.ProdUtil;
import com.yarlst.utils.ServiceUtil;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import durdinapps.rxfirebase2.RxFirestore;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@ReportsCrashes(mailTo = "vladkalyuzhnyu@gmail.com",
	customReportContent = {
		ReportField.APP_VERSION_CODE,
		ReportField.APP_VERSION_NAME,
		ReportField.ANDROID_VERSION,
		ReportField.PHONE_MODEL,
		ReportField.BRAND,
		ReportField.PRODUCT,
		ReportField.USER_COMMENT,
		ReportField.USER_APP_START_DATE,
		ReportField.USER_CRASH_DATE,
		ReportField.STACK_TRACE,
		ReportField.LOGCAT
	},
	mode = ReportingInteractionMode.DIALOG,
	resDialogText = R.string.error_crash,
	resDialogCommentPrompt = R.string.error_comment,
	resDialogTheme = R.style.AppTheme_Dialog)
public class Yarlst extends MultiDexApplication {

	public static final String APP_DIR = "/Yarlst";

	private Prefs prefs;

	@Override
	public void onCreate() {
		super.onCreate();
		Timber.plant(new Timber.DebugTree());
		if (ProdUtil.getBoolean()) {
			ACRA.init(this);
		} else {
			StethoTool.init(getApplicationContext());
		}
		RxPaparazzo.register(this)
			.withFileProviderPath(APP_DIR);
		prefs = new Prefs(getApplicationContext());
		if (!prefs.has(Prefs.SYNC_ONLY_WIFI_WORKER_IMAGES)) {
			prefs.putBoolean(Prefs.SYNC_ONLY_WIFI_WORKER_IMAGES, true);
		}
		if (!prefs.has(Prefs.SYNC_ONLY_WIFI_WORKER_WORKS)) {
			prefs.putBoolean(Prefs.SYNC_ONLY_WIFI_WORKER_WORKS, false);
		}
		ServiceUtil.launchUpdate(getApplicationContext(), false, prefs);
		ServiceUtil.launchRemind(getApplicationContext(), prefs);
		FirebaseUser fireUser = FirebaseAuth.getInstance().getCurrentUser();
		if (fireUser != null) {
			RxFirestore.setDocument(FirebaseFirestore.getInstance()
				.collection("workers")
				.document(fireUser.getUid()), new Worker(getApplicationContext(), fireUser), SetOptions.merge())
				.subscribeOn(Schedulers.io())
				.subscribe(() -> {}, throwable -> {
					if (throwable instanceof FirebaseFirestoreException) {
						Timber.e(throwable);
						ServiceUtil.launchUpdate(getApplicationContext(), true, prefs);
					}
				});
		}
	}
}