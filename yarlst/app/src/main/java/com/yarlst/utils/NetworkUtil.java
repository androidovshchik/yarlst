package com.yarlst.utils;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {

    @SuppressWarnings("all")
    public static boolean isConnected(ConnectivityManager connectivityManager) {
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    @SuppressWarnings("all")
    public static boolean isConnectedViaWifi(ConnectivityManager connectivityManager) {
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return networkInfo != null && networkInfo.isConnected();
    }
}