package com.yarlst.utils;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.yarlst.data.Prefs;
import com.yarlst.services.DeleteImagesService;
import com.yarlst.services.MaterialsService;
import com.yarlst.services.OperationsService;
import com.yarlst.services.ProjectsService;
import com.yarlst.services.RemindService;
import com.yarlst.services.UpdateService;
import com.yarlst.services.WorkerImagesService;
import com.yarlst.services.WorkerWorksService;

import timber.log.Timber;

public class ServiceUtil {

    @SuppressWarnings("all")
    public static boolean isRunning(Context context, Class<? extends Service> clss) {
        ActivityManager manager = (ActivityManager)
            context.getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (clss.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean launchMaterials(Context context, boolean force, Prefs prefs) {
        return launchService(context, force, prefs, Prefs.LAST_LAUNCH_MATERIALS,
            MaterialsService.class, MaterialsService.INTERVAL);
    }

    public static boolean launchOperations(Context context, boolean force, Prefs prefs) {
        return launchService(context, force, prefs, Prefs.LAST_LAUNCH_OPERATIONS,
            OperationsService.class, OperationsService.INTERVAL);
    }

    public static boolean launchProjects(Context context, boolean force, Prefs prefs) {
        return launchService(context, force, prefs, Prefs.LAST_LAUNCH_PROJECTS,
            ProjectsService.class, ProjectsService.INTERVAL);
    }

    public static boolean launchWorkerWorks(Context context, boolean force, Prefs prefs) {
        return launchService(context, force, prefs, Prefs.LAST_LAUNCH_WORKER_WORKS,
            WorkerWorksService.class, WorkerWorksService.INTERVAL);
    }

    public static boolean launchWorkerImages(Context context, boolean force, Prefs prefs) {
        return launchService(context, force, prefs, Prefs.LAST_LAUNCH_WORKER_IMAGES,
            WorkerImagesService.class, WorkerImagesService.INTERVAL);
    }

    public static boolean launchDeleteImages(Context context, boolean force, Prefs prefs) {
        return launchService(context, force, prefs, Prefs.LAST_LAUNCH_DELETE_IMAGES,
            DeleteImagesService.class, DeleteImagesService.INTERVAL);
    }

    public static boolean launchUpdate(Context context, boolean force, Prefs prefs) {
        return launchService(context, force, prefs, Prefs.LAST_LAUNCH_UPDATE,
            UpdateService.class, UpdateService.INTERVAL);
    }

    public static boolean launchRemind(Context context, Prefs prefs) {
        return launchService(context, false, prefs, Prefs.LAST_LAUNCH_REMIND,
            RemindService.class, RemindService.INTERVAL);
    }

    private static boolean launchService(Context context, boolean force, Prefs prefs, String pref,
                                         Class<? extends Service> clss, long interval) {
        long now = System.currentTimeMillis();
        long delay = prefs.getLong(pref);
        Timber.d(clss.getSimpleName() + ": delay is " + delay);
        Intent intent = ServiceUtil.getIntent(context, clss);
        if (ServiceUtil.isRunning(context, clss)) {
            if (now - delay < interval * 0.9) {
                return false;
            } else {
                context.stopService(intent);
            }
        }
        if (now - delay > interval * 0.9 || force) {
            Timber.d(clss.getSimpleName() + ": passed");
            prefs.putLong(pref, now);
            startServiceRightWay(context, intent);
            return true;
        }
        return false;
    }

    private static Intent getIntent(Context context, Class<? extends Service> clss) {
        return new Intent(context, clss);
    }

    private static void startServiceRightWay(Context context, Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }
}
