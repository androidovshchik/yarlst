package com.yarlst.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import com.yarlst.receivers.ServiceTrigger;
import com.yarlst.services.DeleteImagesService;
import com.yarlst.services.MaterialsService;
import com.yarlst.services.OperationsService;
import com.yarlst.services.ProjectsService;
import com.yarlst.services.RemindService;
import com.yarlst.services.UpdateService;
import com.yarlst.services.WorkerImagesService;
import com.yarlst.services.WorkerWorksService;

import timber.log.Timber;

public class AlarmUtil {

    @SuppressWarnings("all")
    public static void recreateAlarm(Context context, Class clss) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
            new Intent(context, ServiceTrigger.class), 0);
        alarmManager.cancel(pendingIntent);
        long interval = Math.min(Math.min(Math.min(Math.min(Math.min(Math.min(Math.min(UpdateService.INTERVAL,
            MaterialsService.INTERVAL), OperationsService.INTERVAL), ProjectsService.INTERVAL),
            WorkerWorksService.INTERVAL), WorkerImagesService.INTERVAL), RemindService.INTERVAL),
            DeleteImagesService.INTERVAL);
        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
            SystemClock.elapsedRealtime() + interval, interval, pendingIntent);
        Timber.d("New alarm from class " + clss.getSimpleName());
    }

    @SuppressWarnings("all")
    public static void cancelAlarm(Context context, Class clss) {
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
            new Intent(context, ServiceTrigger.class), 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        Timber.d("Cancel alarm from class " + clss.getSimpleName());
    }
}
