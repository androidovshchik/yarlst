package com.yarlst.utils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.yarlst.BuildConfig;

import timber.log.Timber;

public class ProdUtil {

    @SuppressWarnings("all")
    public static boolean getBoolean() {
        boolean prodMode = BuildConfig.VERSION_NAME.endsWith("-prod");
        if (prodMode) {
            try {
                FirebaseUser fireUser = FirebaseAuth.getInstance().getCurrentUser();
                if (fireUser == null) {
                    Timber.w("fireUser: null");
                    return prodMode;
                }
                switch (fireUser.getPhoneNumber()) {
                    case "+79100942590":
                    case "+79101728639":
                    case "+79100950364":
                        return false;
                    default:
                        return prodMode;
                }
            } catch (IllegalStateException e) {
                return prodMode;
            }
        }
        return prodMode;
    }
}
