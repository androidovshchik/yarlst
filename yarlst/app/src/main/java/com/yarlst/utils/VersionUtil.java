package com.yarlst.utils;

import android.content.Context;
import android.content.pm.PackageInfo;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.yarlst.data.Prefs;

import durdinapps.rxfirebase2.RxFirestore;
import io.reactivex.Maybe;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class VersionUtil {

    public static boolean hasUpdateFromPrefs(Context context, Prefs prefs) {
        if (prefs.has(Prefs.LATEST_VERSION)) {
            long latestVersion = prefs.getLong(Prefs.LATEST_VERSION);
            Timber.d("Latest version from prefs: %d", latestVersion);
            if (latestVersion > getAppVersion(context)) {
                return true;
            } else {
                prefs.remove(Prefs.LATEST_VERSION);
            }
        }
        return false;
    }

    public static boolean hasUpdateFromFirebase(Context context, DocumentSnapshot snapshot, Prefs prefs) {
        boolean hasUpdate;
        long latestVersion = 9999L;
        try {
            latestVersion = (Long) snapshot.get("version");
            Timber.d("Latest version from firebase: %d", latestVersion);
            hasUpdate = latestVersion > getAppVersion(context);
        } catch (Exception e) {
            Timber.e(e);
            hasUpdate = true;
        }
        if (hasUpdate) {
            prefs.putLong(Prefs.LATEST_VERSION, latestVersion);
            return true;
        } else {
            prefs.remove(Prefs.LATEST_VERSION);
            return false;
        }
    }

    public static Maybe<DocumentSnapshot> onCheckAppVersion() {
        return RxFirestore.getDocument(FirebaseFirestore.getInstance()
            .collection("info")
            .document("app"))
            .subscribeOn(Schedulers.io());
    }

    public static int getAppVersion(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return info.versionCode;
        } catch (Exception e) {
            Timber.e(e);
        }
        return 0;
    }
}
