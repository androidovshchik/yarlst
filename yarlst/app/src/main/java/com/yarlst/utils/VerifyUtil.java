package com.yarlst.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.provider.Settings;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class VerifyUtil {

    public static boolean isGCMCompatible(Context context) {
        return GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context) ==
            ConnectionResult.SUCCESS;
    }

    public static boolean isSystemTimeValid(ContentResolver contentResolver) {
        try {
            return Settings.Global.getInt(contentResolver, Settings.Global.AUTO_TIME) != 0;
        } catch (Settings.SettingNotFoundException e) {
            return false;
        }
    }

    public static boolean isDeviceRooted(Context context) {
        // return ProdUtil.getBoolean() && new RootBeer(context).isRootedWithoutBusyBoxCheck();
        return false;
    }
}
