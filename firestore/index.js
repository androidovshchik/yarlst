var testSecurityRules = require('firestore-security-tests').testSecurityRules;

var testResourceObj = {
    source: {
        files: [
            {
                name: 'firestore.rules',
                content: `
service cloud.firestore {
  match /databases/{database}/documents {
    match /info {
      allow read: if true;
      match /{document} {
        allow read: if true;
        allow write: if false;
      }
    }
    match /materials {
      allow read: if true;
      match /{document} {
        allow read, write: if true;
      }
    }
    match /operations {
      allow read: if true;
      match /{document} {
        allow read, write: if true;
      }
    }
    match /projects {
      allow read: if true;
      match /{document} {
        allow read, write: if true;
      }
    }
    match /workers {
      allow read: if true;
      match /{worker} {
        allow read: if true;
        allow create: if validUid(worker) && validVersion();
        allow update: if true;
        allow delete: if false;
        match /works {
          allow read: if true;
          match /{work} {
            allow read: if true;
            allow create: if validUid(worker) && validVersion();
            allow update, delete: if true;
          }
        }
      }
    }
    function validUid(uid) {
      return request.auth != null && request.auth.uid == uid;
    }
    function validVersion() {
      // get(/databases/$(database)/documents/info/app).data.version
      return request.resource.data._version >= 10;
    }
  }
}
`
            }
        ]
    },
    testSuite: {
        testCases: [
            // WORKERS
            {
                expectation: 'ALLOW',
                request: {
                    auth: {
                        uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1'
                    },
                    resource: {
                        data: {
                            _uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1',
                            _version: 10
                        }
                    },
                    path: '/databases/(default)/documents/workers/ASdHBqPYNTUr4AjNCZORmdDRCvr1',
                    method: 'create'
                }
            },
            {
                expectation: 'DENY',
                request: {
                    auth: {
                        uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1'
                    },
                    resource: {
                        data: {
                            _uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1',
                            _version: 10
                        }
                    },
                    path: '/databases/(default)/documents/workers/ASdHBqPYNTUr4AjNCZORmdDRCvr1',
                    method: 'delete'
                }
            },
            {
                expectation: 'DENY',
                request: {
                    auth: {
                        uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1'
                    },
                    resource: {
                        data: {
                            _uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1',
                            _version: 11
                        }
                    },
                    path: '/databases/(default)/documents/workers/android',
                    method: 'write'
                }
            },
            {
                expectation: 'DENY',
                request: {
                    auth: {
                        uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1'
                    },
                    resource: {
                        data: {
                            _uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr2',
                            _version: 10
                        }
                    },
                    path: '/databases/(default)/documents/workers/android',
                    method: 'write'
                }
            },
            {
                expectation: 'DENY',
                request: {
                    auth: {
                        uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1'
                    },
                    resource: {
                        data: {
                            _uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1',
                            _version: 9
                        }
                    },
                    path: '/databases/(default)/documents/workers/android',
                    method: 'write'
                }
            },
            {
                expectation: 'DENY',
                request: {
                    auth: {
                        uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1'
                    },
                    resource: {
                        data: {
                            _uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1'
                        }
                    },
                    path: '/databases/(default)/documents/workers/android',
                    method: 'write'
                }
            },
            {
                expectation: 'DENY',
                request: {
                    auth: {
                        uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1'
                    },
                    resource: {
                        data: {
                            _version: 10
                        }
                    },
                    path: '/databases/(default)/documents/workers/android',
                    method: 'write'
                }
            },
            {
                expectation: 'DENY',
                request: {
                    auth: {
                        uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1'
                    },
                    resource: {
                        data: {}
                    },
                    path: '/databases/(default)/documents/workers/android',
                    method: 'write'
                }
            },
            {
                expectation: 'DENY',
                request: {
                    auth: {
                        uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1'
                    },
                    path: '/databases/(default)/documents/workers/android',
                    method: 'write'
                }
            },
            {
                expectation: 'DENY',
                request: {
                    path: '/databases/(default)/documents/workers/android',
                    method: 'write'
                }
            },
            // INFO
            {
                expectation: 'DENY',
                request: {
                    auth: {
                        uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1'
                    },
                    path: '/databases/(default)/documents/info/app',
                    method: 'write'
                }
            },
            {
                expectation: 'ALLOW',
                request: {
                    auth: {
                        uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1'
                    },
                    path: '/databases/(default)/documents/info',
                    method: 'list'
                }
            },
            {
                expectation: 'ALLOW',
                request: {
                    auth: {
                        uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1'
                    },
                    path: '/databases/(default)/documents/info/app',
                    method: 'read'
                }
            },
            // WORKS
            {
                expectation: 'DENY',
                request: {
                    auth: {
                        uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1'
                    },
                    path: '/databases/(default)/documents/works/android',
                    method: 'write'
                }
            },
            {
                expectation: 'ALLOW',
                request: {
                    auth: {
                        uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1'
                    },
                    resource: {
                        data: {
                            _uid: 'android',
                            _version: 10
                        }
                    },
                    path: '/databases/(default)/documents/workers/ASdHBqPYNTUr4AjNCZORmdDRCvr1/works/android',
                    method: 'create'
                }
            },
            {
                expectation: 'ALLOW',
                request: {
                    auth: {
                        uid: 'worker'
                    },
                    resource: {
                        data: {
                            _uid: 'android',
                            _version: 10
                        }
                    },
                    path: '/databases/(default)/documents/workers/worker/works/android',
                    method: 'update'
                }
            },
            {
                expectation: 'ALLOW',
                request: {
                    auth: {
                        uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1'
                    },
                    resource: {
                        data: {
                            _version: 10
                        }
                    },
                    path: '/databases/(default)/documents/workers/ASdHBqPYNTUr4AjNCZORmdDRCvr1/works/android1',
                    method: 'update'
                }
            },
            {
                expectation: 'DENY',
                request: {
                    auth: {
                        uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr2'
                    },
                    resource: {
                        data: {
                            _uid: 'android',
                            _version: 10
                        }
                    },
                    path: '/databases/(default)/documents/workers/ASdHBqPYNTUr4AjNCZORmdDRCvr1/works/android',
                    method: 'write'
                }
            },
            {
                expectation: 'DENY',
                request: {
                    auth: {
                        uid: 'ASdHBqPYNTUr4AjNCZORmdDRCvr1'
                    },
                    resource: {
                        data: {
                            _uid: 'android',
                            _version: 10
                        }
                    },
                    path: '/databases/(default)/documents/workers/ASdHBqPYNTUr4AjNCZORmdDRCvr2/works/android',
                    method: 'write'
                }
            },
            {
                expectation: 'ALLOW',
                request: {
                    path: '/databases/(default)/documents/materials',
                    method: 'list'
                }
            },
            {
                expectation: 'ALLOW',
                request: {
                    path: '/databases/(default)/documents/materials/material',
                    method: 'read'
                }
            },
            {
                expectation: 'ALLOW',
                request: {
                    path: '/databases/(default)/documents/materials/material',
                    method: 'write'
                }
            }
        ]
    }
};

testSecurityRules(printResults, testResourceObj, { verbose: true });

function printResults(resultsObj) {
    var projectId = resultsObj.projectId,
        testResults = resultsObj.testResults,
        error = resultsObj.error,
        errMsg = resultsObj.errMsg;

    if (error) {
        return console.error('\n\ntestSecurityRules ERRORED:\n\n', errMsg, error);
    }

    console.log('\nTest results for '.concat(projectId, ':\n'));
    testResults.forEach(function(testResult) {
        return console.log(testResult.toString());
    });
}